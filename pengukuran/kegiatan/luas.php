<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
<meta charset="utf-8"/>
<script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
<link rel="icon" href="../../img/favicon.ico" type="image/x-icon">
</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../ukursidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Luas</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Kegiatan</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="history.back()">
                                            <i class="fas fa-arrow-left fa-sm fa-fw text-gray-400"></i>
                                            Kembali
                                        </a>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body text-center">
                                <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1603,
"height":818,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 39 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAIIg1VJvAINe/gQAANElAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9z2jgUwM/bT+HRafcQsAEDycTppJ3Z2cykaWeT2dmrsIXRRkheSw4mn75PkrFNgDQYmpA0HJCfrL+/9yQ9ST79mE+Zc0dSSQUPkNdykUN4KCLK4wBlanw0RB/PPpzGRMRklGJnLNIpVgHydcoyH0itfq+v43CSBChkWEoaIidhWOksARLjMaOcIMfJJT3h4gpPiUxwSK7DCZniSxFiZcqaKJWctNuz2ay1qLUl0rgNBct2LqN2HKsWhMiBpnMZoOLhBMpdyj3rmnwd1/Xa/365tPUcUS4V5iE0BLoVkTHOmJLwSBiZEq4cNU9IgBJBuUIOwyPCAvRNS87v45SQP5BTZAJaLjr78NupnIiZI0b/kRDiVJpB0UU+I7R1Gnj9WTCROmmABgPkAFwdjALU8X2AxpIJDpBrEzM8J6lzh6GEIgZnSoQmv4kdYyaLgk1NX0RE7JtekZ5T0BLgdKQioA8POTIhJDJPtofwAMqZGz3XyguFSCPp5AG6wlfImRfhvQ1NEsPmmt4XVfr1WDVnRbRp+Wm7wPo0wBFJCI8g0RJlrxHl/tBQ1gFQ1sFrhlxU+fMg998hb4IMw3xbyl95nW2nEVuvAxMDdMmE7xNFje4F/5vE0OY64+47470yXrbgXiO64A1Af+D/VZI1SSxDqf/BqxHThJF8j+CtR1RAvDRCCb3TzLuoQ9fu2PMjh1obItc4LDw1oeEtJxLcO7Cbslz98BeNYPUy9QnwH6mCkrzB0JZA/udLKqOgMQppHlfDOOOh7lOJ9nOW3tV10e25L6GNqsx9K2NX0ptZShJrqeRyvZArw27m0P3ahi0yxXTNF1zBpguYQUvlStduCUluoKiv/CbFXOqd17IlbdZbiueP6cx/19nh6Wwxb/E7nJaaqGutmVe0cd1ugRm8sOq2mMTrIHZ3YA7KfLe0zZ2MqN9s6Hfc3np6rcEBG9EddE9UGP4pxMoReHfKtp8H13jSOFVEUsx/tC9h87g2or8t5FIfA6uP3du49c7R7xqd+lDaA/v2XPvzeseu5/Vh+3+w5q4BL+1BNGEbUSG27trzIz7QQbOZZii4PvNe7CGsVHLsvbGpYw+bNRoTbmdcCdcCrkk2hwAy32tJ3yfknpHnEMDbex1AtMkPDU9p7pzbHOc24XnHBl0b9Gzgl4Ca7RCNahOYtWo+8oOloddsW/OaJpI3qfRn8NN5NiVpbWq4Wsil8fh2coDysvrJ0ZOmgk12stkqJKMRmNCUgpKOQHtTDA6A9vNHUrBMwZUc3HTx6krOmvGMRmqiHTuoe0xzbS72xUSk9F5wVcJw9CA4Z+bubukUY531dB5zW5dsdbfZGfOYVYPx3EqVAuwhvUn08PxunV7qCKEhhmC/1Rl2vaHfdQfe4Ngf9p9I1Bs2JrpqHmAMuxvIVsMcFLg6zHEaVoej4DWvnXfclusNen63c9zxvePjHjxAJfveB/5ZRlR7mkM8zTMWsJL0px3UMRFmsjp8tlJJCEzyTXkrOMspozid72brWxFWJK/8hRsj1L4dOEDAm7sC2OOqaRdWql3R286MKVDk8GEHHCCYSij/hMPbOBUZLyy7vgrtpevFMnGI+4GREIzAHnfRrU8LuXY1vLLubwJk41909MEHNOHtSORLa9UP7rdkNQIujVC7sl0zAp7ey9V17ujFTaHJwdymm8S1nkiddLv28VJ78aXU2XdQSwMEFAAAAAgAgiDVUjCkgi5iAwAALBEAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzXLTMBCAz/AUGt2J7cROSacuk4EDzABTpheuqrxJBI7kSkoc99V4B56J1U9Tp7SdSaeUaSGHrH68K+nb9Ury0ZvNsiZr0EYoWdJskFICkqtKyHlJV3b26jV9c/zyaA5qDmeakZnSS2ZLWrgnt3pYG4zzsWtjTVNSXjNjBKekqZl1KiVVs1ktJFBCNkYcSvWZLcE0jMMpX8CSfVScWW9rYW1zmCRt2w4uRx0oPU/QsEk2pkrmcztASQlOXZqSxsIh2t3Rbkdeb5imWfL108cwzishjWWS40RwWRXM2Kq2BotQwxKkJbZrABegpOAjHKNmZ1CX9IO0uFbgboqEr/Qa9aNySUdZkdLjly+OuFK6MkRtSookVBfERRAt4kVkoW8d+tahrw2NbWhsfWPiDJqFaok6+4YDl9TqFY4aJ+Qr/hnsfqtqpYku6bAoKEHHZcMDSs68NVY3C4alQZaGX5ZP0iwbZ8OgXrMONFkztBkHZSuruLfoW2esNnEoP/YnVUHoyePzUmBIODDGAjofV2kagMqXAk6cB0ZC54Oqbw8D4tR2NRC7EPy7BIPuxBVslVzhvagqcLEZdEDMQa4RiNIGYyn1o3Qo8PELV3NBuMl8vUOBvRdOYLPXx6lqsSHToDEND06HQYyCyIMotkjgXIZ5Gvdf0oZpDF80xF3/URKD57cwYhthRu+2TpvGai9y0pGPnH0djdNDlPi/6+Qn6tLb6ZJYBlzzzx93w/avJWfaghFM9l7et67jOvfxv879dpBoX0KP34mv7/DDpHovfpOJBzjMUCJCL7cZqngojDEPh6wakmpMtFuTM+a2uDjKIAbijRAju70hqrpbQKWVvOLYa7pCOYoo7/Pm7Is/K0aef4HhdG17yOP2UEzGaT7OH8wX9w3pvchONV+IJVTAdtHiJvdYaIcZrgcXkIed14nnwfakwwwsMBv0uT5eyPoUgZOfBK7DZxOzJ1qY5S7V7BGpjkMiDlQnWHuCVCXY7To/u3I/qxb/s+o+LM9XrPInrrjUL5f1PtMQoHsHW5rffGwaHDwYkD9xlbjxIuEaw22hC+ICRTS4792CTMdBHATxOojJrfcOsWxqwYW925VmpWd4sb7pKBy7dr2a/y2vXhl+lONwX+nOk1zS+y6QXH6EOP4FUEsDBBQAAAAIAIIg1VJFzN5dGgAAABgAAAAWAAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc0srzUsuyczPU0hPT/LP88zLLNHQVKiu5QIAUEsDBBQAAAAIAIIg1VJ93V0pIRwAABTeAAAMAAAAZ2VvZ2VicmEueG1s5Z1pcxvHtYY/J79iCh9Syb0kOPuSkElRkmPL1mbLsV03lXINySEJEQQoAJQo39z/fp/T3QNggAEwQ4DYQpsaENOzdL99Tp+9j//2cNu2PmW9fqvbOWk4TbthZZ3z7kWrc3XSuB9cHsaNv/3198dXWfcqO+ul1mW3d5sOThqBtBxex1/N0A/lu/Tu7qRx3k77/dZ5w7prpwO55KTRvbxstzpZw2pdnDS8i+zyMnT9w8BL/UM/i8PDxLWdQ+/8LA7iNI5976JhWQ/91p873Tfpbda/S8+z9+fX2W36qnueDtRTrweDuz8fHX3+/LmZv1+z27s64hX6Rw/9i6Orq7Mmx4ZFJzv9k4b58GfuW7j6s6euc23bOfrl9Sv9nMNWpz9IO+e8sgzAfeuvv//d8edW56L72frcuhhcM1yh7TWs66x1dc2QxE7csI6k1R3jcpedD1qfsj7Xjv2pej+4vWuoZmlHzv9Of7Law441rIvWp9ZF1jtp2M3Yc4IwiB3fTmI+uA2r22tlnYFp65hnHuV3O/7Uyj7r28on9URebNDtts9S7ujQS+vfFgdXHzzL+rf6EOi/ffNnqP+M1MGxzbex/JPIH2FohbH6wExo9Vtn7eykcZm2+wJy57IHbMO/+4Mv7Uw93nwx6qxzwBj2W7/R2LOZQHqI1XseyG/Iry8npJtjfaLjw6cOevdzH6rPj545fKJjh3yu+kh/bBjVeLi2a1sHcnD0weXAuKhTDLN8x/iqg5zi4OsDYy1tfH25r5v6uo2v2/i8WfVxnewiw6O76CZ8rNpDZtfjHzkcVTcJGKn07KRx+urrr579cFoD1SUn0xBYf6zXgX2g/le/U4/0lur06InB2OSd/8SwMJHW/XTHhR2sYoz9uPIzYV7RivrJffJnurF34MThQeI6B4EMwMRTI74qIVh9hAmq49rRDxweOf5almMFMIvAchJhH8JwXcsJLJ9vYr6JLE++Cxzf8ixp4niW4hG+4s3Cp4OA6wPbcoSR0CsLTkQPXeE9QWAFoRVEcqGw/BDuzc1sfqU1r8OvJ995Hr/qO8/nV7hRwI0CfRteIvBC9UlWioD7BzKalvrSiy0/4UHyRRA5lsc78HdkW9yRG/OiqhNwPPnfsTSziyw3trgf/ZY727XQmD01gxg+MpfnHB/l6+SxwcLqX0trs5YMslskBt4+sSLPCtVaqWACH15UYxW5VhRYkSCQI8Ygx1YoRwObgBYXYAsE1Bw7gAvlSwZK1ldLjbzG0fVzKPmswBSgi2Ay9v5o+HlBuZVjWcwVtTrnOPAW7hAJl9dn4Fm/mQSuFYo8MAMURLhuvzUc3eusjXhnxl0NY6tzdz8oDN35rcg46uOgS+u0rUQz0/6ie37zbDjY5k5Z2keAGt0WaWYkM2nppiBS/e64nZ5lyJNX72UyWNantC2cVD3hstsZWDlZhvLd8ZES346z+/N266KVdn4C+lxUenN/e5b1mHV87Eon1U3kcmso53nemJwXhYFuc97t9i7ef+kzVayH/8l6XB0ESTMOIh9xLfDDKEwa1hd9xrf9ZhK7UeIkCQKU77AG9M9TmedO0nSTyI3C2Hc9L4xFqPxSfi6JfP3s7NP7bDBgAPpW+pAxU83gXfWExYz98bL/rNsefXXXbXUGz9O7wX1Pifg8qifdOu1ctTM1mApnJODzm7Puw3u9njMN5F4/frkTKU2/wdnV826727OgQzdgvedm6siqL0fVRl5t2ArCow3/0sJAJTcdnncSiJ8W6kgbOapWojdonHVXeT/dTXOX9KHVV5yVexemlZokIk7fd1qDV/kfg9b5zaincoGeAtxZzdriPU2T5e95fDQx/Y4NYeST8bZ7kemJbObgebfdTu/62cXY9D4+Klx1fJP1Ollbt+4wH+67933dXL+vutF9P3uXDq5POxc/ZFeQ8rtUGOqA19NNR92+yM5bt1xYeI9U5sY/6K7+9iK76mX5MOmX0eiYt7T6d70svehfZxnUYjDStDLeTHUmf/1jpMV2ppbI2xas5pApcJs+KIEU+oKLqHc57p/3Wncy7a0zuP5NNprYF62+3GL4hbRmSPr0Da7R7QDRQOB5dZ/2rYu0Y30H/xAOYr1jHBgW613a+UBXUWHvB9ddJiQ3TQdcgrJ2nLWzW5Qua6BIoHN/m/VEvzWzIVX6HB27N90z818mgtU9+wDjGsoO+orRkHN6gkQcWQUhgICplLbvrlNRA1nA1I/jJ7bjhMgQmjbSL8K8xtifesDby8t+NrAYwENhJhyisbOvJ2dan3nJbdTQ81A18pByetbvtu8HKN4A3hkp3ronhjmizKBXy1vrJyXwvMvWgyCh2zGWrd+YnMOZpujjVM+c8ZEY0fngGnpCnRViFxlODbr58E3r4iJT72kmJxNWAZzPE+ZfJk/n0/BC7BBfFL8bTY4jA+kUuIpFDoE6VdAuANLKPol2OIVnhLDKQ+Vg2OIQ0LnwjQDK1zQZ0EliVTTVVyCzTMRO6IYOqw5rRBhpLLDIxEnkIaYlHHyb5/82HDnVT2HwhSmrv50i1NFgnXdvb9POhdVRMtk7NVgjQSC1TxrPW73zdvbP0wMr/ZcZk/tBfvKZvp+5y4LBf7bM4DuuXpXU8QmGP71/gIGkvS/zcPEVLgl6QBI7rscbaFgix2dBh5QRsF2mx4pheZ9dyfcTwKCHp46BoADK5a+KncyGBQap7pcPvFwwBU1xhZyLTVEWeCQqo3G2m7YTu4kbO4hYjhNFsY2Oy0gjc/EThigvwOCJssdIH3pcEAcJ8plIW7EdmFcs5UBiyyhnQFYXs2RrwHOcKB4Oals4zcuOLK8isLIGTS3IN1l2J8LU286PvbTTFwtpcRLN5k6TS8/ZFi89iLiMjWeG5umXHsfj7x1beyqz02cH1tk0O33+qxaMKjJUab4M3a6Qp04OHvNsevj4MD18FTisgyejwGJhB2thsc80izWwFKC6qstk5YIl1r/leKwh8rGRxslT+AEbRpo7C0eNfDiq40eBDZMInciM6VYy1DoSTAnJvahDcC92XoIplyyFwJ5YtJwrwygQCrBc1yOu6wXA5Jap1dHW+MKouVROJAspb5zSkkQozbMjRj2OvOHqutOU9qJUV3hea2nbeUpTK5lne35oR3YUioXQUFqYuHjL7cSJXMfDGLcWSnthlrIpWFr1KK21QUpDsxLjB1JBJUpDLcDvGrkxfm7bccMYE+6YHhGgVsAKIzvAxiG0GLAm+m7g0FAkDVubm3eEGNtfrrqdWRqilboCvZV6Zbz2o6gHtEsRc1TLM/WBtpn64GMlXCyY6hfIpwH3XM3QLTfTSjUguEK1+RbqCefn+mTpfCvhELP1vUlqkiFfWigs62Q9uXCCoZXK5v9BWvkkSkIPO4LSoaARxRKvgq6Ec0p8VnwNGPIT+wGCXuCGXhDBDpH+cnHfTxzX9xNY39AisHtACb9a3pC1JqRmLk8Kw+n1aaeWp/k4yXKyUZxyq0UFnMbF9R2X1l9OCgis0pPCgGr40alniBpdtKXmqKq2vIqyfdR0IcswwpHvBl7C0iZzJG46TuLJ9CAywHPFjDkS7cdcpirkhbDatDfI+jiQiY5dsQunDtALzFjlQC/nL9gdoMNmFEdBgqHExiRJzJYCOoIRxxgoMY55ceh5BY/P1gKtIj3qAu39hwAdNNHQQ2LKbD9wEY20XSxuQuA+VJ6EQYImr0XbJwc6eyDmgpB7OpuPcnbTp5+cOGn84eN9d/CXV/84fW89e/ni9M3X1ovTH09/0F+rWxWBHWQPI1z1jaorVmb0K+vwY3FEjoQqj8URjdoOI4oqW2EkCKyPp+xyFDfIqL6WHAeJvFJxTVChwcFcRcB/b6AmspGMbTeGP8dxiPpCKLw2yITNOEH6DX0Hu4yE548wru63O5/025mwqrp+uzz2SkQaoReb3q0saKTcbDLHc4c4WstzpwI+leeOO+yY524eM/xqE1EjY/hJGP1s12vFoBJyenCe+S46Ovb/MJbcExW9wGRCnw+TWAUzrjWo5KsD63zaUPz3OgvV3zdjKB6hw8wSbAzBr9aK7DY9pAxUdQ+NQ/OmtUBWbkL+SpsJFTwFwD7UMyF/2CI/qEFP20FcuxnZHrYROwmR+iIzpDuh3pXZf4eAKauumaFjuJ0+E5swLW50i7a2+t5qm+/X9Sy+crPNmnzzxVNPjgIt11w7JyZMdcPGzTJTe877P4E5d1cn/nwA2tsIQOl67DaDJMYxCCP3yNWRdFMR0hBoxfg3+jGxLmEzYAFIMKRjSRcb4M5CdLsJiMalKT3SywI4Ez8MBeM/Rmd0mokomEni2W4UgequwFcUt75eBryQMQI7OQCeHNbrlCf9ukScSoTi3JhcHpDBfVUw261GAkZXVBkak/5ZlszJNfnGJDKwvLYW2GAnldDipRMKKZbrwk910ap87axrmCrr64+kot1U6GhxCo5dtcA2NUYAu2iVQmNjWoY2dQnIQUtcwvC09THyPc8lENLDiuEWxf+1WR9nTOkS+2M+LfWp+tPZhBKMzWWi2EZOTrHE56LtzIVicQT0CgytamIu6mXJXDYd3N+JLA4TXJyJn0SImZSfyM3osU8wlEcgokdJjLAQCrVO6ypKjLKs/tGz/ss6/5NOWp0/OfV0GOevK+Sn8x99saIcgZxCHMlYqfKKFYQrMfyBrFs+10pMjXlmYF1TI0Vl9iM/bRLdbMWW5FWi6+pgEjdXDysbksWKXQ9dLU3vmyH5m11IP8QY4OEPId8Cx6fnB8MoLypMBCH6DhFdLpnra7QTf0OFhWk78cs6K+3LHQ8odv1tROViGpVv66Dy7TajUiQJL/KI1gqJcyS+I9ccZfQp/hDGREVKtOha7PDfaKOtGuhiIG89O/zHddvhx12Sc5eS6aQJHRIX+7EfkRaGyDgMa+RPrJZoR2j1SJiSyCQo2A4iZxw5UAdeEsLst9feUpngXh5Y2TTBfVeH4L7bZoKrygZjUiqI0khc39HyZmTYoMRuOI4tJUfWQokvNSUqBAqY9OpRotait8MjhmHaGJ6VphYT6T0KIZZCgoRQUG/Kj2zCwNU4R14TFVwCZTz8kXzaIVorc5sZ/irg4hT7TjvFSrgts0zaXedpE608beImT5v4UDttQgfWbd6Htkq1QQrLyQTKefBSHjYZ7RX4D0r6V9l/sDjbr7oDTgtsKi/TJr4shLchXUh9ve2loQUJYgsBmmHNeiw0j4rq3m8ut8BJvRkSelyWBH4zQYlafaFDPlFk8lkI5sPvhvEXNkQgvqvpKLZxyjmSchmQeeliHNpVjGTl2H4qKpfKZwrlhzsslc/yd5i5XObvWBBZP9PfoWWAcX9H2PRjF2mXuD2S89E+1+u7K+mkdlzUyh3IL9lvX4frClZ2RKKdJ4UUxKuhYvZIgnThTBQxofRC0VbwhL6OuTPucnKiGXqsa2tWvGYlgVfTQcvlQuMKg5ZZ5HUoVMJxg7ZmqY24WlfC1SS8ZjHfCnj1oFMZ8oldCXk1KV8qLeyRK+Gnxcl28zW9lbkTVlX6yQRQraIgxryBe7U4eW1NA7coYt8NxKzlkZXthUGERGWqYa2i+tW8EXq9Z1PLjSVfL6SiOtITGoPsZ7COcXyzf+NIjQD+w8Iq+erGB3XoeE0Piyx2WQq1E0Wch6w+2cC+3bOBVZSOZUHWUWxA7jCW1Gv6FOl3nSiJEhEpV2/XLreACo9Utk1hBcq2KXNZ2TZl7KcsobIo07qdt77NW3fUjbCEdmvaQf2nsIPWmSJDPdrUL1fp3EunEyyY5lPR7MtO9Lm9WL3HZo6hU3LkVcGzaHfLi8i0fpz2ukIcqgy55xQ4spa1MctEQRBHZDgThi6lejQiMVazhK+ZzBQiIaByZ/ERbrMT+AAFEeexxD7isBSzshZJDgMf1hJiJMDM4UlesinQwwIhhV8iX7aPwNq2sxB1t4afVbI+Q0hTK7AmJdwGCJQRxa2gHdqYqleEsTYx1VEci4gc3x6Wqtt+pCatB3pXuzHrgeELtQMRZRJXt01WCTI1RUtz635l24HMlVr57CZDBErccdvBJLj9SXANR9kCcI1yVnmHC0BV4CLL1wRXL4v+hsuML2/3m6jcsnfK0XrsQj9v6ag90phGlcuAtMSQgnF4TU09m0PqGVGnkQqOSBPkLAYid6wvYPTnA+tyOn7tbR0n0tstiV9bldHTbbqhlAEkyoAgXsxTOU6BK1Fu7IWYRIS2PQFO5RFtP2tNXmFSQKmzKJRpWhhfkg+teFcRIjtcL8T1nLhUcyLaRquleLAR3tCBKH7O8GtHtRfSlnXLJ91a6mhucz78ktT3ug71vd4S6ltoOfdhf+zzF/n8xyaVEhdvyIq62/BEqEtKoa4+XH4uVamxLoz+XV2qkgu2aKseISqX7L3IFS0IG7uqkaY0WSEsKfzrJ0FIkJWJAPH9JqXUXMmCV56NfaGsq2nKelOHst5suowRW8MrVadc7l61q69cOnEoQ1IQT3Th27XRp0KsaNmuS59ywbrpcxrEasRrzILD6EZXSJLqFaTj5uUB9pEu39Wqg7lxujQ1ocvjYNZDl+7Taw1z6VIhVsyvqEuXcsFWSaPjxHc4SX3bbDWs4j98neOmvIFvtS+whLuKQY52mfIS0vKKerOq7W95BsXl4rK1E55DXbF2cxkUTxItJUYlyXMqT5er6VWU4V6Kq83sYOXu5Du5zN1Zo5qFvhlFJH5RBQQuKxQ0ptdNi58BFRUlOhxXSoQuOJR2tpHQFm0nV1KaeZ0gMnn14jA3hbOSt5LlRangFAfxyaOdo5jHAZXXydnwyMONidjcXfyEw20UP1NTdk5SVtUcgBKzilb/puDD3IVBhUbsKkgeOz7OncVPFqbNMlHjKVkBDwXCghKfO6PLeSgJ701c1PDcBPfraBHbRgwrJnFMl0a9y5vqc9WzOMYvlHuOO1DjJjwLrz/F59yYUsyG9taXxzHdT13n33vMdh9yURVVd2dTOUQfItCRYMeEISdCxixNUowghFCY/BK9QcTjJpI5ipB8v2dePymDGvvoRKTEBrGvc26FI3mxo8o3MvaeVHVbn/eKIT6w7qbtCT/UIZ0fNm5PMKvGGuwJYAhUYeKz5UIAzzNl7A+p1sAfHls8R+woZwfrqpsiNKL0TYVYAcNBPWuCvvO22BJ0iqyPDM2wJhHxY6PiKH5ISQyq1JLTLEuOHmqUJluIiBWcyDO+3yV7Q236/LFWkuKWeLiehouaXCjwL/JRe1270A5JUIFSgOmyrklPLtgmMjSBKnj08edjZPDRbwgT9DK1fwfDKyUHwpht7AOX8FupK7AvVNefprr3daju/V5RXbwh2WUBySlEChhd1SU5uWCbSM5YRdVIopWyIx9RM77UY2fk94a6Sta07+tQ1/cblzlNWBPhtE/vxKIECPscsT1x5LFRAmUmTMWQjdOfwqyAYr8u/ckF20d/svApwxDFPbDksoMI+yVSJ4eqH7tDgmVurO9zWUU5p95r19S08vBRKEIVAjNurL76QNtB7sbqLd5mc8KNFWxJIbCq9dOXCt6qU/5rKd/V7F5VZmXGODoj+6CO2dXIi6ZeJbRDUX8STWQrwoBd1LeXdhYxqc1CZOL62dRoWYSGO+QVf7Qw7zcDUulkPz07xGnl7m49MOFSmwQMoFbjyJhlCGG/3qIhxDiD2d4V1Rf3RuTjj8nN8TuIoCwvG+WKsgXy7OjB+vXepjHk+zjw2C4/ITI7xnKoY5Akgl4KwcYBO+ITKL/VIFb0RuXlvcq8UercY7xRWp4Y90ZJ6C4EQQYqo4c/Ni/ytTZv1HQ/tWMpeIw3Si7a88piTWBimyqbEhFSjdw3lBE0yU/EuI6ryvclGGYbSovdr6i02PgMXE0sFYU7H7dFicRg1cowhLJM+ijvvdMZhpPoflrRFjSrQrdS9q/cvh58WtTHS7Lj2b8TlcM2vJu15NsKAc6NUl+R/yOmbEJEyg+yn8u210Z3i5qErxB8JNUX2FIjfoId/2ab9H46sO6nDXq/1Fnxftm0Qc+U3R8qqU9ZdAo9ayMQllvzftKmHgVXAcDzurY8uWB7qv5rmtSigx9gi2CDBUL3wija6s2A61Ddp5KtM379X+f/am2foS7YtDkd25PWidZAfi6lanwKYRAlEAcUQjH0R50aCJIUZUqku7aqnr5O+hsCV4CztcjKOl2rfpHevHpz+pShahgEu2hjDqrBJUj9RNLIvsPUqNfSJXYoqSEUSaknKNbXSa0JwLEzEVW52elWQjX3gYp/KaXiV3Uo+NVeuZqltCXRBL7HjrAOmVtm3zwhT8eO2HEzZqdw8mzXlbD1iyZPhUgBow91SVMu2J7lEdoLPMmMS+wg8dhxAGowxDf1tdrtm5wGnJDYV6TAl0S+7Q71lXnChmKP8m+90t6tcib8UQpw07alcrlo/SFP6mrnxSBvanvDwif3hlWYTWIYVTXHpNzlsoqqaVdnH6w6e7AsSk6YYQub3dEnKIK3myLnIra1AyO/z9xsUUHVFawqS4BUZ38o5DcX9wfaL4J35BFEPQxkIarMjR1xhiROZMp4UiKSVpKhzWXkEMe7mxsn68P6UZqKkV/aw18usAv1lUnshzssss9yZeUZTmWuLHXuMa4sLQyMu7JwDqKdsjkrFTcJcs7z59fnyprup/ZKhY9xZclFe+7Kcpsh2xklFGGi3jCcbJg1OhlAthZX1qOzBD/Xm8KfdykjsNbeTqOL9nviykaNiQ07p8SvT/QPFbbzmUvabECSrOcSFhQ6a0oJrITxdMF+DdcjWNO+M6ZDBxZkxzgU+A85CueCwlcK47BhHVvZxTjbJVRoPT72h7sey710Ns9Pyh4EBYszJ40/fLzvDv7y6j7tW5b+bP239cc3zIV/nv7rT3zWX06cejb71PPZp16MnbJORo/76Exc0bCkE8WpNOCtRylWugtPZdw5uxrNukjPOkKJ9KyTo2ktM3AF+x+UOpR1eQzj/Of4CKI9ffa8nGrNiVpkK9fsN92ygwFx/WwGGkGdNnK1JtuIMqqyEyUcOSFALC7Ue1w32QoKU3Q7SbbPxklwnM448xhCK4V+VR6OMVKjgISeKBGscWKiPDWp5ZW8ZRfY1exuqRe7R0hA+y7/uGQOS/lUIjHZK0QMdfnmlrgDqelJBKb8s0XCz0wB9xHi7Z6DK/UuiISGhYIiOpmTB1ugpeADRjMjgDqGwRbKcm4Y35lBpI8IId1zfNmmjc3LHdnQBPMTlRfy0pJNEhhIlgtRWyhAGBa2b9swvDMNK48wq+w5vBhVUEopgcf+fIg6Yv0z8II6Wgt7dGNOJop+o7oLLsAFMtA3s5WQl7NPfTf71LezVBf3UarLE4aMjAtUM/dxX5E4NWtCzZazqPBmEnWUXXk+2DJuJXrqBDSvZ6P2bvapt7NPvZmFtVcXa92BtUBtNtFRxzXLzlgOtQiHG6kSpovp9/vZ6Pw4+9T72ad+mIVp8BhM10y+JRuVPbk+5Gk6ZUORapjyagsw/Wk2Or/MPvVqDruW2JFZuIaPwbWa331pk5J4YYVWpfypMimtnDHP5r++CZnQEYPjuCIIdfrcQ7n25O+rrKuyEf76/1BLAQIUABQAAAAIAIIg1VJvAINe/gQAANElAAAXAAAAAAAAAAAAAAAAAAAAAABnZW9nZWJyYV9kZWZhdWx0czJkLnhtbFBLAQIUABQAAAAIAIIg1VIwpIIuYgMAACwRAAAXAAAAAAAAAAAAAAAAADMFAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbFBLAQIUABQAAAAIAIIg1VJFzN5dGgAAABgAAAAWAAAAAAAAAAAAAAAAAMoIAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzUEsBAhQAFAAAAAgAgiDVUn3dXSkhHAAAFN4AAAwAAAAAAAAAAAAAAAAAGAkAAGdlb2dlYnJhLnhtbFBLBQYAAAAABAAEAAgBAABjJQAAAAA=",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>    
                            </div>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>