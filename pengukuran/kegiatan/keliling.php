<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
<meta charset="utf-8"/>
<script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
<link rel="icon" href="../../img/favicon.ico" type="image/x-icon">
</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../ukursidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Keliling</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Kegiatan</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="history.back()">
                                            <i class="fas fa-arrow-left fa-sm fa-fw text-gray-400"></i>
                                            Kembali
                                        </a>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body text-center">
                                <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1603,
"height":818,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 39 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAM24zVKDPI3r/gQAANElAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9z2jgUwM/bT+HRafcQsMEGkonTSTuzs5lJ084ms7NXYYTRRkheSw4mn75PkrFNgDQYmpA0HJCfrL+/9yQ9ST79mE+Zc0dSSQUPkddykUN4JEaUxyHK1PhogD6efTiNiYjJMMXOWKRTrEIU6JRlPpBaPd/XcThJQhQxLCWNkJMwrHSWEInxmFFOkOPkkp5wcYWnRCY4ItfRhEzxpYiwMmVNlEpO2u3ZbNZa1NoSadyGgmU7l6N2HKsWhMiBpnMZouLhBMpdyj3rmnwd1/Xa/365tPUcUS4V5hE0BLo1ImOcMSXhkTAyJVw5ap6QECWCcoUchoeEheiblpzfxykhfyCnyAS0XHT24bdTOREzRwz/IxHEqTSDoot8RmjrNPD6s2AiddIQ9fvIAbg6GIaoEwQAjSUTHCLXJmZ4TlLnDkMJRQzOlIhMfhM7xkwWBZuavogRsW/8Ij2noCXA6UhFQB8ecmRCyMg82R7CAyhnbvRcKy8SIh1JJw/RFb5CzrwI721okhg21/S+qDKox6o5K6JNy0/bBdanAR6RhPARJFqi7DWi3BsYyjoAyjp4zZCLKn8e5N475E2QYZhvS/krr7PtNGLrdWBigC6Z8H2iqNG94H+TGNpcZ9x9Z7xXxssW7DeiC94A9Af+XyVZk8QylPofvBoxTRjJ9wjeekQFxEsjlNA7zbyLOnTtjj0/cqi1IXKNw8JTExrdciLBvQO7KcvVD3/REaxepj4B/iNVUJLXH9gSyP98SWUUNEYhzeNqGGc80n0q0X7O0ru6Lrq++xLaqMrctzJ2Jb2ZpSSxlkou1wu5MuxmDt2vbdgiU0zXfMEVbLqAGbRUrnTtlpDkBor6ym9SzKXeeS1b0ma9pXj+mM6Cd50dns4W8xa/w2mpibrWmnlFG9ftFpjBC6tui0m8DmJ3B+agzHdL29zJiHrNhn7H9dfTa/UP2IjuoHuiwvBPIVaOwLtTtv08uMaTxqkikmL+o30Jm8e1Ef1tIZf66Ft97N7GrXeOQdfoNIDSHti359qf5x+7nteD7f/BmrsGvLQH0YRtRIXYumvPj/hAB81mmpHg+sx7sYewUsnRf2NTxx42azQm3M64Eq4FXJNsDgFkvteSvk/IPSPPIYC39zqAaJMfGp7S3Dm3Oc5twvOODbo28G0QlICa7RCNahOYtWo+8oOlwW+2rXlNE8mbVPoz+Ok8m5K0NjVcLeTSeAI7OUB5Wf3k6ElTwSY72WwVktERmNCUgpKOQHtTDA6A9vOHUrBMwZUc3HTx6krOmvGMjtREO3ZQ95jm2lzsi4lI6b3gqoTh6EFwzszd3dIpxjrr6Tzmti7Z6m6zM+YxqwbjuZUqBdhDepPo4fndOr3UEUJDDMFeqzPoeoOg6/a9/nEw6D2RqDdoTHTVPMAYdjeQrYY5KHB1mOM0qg5HwWteO++4Ldfr+0G3c9wJvONjHx6gkn3vA/8sI6o9zSGe5hkLWEn60w7qmIgyWR0+W6kkBCb5prwVnOWUUZzOd7P1rQgrklf+wo0Rat8OHCDgzV0B7HHVtAsr1a7obWfGFChy+LADDhBMJZR/wtFtnIqMF5ZdX4X20vVimTjE/cBQCEZgj7vo1qeFXLsaXln3NwGy8S86+uADmuh2KPKlteoH91uyGgGXRqhd2a4ZAU/v5eo6d/TiptDkYG7TTeJaT6ROul37eKm9+FLq7DtQSwMEFAAAAAgAzbjNUtoN/fRiAwAALBEAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzXLTMBCAz/AUGt2J7cROSacuk4EDzABTpheuqrxJBI7kSkoc99V4B56J1U9Tp7SdSaeUaSGHrH68K+nb9Ury0ZvNsiZr0EYoWdJskFICkqtKyHlJV3b26jV9c/zyaA5qDmeakZnSS2ZLWrgnt3pYG4zz3LWxpikpr5kxglPS1Mw6lZKq2awWEighGyMOpfrMlmAaxuGUL2DJPirOrLe1sLY5TJK2bQeXow6Unido2CQbUyXzuR2gpASnLk1JY+EQ7e5otyOvN0zTLPn66WMY55WQxjLJcSK4rApmbFVbg0WoYQnSEts1gAtQUvARjlGzM6hL+kFaXCtwN0XCV3qN+lG5pKOsSOnxyxdHXCldGaI2JUUSqgviIogW8SKy0LcOfevQ14bGNjS2vjFxBs1CtUSdfcOBS2r1CkeNE/IV/wx2v1W10kSXdFgUlKDjsuEBJWfeGqubBcPSIEvDL8snaZaNs2FQr1kHmqwZ2oyDspVV3Fv0rTNWmziUH/uTqiD05PF5KTAkHBhjAZ2PqzQNQOVLASfOAyOh80HVt4cBcWq7GohdCP5dgkF34gq2Sq7wXlQVuNgMOiDmINcIRGmDsZT6UToU+PiFq7kg3GS+3qHA3gsnsNnr41S12JBp0JiGB6fDIEZB5EEUWyRwLsM8jfsvacM0hi8a4q7/KInB81sYsY0wo3dbp01jtRc56chHzr6OxukhSvzfdfITdentdEksA67554+7YfvXkjNtwQgmey/vW9dxnfv4X+d+O0i0L6HH78TXd/hhUr0Xv8nEAxxmKBGhl9sMVTwUxpiHQ1YNSTUm2q3JGXNbXBxlEAPxRoiR3d4QVd0toNJKXnHsNV2hHEWU93lz9sWfFSPPv8BwurY95HF7KCbjNB/nD+aL+4b0XmSnmi/EEipgu2hxk3sstMMM14MLyMPO68TzYHvSYQYWmA36XB8vZH2KwMlPAtfhs4nZEy3Mcpdq9ohUxyERB6oTrD1BqhLsdp2fXbmfVYv/WXUflucrVvkTV1zql8t6n2kI0L2DLc1vPjYNDh4MyJ+4Stx4kXCN4bbQBXGBIhrc925BpuMgDoJ4HcTk1nuHWDa14MLe7Uqz0jO8WN90FI5du17N/5ZXrww/ynG4r3TnSS7pfRdILj9CHP8CUEsDBBQAAAAIAM24zVJFzN5dGgAAABgAAAAWAAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc0srzUsuyczPU0hPT/LP88zLLNHQVKiu5QIAUEsDBBQAAAAIAM24zVI0ZgxfVxoAAOLRAAAMAAAAZ2VvZ2VicmEueG1s5V1rc9s21v7c/RUYfXhn930tmQDBW1+7O07Tbd3c2iSbZnZnp0NLtM1YlhxRjp3O/vh9DgBKJEVJpHWjtGlSShR4AZ5zDg7ODSd/fbztsy/RKImHg9MW71gtFg26w148uDpt3Y8v237rr9/96eQqGl5FF6OQXQ5Ht+H4tOVQy8l1+NZxpaRz4d3daavbD5Mk7rbYXT8c0yWnreHlZT8eRC0W905bdi+6vHSFbDt2KNsy8t12ICzetrsXvuOHvi/tXouxxyT+djB8Hd5GyV3Yjd51r6Pb8OWwG47VU6/H47tvj48fHh466ft1hqOrY7xCcvyY9I6vri46OLYYOjlITlvmw7e4b+7qB1tdJyyLH3989VI/px0PknE46OKVaQDu4+/+9M3JQzzoDR/YQ9wbX2O4XMtusesovrrGkPjcb7FjanWHcbmLuuP4S5Tg2sxX1fvx7V1LNQsH9Ps3+hPrTzrWYr34S9yLRqctq+Pb3HEdn0sr8PFBtNhwFEeDsWnLzTOP07udfImjB31b+qSeiBcbD4f9ixB35Ogl+zfDQeiDzdi/1QdHf5fmq6u/eurALXPWp/8F9MV1meurD6CEOIkv+tFp6zLsJwTy4HIE2Cbfk/HXfqQeb05MO8uPMIZJ/Aca2xYISA+xes8j+ufin6QfqJuZPqHjk6eOR/cLH6p/nz5z8kRuufhc9ZEyM4xqPIQlLHZEB64PAgeMi/oJw0znML7qQD/hIPUBY01tpL5c6qZSt5G6jcSbVR/XYhcxPLqLIsDHqj0EdT39kZNRFYGDkQovTltnL3/84dnbsxqorkhME2BlpteOdaT+qn8zj7RX6vT0iU6GeBc/0c0R0rafzgXEwTrGWPqVnwnh5a2ln4GFl0+faQfiiPvuUSD4kUMDUHiqh1MlDKuPEILquHX0HY5HZl+LceZAWDiMByQ+SOAKxh0mccbHGY/ZdM7hktmMmnCbKRkhlWwmOe04uN6xGCdBgl4xSCL0UJDscRzmuMzx6EIS+S6kN25m4R+1xuvgn03nbBv/1Dlb4h9JIwc3cvRt8BKO7apPNFM4uL9Do8nUSdtnMsCD6ITjcWbjHfDdsxjuiBvjRVUnIPHoL2da2HlM+Az3Q7/pzlYtNOaTpuNDjiyUOSfH6Tx5YrBgyTW1NnPJOLqFxoC3D5hnM1fNlQom4IMX1Vh5gnkO8wiBFDEMss9cOhrYCDQ/B5tDoKbYATiXTmKgaH5lauQ1jkKmUOKzApOAzoOJsZfT4ccL0q04Y6AVNTunOOAtxAQJgdfHwGP+BhEI5pI+MAcUqHDDJJ6M7nXUh3pnxl0NYzy4ux/nhq57SzqO+jgeonXYV6qZad8bdm+eTQbb3CkKEyhQ09tCm5nqTFq7yalU35z0w4sI+uTVOyIGxr6EfZKk6gmXw8GYpWzp0rmTY6W+nUT33X7ci8PBB0Cfqkqv728vohGoDh+H1El1E7qcTfQ8287oeZ7r6Dbd4XDUe/c1Aamwx39EI1ztuLKDue+r/iZt9S3phkTOwuo4jvClIz3L87j0cM+v5b+56SOiL++i8Rj9TFj4GIEgzRhdjZQiqQaZPp8nz4Z9nDE/3w3jwfj78G58P1KKPJAb0cufDa76kRoydQ56bvfmYvj4Ts/aAJvu9f7rHeli+gUurr4f9ocjBm4TDvqCm6kj5nY6qjb0ZpNWYC+0wf/RwgBCN538zgOwOL0SHdGGjqoVrQ40mrqneD/dS3OX8DFOlPzEvXPEo0iBlOb7QTx+mX4Zx92baU/pAg007qyGLX9P02T1e54cF4jsxJB/SnK3w16kydVQWnfY74d3SdTLEPHJce6qk5toNIj6uvUA5HA/vE90c/2+6kb3SfRLOL4+G/TeRldg2F9CEptjvJ5uOu12L+rGt7gw9x4h0cbf0V19thddjaJ0mPTLaHTMW7LkbhSFveQ6isATBiPNEdlmqjPp659AJ+xHaiK8jSFQ2iCB2/BRqZ3gIsgK9S4nSXcU3xHVswvI9ptoSti9OKFbTE5QawxJgr5BNgwHgGhM8LAXkA4kHxhWpPfj6yEoD1eHY/yGtddJ1I9usYZiY0Xrg/vbaETLVQN7qJZn6MG96YchdEKcDS8+QQ5NVAF9xXRs8XOBFzhNaqB0BzQT9u+uQ1rVYT5Sf7gMLM5dqASaCcKvJIsy0kw94M3lZRKNGUbKJumCAZu0x6+viiSVgABxGzXGeKgaYlwXXiTD/v0Y62ggO5iuo3VPjKzD2gTLZHprEk9QuIIWu4wfach1O4xl/AeocEJSihHONIlkR2LK0ONrMA5Wp8TVpJKpQTcffop7vUi9p6FCUKZCMiUIEFpET8enyYUwK3xVgm1KBccG0hlwlSycAHWmoF0CJIu+0GJvBk8POiweSgcj/yaALoRvClAqywq9BD3M9hMfZvtpeCwhiNqYNWwXNgHHDTxoh57QxOF1fN/zHCFFEAjhSiD4x2SA1XCQwM9Rtj47w7jTMe0Ob2/DQY8NlCb2ixrT6fQfWqet7+NRtx/98+yIhf8yQ3c/Tn98pu9n7rIEo2erYMRpEIjr6NgIlLijUQIQnuQuqQ8pSjIQknuu5UOxBrutGaV30RWdL+CExXjIDSI5jC5/V0JlPkqJuV+KA10wg1R+Al0IVV5VeCJI04G2OjCy+PjPsX3bE75rO1hnY6RlJ1B/INqwDA2g5mCg23bQsX1OtrXAtS20XiC1yJxRLrTYEJbJeIyncM+fDGmf6OF8QHMv6ayYoGZm65souiNN683g/SgcJGQkzU/T8yVacbq6aPB0hXvRdGXeaPPTFbfxfc/mq8qy9dkRu5iVrd//rrWmitKVmq/CtXsjYLnYkYR9piWswSWH1VVdGUsXrDAbriZiDZdnhlqPLO5EEtRzOyRufSF9z/Jt3zy/kRK0jv5SwmPP63DY88PSX+ZomYq9Mmomzm5Tf1GI5DC6rsdZ10tQSk1T62Os6bTYlnoMy6fFJWwHZ5ziO88H3zme7Zox32++e166bvi+1sx2WHxnprXAczkX0nNdLDsN37megB4Lz57gNvwhW+G752ZWm8Eorsd38Q75To9gG0aUSnyHFQXcsFhKwO1tceFi3LF4mKwoHB/LBuF6lqNX2G0Yf+FXAjRYUTjcE01eUhQZs//1ajiYt1RkoSDkWWiXCd7PtFJAuxAKj2p5oT6gbaQ+SFgTl+uo+gVSKsA91zN0qxFa6WJouppZQm6GY2WqGJWSm1qsVF36FZmJhnxl9bCsk/U0xPD+EdbWcPR1hib/C5fnRYyIG/YEozaw8IXtONKxLdu1KKKJBCZhh9AVzjHd2BZMAPA3ixQpLAOgG0L/F04QwOVlxnkPkSJxtbpBa0tQzZ2cCMSS2WmfJqfFMNFkslOYUj2vAkxZzX3fFffzon6ASbqoC6iGn3k9k9T0ooYapqpa9WZoYmpdzCr2MP1ziEo4/H34+o2frRMIhNPYAWxViL2kGK2pWp/xq6roF0TYhqNxlMDLjEDZNft16sC8xJpVDvNqXoN9gtm23cANLNuBoQQHBbTf8TG/IqrXcz0PBKBn0YYD/QSY/0tAdjvclRbNsx60IMvTDg9udWQAVShAiCKUXUzC20E5ekRUBkLv0dt0mKObBB3FD6et//l8Pxz//4sfXp6/PH/9I3t2/vwMh+dn78/e6p/U7fLYjqPHKbT6ZtWXVQaCygv4TLQRp7DlTLTRtO0k7qgqBZSssEyUWAI/2uU0sBDD/QpP7mBdosPHsH4zAJlbISNgNFYEbtRlISDCueN5QsAMjldW03LHswMQA4fKDMOom5Hl1b163aJXT4Wy1ffqpWFbpOcQJ1no3ep+PS3LRKrBVPbrqbBM5bnDayjrcke4rg37Fixcge3ZXC8KYfxyJJYUri9h+AIH7Z1Xb5GI/GEXUSgZ8zNF2WOQYTvcgvtAz3rbjEH54Yh1Z23Jf6szif1tN7bkKUagL0IoXaVtECHNcNsyI/+gbYUKjBw8n+qZkT81yC2qsTLmkEbbP6pYfScIKVuuIcAMUGfPyBKMFje6RV/bem+1pffHenZeutluDb3p/KipIceqNafH9Zp2b1Yh8AWd2oBld2/If/GI95s44qWrDqfDA5eT6x1LC6hPWoRDl9KW9MkfjQkSc2Fyci1aikKjcvbX8ne7C4SyepO2h6+K3xz4bGVlz/zBecCHJBJaRVqS0kO4DVV4X+DLq1Q/rgIeraGAHR0AHh3WYrMtQwdJ8DMwqMjCWdTWrdJiAagyNop+WEySxVn4xiQ4YEKNl5hniivL/KWFVSb3C92vrj6VT5d1jVBlnX2PDLSbCj3NE1zmqiW2qAy576MVCsYm13ItBymb5OIRcN8ZmzKWzwJUK8kasRtD4xySLjE1pmS5zNo4j5yNyTFLyzCqp+5MYcGVnCqzc2eF5UHPa7CpKrqsZ1NNLzlsOsb82oFv0rYtzxVWIAOp7RSgY4kvNPdJDydzCS3bNKZi3aIMqX+22f+y7l90rupi6tT0kBOwaxSos6+YJr7lTb5pNpz+yv6P/ZmqqvwTsvZff8E3c/p0+ntujpi0MFbikm7nzMSTd9jUEj2blSowDxNtkyOlQNvrsRSX5yEoPY7rAB+YL5cD8x62c4qUKoNlBpcMKoWfns3/KYtl4afnS2D+XBtj052NxfJlXQGeSTymWEeCmI5bghhcZyBWz6luvu+t2XyPyI1q4qGK9V77KUT5QJVY79M8XYxFrawc5F8cRhJpEd2oiK4h7yagKzDYT/LNkGOoHrp6OcyJnvYa3ryy9VMjc4SfYtoHh09Nc1tyvvyE2iazzpfzOtrueUMD+Z8EgZaA24agNwvBz3Ug+LmhEDzNxWX4gAovbcXJ9ZN2kKgRz4fK13Nyfd62kytj67QXTiOzOUpaTzKRjhPVoolGysp8dH7Eolk+elGHj140lI+eJso6lo3AfegjbiCl5KmVCZ4GOotkW8ejGJit8Ni55jGFRg6fUT0e05apZjiS4QIojiXxVGHYpevrcLLA6SCcGzXWeODBwIWqi/vDdGWeZyM2CVn4lV9ov3KJEIUYp3bXab5RnOYb3aT5Rp9q5xvpoNTdu6HXuRKg+ozkWDJLzw04qQmCNbjkSjpd2SW3PJG2ug9bu3Ta2pzTUEZakl25FJA5NuKnQvGkrIgDlnNLgjp2wy9PyzAS5AnkNopeOihNzLU+iJjXGSUAEPmw3AeIgQ64T/U2vGBvIaKpo/kclNW223ulbs9zCBrKLHMILskymesQ1HN63rkNaiYnvmUje84yVLo133ZJJ7Vnr1YeTXrJYTsDhehwW4l81O3FJ7Pa8BFyL1BWGdmp5Nkl18BufIHKAQJNtIKb7af5jpnz+T+9mP/Tz8vcOeJJ7pzNlRpaUEZ2hcSO+W6bQBrPnA5HrG7YvyyKDBMyUNewryaBtUSVZqpXGHN7edR+raQLJKIiIgRFm7jvw+Oup3YkviArFRtwIJMYicXCxnU7NOtTUdj1em20QzoDrhn5JoBr1mq1C+XVddqkhfJkBl0zb+y31+bD8vThxSvwplV3TYsvrMN1sGjgXjZk2Eo989nliNMJEByESCCURUL9Asqp1OJws+Pz6sAISyDCU/qo54a9b6RA1t2WxvH14Y0j6nLhP26hEBdsFYZfESgM5dSD6oGZFFUng00P7JsDG1jF6agqSbMoKpiIVBBiYBHIie8eygRZHuonbMCpWmaXfqntzSQIlL2ZKFnZm2nkZ6zTNCOjdT9tfZu2HqDygbJOD2vapuUmbNN1CGRi2jCbNmD5vIYsqSVEPpOesyqZL+zFOqLIKtuZqfIVGW4ClAtoruFmSS5OVTg2iUOVIbd5Th7reAzYmlGvAXVrJSosBhAzujhw2/ctyB6PiBl1WALUCNpXfEja7AU+gCJAJRxk/wfC4xbKw2mFpO1IiBbXxQRLNeMCkw7VRgHajsNd30PhWZS/wkp/XyEaNkaeVXPakIGgMP8atw1gc2D8d33wDiVbGGZCdRQf2VI+slJQJGVSdrP5SBUtB3rDztUtB6r6RnX7cvVKHDDh1DQcEK3UMRw4xgALTtzzaM8iuEkRXCNRGgCuWZqlIm4puABVgQtNvia4phbnjrdPWN3mV6hFdXBLo+3YhH5r6Kg90ZCGkr1IjsTU5XDf8UzFvbbbwVYRNse2yijCh12WTbTBloJ0fztil7PBhW/qOALfNDS48OkFz3eFU3mI4W96Ja8wyaE0WBZeNquMryiHVgs0NGeLy1C95sH+Dtiwmf5DWUKUpfSbXB9iRfZ6VYe9Xh0We8Hph5egv8J3Ais1V4K7hJSOhdOSKlKuP1NhIXMpRHIY3dVlLrqgQfuQUWVmGA+wnsUGDRBcnkCBwEyBFnx3LVvCBo+9q03AIWpOSBg7sZsKotwowfkwOPBqlgNf1+HA17uuwtbWTGKXK+Dr9veVT39+Yfpb/3aOCxlUAZa3cNdlULpg2ww6i2E17jU2DV15B5XtOUxQsDhZFIC0Z/W667DlL7Uq/O6cLU2l+/LA9u2wJYpq7VQrVYjlE1/q8iVd0CSt1KxuTVRrkfsm267sA/uVuRFfpcApt+Ab7RQsEa9kmUO7SLkL0fLqdzqDtn+k6S2XOFPPhWjvOL1lEyFT2AlN76RbnqJY071Iw72SWJvbwcrdSbenWrhfUCVTPUeoTIC9UFGW3Leli10/dDLnfP1TejgnAtRfRvCfmwrWJrLasg0zNanvDEZKMla6/sLE2Soo6hvpRbpjdXzLQ515Hws1eMmarIosBoiE2E4BMgVdFibFVYenbXaVhREFCiIKVwUOYmeRL7O3ANHksltBaNwea5CDSGHKL8UXr8NdG1tZQWh6CD/yKVygwSBWTKuZrd98lzbVv1XPq8leSPfMJtbIDspiYtsaKMowYlAV/y1n1sz2U+9DYj9lKyK6qMqCdW+Ta2hV4/gwOaFgJAXFUASBCrnoUFgFOAbhFhzTTC7CboPZNQsQ+fXAXHhQtZD1KhDU4kpAoFNfMfABtjvExogeLTSRkrFNVxSG+IjdzRoF3tbhnLc7NwqYWWMLRgFgCKiQ/4r0GUqZ4QZFT6Fo0z6W3EfFUZDcVkwCxCNqzagQy2E4rmcS0HduikEAyxj8kdgfBsOK9GKsS9L9KJEdDkUY+hZ22RFwW2mGoRraxES0kvGxvxpZZfZmCq/PoO9rpY0elDurKEZNWhNVFsgJ0nzZ423woAIlB9NlXcMcXdAkPjSGOezVhJUNak1L7FWIoD87UjsJkQ8ZjIpa0xJbGELR9idr3/3numSW697V4bp3B8V1/o6UlyUspxDJYXRVl+XogiaxnDFt6g08eMeBGRxFN1CNAxHsBz2n/VqHu37dudIJQ4/er24LriiLqldgr0Hfw97p8DhRWd1G8J/CLIdiUpf/6ILm8R9NfKrCChLm4flDgAZiZLBZYaPtr1V8Ub+muoryML3T/qXZ1cNn4ghVas34ohL1AW3HqS9qtHwT4IIvymlIqbWqWz6slBZWp5baSg6o+b2qLMpMlNKcXIJadletL6amcQgsBEHR1vMQW2J/TeNE/buEyETpt+dUbq/tvHCLG4ZpZV52KAoNeVfYNAa7x2Av8n0FjKTULgEzhYdX92TMs4R4RUuInq8cv+Ni6Yvqeh62IPZEkx0Zy2JUdiwVEfu3IASwplO+FEOc9x0bXkPs8hb4MB2mVd1Q9RCbSiNW10IgjHCaDGJFb5TRPku9UWaDjfreKK1PZJPz4IwV5IrFfkE+2CB1xm5oy533KKRFilKmEthkANjwcmZ3l19V9S79+X3m87vM57dzKnxle728ppd6r8qqUjqrrFLVqywXdfPb8JjZDFsAPcV1OEuU2gu4hCDLHIclo31YRflQN8zykf2Luhw2dms3G3RhBYq0UKT7ohCKlJ4HUbUDv2FRQtyvaQueLA+sJ3KNNjB60gY8FPFWK7ETItBk7eK99zqxs4julyK6hmh2hW6lpGu6fT349JoM/qw9T7ouFGvbxRY7WaVO6+Wo+rF5RxUiLbgnUbUaerjA9sup4o4wS9S1oKIX2FYWSXdbdPh/OGL3s5bXj3Xmu4+7trwan/vEmrDJSl9YEO8EwnKz6wdtk1Nw5QDs1jW60gXN2f0iu1mVpEgkRJKhaAjC8SYR3nuw9FnIdV9KtpOpF6BGzXft80hLwS+Iu18b6yGCM5CoPaJiOVB7xvAeSgOBGX3UsKVoW05zwTZ5z4CWAzJeZgif3athmWlj/R6PGVtiuidyha1pUHgT3g8bJbKoJLcJo4apkIo2eVRbC7wqdZGmgAJvuRS2bSP/AIaNQ+Dfj6X8+7Ie/1LzQ4oIoGqiCPqQtqQkONTpMmnlYFBEw3OUE3BBGUgw3xKDftQMalDJIfWpLoPSBc2ZHsGBDireQ92wHAS0e7apB152moYa5TKRNQfpCCsiwhERqbM/PFjmspyoPcoRSQArR2SZKP5MG9Wibawy59D6U5pC109rcN7Udlu6O3ZbanIiE7ayr1GZ0VVXqvO3gJprDaqzFdGyPJI5xrD5Hd1A8cH91DmXya09GPlDFmfLCtmuYVpZAaQ626JBjUNaj4vlL7Rvj7aLmIQcIf5P+EgXFQi+9kz5VJTmRCvKiMdlKCbn729xTpogto/STDrDyrEY5Xo7cV+Z4t7eY819ntPRSIpSp6PxGNV3OmptIOt0pJxrqg9BlU4Rjo7tCTbudASWVZ2OHzLOxY+Zz9CjMt+gTC11POKhVRyPeoA27nhUEom2EyInILEamXa35HgMjBnZ9ZQAqO14nCVM7UVcQpRljseS0T4sx6PouEKi2gpCWhDlwklcpQmL+bjMrTgen5x8+1BP3jzsUaLtE9JsD5xoYShFOppwXQT72Cg0j51O05p7EFOcIxgSvgMUEMrtELLdTewgqaG7lW1iV5xBXs3fj+6X+T+9mf/T6zlTzUP9zetULzZmS83GuZgaz+pYIL1NTzecnqmmGxOZMkUWbDdIcA+lVdH3q2ioPMHf/QdQSwECFAAUAAAACADNuM1SgzyN6/4EAADRJQAAFwAAAAAAAAAAAAAAAAAAAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWxQSwECFAAUAAAACADNuM1S2g399GIDAAAsEQAAFwAAAAAAAAAAAAAAAAAzBQAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWxQSwECFAAUAAAACADNuM1SRczeXRoAAAAYAAAAFgAAAAAAAAAAAAAAAADKCAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc1BLAQIUABQAAAAIAM24zVI0ZgxfVxoAAOLRAAAMAAAAAAAAAAAAAAAAABgJAABnZW9nZWJyYS54bWxQSwUGAAAAAAQABAAIAQAAmSMAAAAA",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>    
                            </div>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>