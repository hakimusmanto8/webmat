<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
<meta charset="utf-8"/>
<script src="https://cdn.geogebra.org/apps/deployggb.js"></script>

<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
<link rel="icon" href="../../img/favicon.ico" type="image/x-icon">
</head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../ukursidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Sudut</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/sudut.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/sudut.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body text-center">
                                    
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1388,
"height":792,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIABwL1lL8yfIf/AQAAEYlAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmt1S2zgUgK+3T+Hx1e4FSezESWAwHdqZnWWG0s7C7OytYiuOFkXyWjJxePoeSf5RCAFiKKQ0N5aPrN/vHElHko8/FnPq3OBMEM5C1+v0XAeziMeEJaGby+nB2P148uE4wTzBkww5U57NkQzdQKWs84HUGQ6GKg6laehGFAlBItdJKZIqS+jy6ZQShl3HKQQ5YvwCzbFIUYQvoxmeo3MeIanLmkmZHnW7i8WiU9Xa4VnShYJFtxBxN0lkB0LXgaYzEbrlyxGUu5J70df5/F7P6/775dzUc0CYkIhF0BDoVoynKKdSwCumeI6ZdOQyxaGbcsKk61A0wTR0vynJ+X2aYfyH65SZgFbPPfnw27GY8YXDJ//hCOJklkPRZT4tdFUa+PyZU545WegCJGALz0no+kEAyGg6QypGJ6VoiTPnBkH+Mgblkkc6t46dIirKYnU9X3iMzZdBmZ4R0BHAdITEoA3PdUSKcazfTP/gBVSz1Fq2yos4z2LhFKF7gS5cZ1mGtybUSTSZS3JbVhnYsXJJy2jd8uNuCfVpeGOcYhZDohXGXivGw7GGrAKgrIKfGXL/R0Me7iFXVd6FDIN8W8pfmc3Wb8XW82FigC7pcD9RWHTP2N84gTbbjPt7xi/KeNWCB7/gMqeTGIZCPcGn4fOU4uIFwRt/qIR4roUauv9830I5Y6+PHGptiVzhMPDkjETXDAtw7nyrXPXyF4lh9VL16Tz4f7aiJAI6IhGRD4Of5ixSvahhfs6zG5t+f9B7C/5NmS+N/162MCY3sHU4eOZEgpa80fhhlgInSqq5XFZyY8rtXLhfzZR5Lqmq64xJ2FYBJWibWOvMNcbpFWT+yq4yxITaW63azmZNZWj5kJaCvZZ2QUvV3MRuUFazt/XUztfZuBp3QPFvrKwtJmobxPPdkp0y2O2tsb0RDdsNdr83uJ9eZ7TDRnQD3eMNhn9KsVns967WU2a+ezxilEksCGKP7S/oMrHG8LdKrjUwMhpo06qt93xBX+stAIp3bLgscRdNWCFc2S0ohiaigWjcrNeAuKOmv5lfxJk6ja68fSPV5AbvbAJotZEiCWZmphRwRN/TtSwhgOS3SlJn+4Wn5SUE8PVWBRCt80NTM1I4pybHqUl46pugb4KBCYIaSbvdm1ZmCnOP5c3emdIH7bYcuzw5vBO1voIHzfI5zqzhflHJtXkEZsBDebl9UvOk4V1Zwma9C0piMJI5ATUcgKc9R7AUK497IjjNJVx4wT0Say68jGEuSCxnysWCuqakUAZhPsx4Rm45k3XnHWXWp1TfjK2cGbSwj7vWqLr33MkWsYQ2I+3USA17cx6uE9nH7KY96yqxaUJDNMxhxx/3vXHQ74280WEwHj4RrjduDXfdMjylqW1tYysd+WVpWWRdBVXTzprieuORP4RLWD84PBx5w8Ho5Xdff9YRzU5iF8/JtIrXkt6HHub7NvsryqNcNEe3RqqZgJW9K38C5QWhBGXL55nzI0wlLpoV/UoL1k37DiLd3BUAnTRNOzOSdaVtOjMlwI3BbxCwNdeVEPYJRddJxnNWWq+9qrxI19/cjdkMbcI5xbCXrLr1qZKtq9S1dXsTIBP/puMNfjeJrie8WFlwHrkPEs0IONeCdcV5zwh4ei/XF6uD0hQmSXO8o84FIbEO75wTqvc3n4jaeDhb3NSteyVrF3hd64+hbvV70sl3UEsDBBQAAAAIABwL1lKU9B6XXAMAAP8QAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWztmM1y0zAQgM/wFBrdiX8Sp6RTl8nAAWaAgeHCVZU3icCRjKTEcV+Nd+CZWP00dUqbmXRCoQM9ZPW3u9K367XVsxebZU3WoI1QsqTZIKUEJFeVkPOSruzs2XP64vzp2RzUHC40IzOll8yWtHArt3rYG4xHYzfGmqakvGbGCE5JUzPrVEqqZrNaSKCEbIw4leo9W4JpGIdPfAFL9lZxZr2thbXNaZK0bTu48jpQep6gYZNsTJXM53aAkhLcujQljY1TtLuj3Q69Xp6mWfL53dvg55mQxjLJcSN4rApmbFVbg02oYQnSEts1gAdQUvAh+qjZBdQlfSMtnhW42yLhK71G/ahc0mFWpPT86ZMzrpSuDFGbkiIJ1QVxGUSLeBFZmFuHuXWYa8NgGwZbP5g4g2ahWqIuvqDjklq9Qq9xQ77j1+D0S1UrTXRJ86KgBAOX5SeUXHhrrG4WDFuDLCyvWQearBnaiE7YyiruLfjRGatNNO19vVMVhJlRXC8FpoADYSxgsPFUpgGofCvgQ78Y+c4nUd8eJsAn29VA7ELwrxIMhi/vKbnGa1FV4HIx6ICYg1wjAKUN5k7qvXQocPml67mk22S+36HA2UsncNjr41a12JBp0JiGhdM8iGEQoyCKLRL4JsM+jfstacM0pisa4m7+LInJ8kvasI0ww1fbIE1jt5cp6dBnyqGBxe0hSvzdDeojDenddElsA575x/f9sP1jyJm2YASTvYf1pZu4yX38r3O/GyTal9Dj98H3d/hhEb0Xv8nEA8wzlIjQy21FKo6FMdbdUEVDEY2FdWtyxtwrLXrZ1sLbIEZ2B0NUdbeASit5zbE3dI1yGFHe58k5FH9WDD3/AtPpxusgovtzOXwQyqnmC7GECtguyyy/myW+Co/JMs/wPHiAUXi1OnENMw1/xWScjsajoyX2g7D90GHJFfj497nuydGjcw01YhK45jtJ+qi5amGWu1SzB6Q6DpU3UJ1g7xFSlWC353zv2v0yWvwvo3vhfVuxyn9TxbN9vOr3IYaMPDi70tHtH0aDk6Nl0++4LNx6VXCD4T7QBXGJIho89PZApuMgToJ4HsTkzpuFWDa14MLuD6VZ6RlelW/72I1Tu1Ed/bVRPfIXb19p78da0rvqJ1f/Vzj/CVBLAwQUAAAACAAcC9ZS1je9uRkAAAAXAAAAFgAAAGdlb2dlYnJhX2phdmFzY3JpcHQuanNLK81LLsnMz1NIT0/yz/PMyyzR0FSorgUAUEsDBBQAAAAIABwL1lLod0uoKwwAAOw/AAAMAAAAZ2VvZ2VicmEueG1s7Vvpcts4Ev498xQo/thfscT7yMqZko9kUpU4rjjZ2po/WxQFSVxTpIaHLaXyUjv39Qx+pv0aICnRkmMpPpWaKssEARDo7q/R3WiCnW+m44id8TQLk3hX0VqqwngcJP0wHu4qRT7YcZVvnn3dGfJkyHupzwZJOvbzXcWinvVzuGvZpk11/mSyqwSRn2VhoLBJ5Of0yK6SDAZRGHOFhX3Mc3hoO4fm8x3V6to75sHz7s6ee6DvOJ6p7ZsHdtc19xXGpln4NE6O/DHPJn7AT4IRH/uvksDPxayjPJ88bbfPz89bFX2tJB22QULWnmb99nDYa+GqMDAZZ7tKWXiKcRtPnxviOV1Vtfa/X7+S8+yEcZb7cQCSSQBF+OzrrzrnYdxPztl52M9HYMNwXYWNeDgcQSSOpyusTb0mkMuEB3l4xjM8u3AruM/HE0V082Nq/0qWWFQzprB+eBb2ebqrqC1XtUzHslTT0HXHtm2FJWnI47zsq5VztqvROmchP5fDUknMaCosT5Ko59OI7ONHpqu6yp7QRZMXXdaq8lY15EWXF1NeLNnHlE+asqsp+5iyj2lAK8Is7EV8Vxn4UUaAx4MUENb3WT6LuCClrJgzrj0BO1n4AZ0NFcokxY16VX1CPxs/kxqI5QX+gELNH/pp7CPDBTzRxWBENwqgny5meWvLW0dcNCEW3Lqy1qNbyPpGzEAYn8GMATVaf9o8La4SoWXNJ7XUJ+JP/JamxCP1jHLADfisuXRN4LAulxsxefWcUEtg7/d2le6rF4d7b7vLFOhgfBUF2g3hrUjQrAVwPy1nu7EQbwRzPbsJSa6Jsg58boNj012bY1P1nFvhU7e8ZWgdmpXEf2lWh5Ri2eDJK6yDuN5smX0G+paGKRfJYhqzYKwspnmwnjZZIp1pFjNR46LGYQbVWZrJDEZdNIMJQ2sKo2WjhZrxH0JgGhlkMMZgzMGkTjbcspiFbg49S+bQhl3DUyp+1BsU4WdQnWHgJ+oMEz+y6hYGsuQwoMMybFGyxH8ykxZmsUisTDShzvQwHVVYjsYMUIJ7R2UYF8ODYsEN/Af9aUy6DofpLhOjivHV24HlOnvbaVduslMiwrIR9S7NX87HCBhU5hjMFn5EIAWIQKKEy9GZYzGHvEgFGoTsMpuuJXKEm9tAziJcF+CzqRIiIqfDhOQljrpZQYmyAJOAboIJqZtzwYNAGkpjDOrCbAKoRABU6DUGOsiHyG0GnCyd2aRBV8CB8C3JwlqwIx4htCshEDIM40mRN+QWjCm+EcU8QW8/EmFZ2b+fBKd7taTLkbifIXiaD4tIZh4vycimEU591Yn8HkcsOTwhNWDszI/IIYgZBkmcs2pl2rIuSOLsOE3y/SQqxnHGWJBEak1wEmkLZb2mBDfGQoO52GAtNNgLZafq1Jg3QQsrMo75kxRKVY7j9/svqctcqyGVN3E020u5fzpJwjiv2xrjHfln4VDoO8lSRgy0kkW0Pdsr8pwAk0RRzQHHP+FwJ3i62U5jnuQcSGkuzdJpi0C3w4sgCvuhH/8Lq6QKKo+KcY+nWJ0oJqQSgiwSNqsjYg3jzCNiw6tIT9L+ySzDqmLT73iKp23PbdmW5Rim7moIceEZZ7LF8IyWaqmOo2uebtsWBswCn8yBZrZM1dAN3fE83XE9eITZFU0IosTM/OyE5zmUJWP+lM+FPUxJcAs3L7O9JJpXCfnv+5O8SMVWCAFHSkx142HEheKJNYGdQnDaS6Yn0kdhydBY72YTCvokBb2hAJ7BWukUkWEwcUXIQlfRh0ire8FCoQ/+o4cq22nQuh1SET3EFX3oKnrR/kquCckq6JNslqP40zAT+oKxG0tQLCjadhRxmL+qbvIwOJ1zSg9IBcDIQnmaY5Zdbj5mp31J+TrZBCuin404zyt1bCgd2Kk0ruQUDwz2eRQRLHU3Cgou9ct4RHYliRkbnQRpEoFICssWyli8sBq7yg7oTYk/FASVS1Rh1qTn98IozGf7fhQU2PsmqbB7YZanYa8QM+VCOTBymO0XY+oEuzbXO588XS4kArclyYQq8hRmjo2Tvnw2KilhI/BTLsUOtqs5ZgoD0qWKMRAV8H6R4rnvXh92j/7z7vDkncLG3AdTR/4RUAB6olBVBEkRQ0aiTP1gFMuOVamuEl3r2iKKvp3Bloi7iJ8R5mrLg7LnfohyEff5AEqKHbkvzHnD9h1Oycjz+RqkyoNwMGhUvE3Oj3kaYAPcqAbHl6slcpCjpTzroCGdSVchZPX3/cPet1cp6zP4+tVrqFMGEtX6p4Ww4Pg77UZ755SnMY9KpwnrXyRFJrsv+FO45GM/H3Xj/ls+xOzH9dKTXedGrs+DcIwHZX1pbX3yBO9h3GRtnw9TXhlFSYy0xSWVbMFe1PZH+sV5N1HdaVfkd7CxxTKi/cM4JBsEgz/2p8KXw5eS15YLP0jDCTk51kMofLqwhGB5aIi6QkCxaF6Ngyv8s2a3DNfRVdtRkdnTTAcslA56B2lC07OxUdBUD54bJYV9qNvgsXXP8DwkqzxHo9Vf+Wi95TnYeMCru4bt6jrs11Q4VAgIXv5DWdZM0zJbrqfZqmOAAt1cx5fLgObzXfmS494Od7kwJmnFLY1JeVveGG3tWCYi2S4tNqjcZEIailXXMNzzGxm0Vw48+W/pwCp/uRwakO6KqBh0l33DnPiCeynyUQIqsQLACK6k9xEfwwyVAwqtqGXUFQlZooclPZq53lrKdnnDzyiVKghEr5UBmxCHH01Gfr3IEX1T1EwCqUREg76uTVgVnsUwMoKNam3DZnBauyhJolGgiF6ocWNngOWbMRiGHR3TI9jXcPkwlybxKmKhRSpk7ZLxKcV0jcD27l9gbwaDjOeSS4r8Kf9+5/I0QSVmwv87Fef+Q4oTppeEWW6X71CYpJW3JMwgGY/9uM9ikRc6TqLZMImFEGVSwscOH7lgH5v7PVwQou6Xgivyqj0HxdQB76moA7IUPrb8PTlVOcEK3ORUNTKamHW+98pHMLB4FQN7Sum+Ktqnwrdhv89FMkXuPq6Eu0qNrMZbI38LIVugX+wRa8Bb5cjrmZyrlTPjQ7qr6QmuUc/PpXdDA1lMERj6VRy5UsdKFROqBiVDzu/y3nh9fKAqEanzS9p+ZdIdLYeRp5xPKG54E79L/Tijt53N+HF9MftbI2ZpFcnZQMjbJOLe1oi4spY7mrSXOzrE/ZgE3TTBIpq+ZIDJ8sK+kh1eaYAv/vdpWyu2WDU06F3vuy67BiQo/agoa7SWgbQi3uxiA2IibV86tht5V7xA2NDYLoQr9EaXkLSclUpRjbMJrNUmNA3mvtO4xkuugug6H3nxw0YQ/bAmRFbL9BzHdTTTcFwd5yhWQNTcxdwxRjptbzH26iDoIREqFxHOwxBC3WWEftwIoR/XRgiHFgxPs1zk42F6nFJuDwdRZRCtx4dRuYpKjPaWMfppI4x+WhMj5FZdvPAwHLz3QIIGu6KHNnRyK4FTQI8NIYJmbudWrKKfN0Lo55UIlQw2EHJcy/BczTYMzzFNt5TMw0FExxUAkbE6Prk7iD61+z34UrIv95R8Ofyy5HXXyZXnX4i49PvJRb34ssR1K6txnXTTgXQxh9LFPJfZpBdLnub7MutEL0HRj06CoN8AF5wxGW6WfMJYX37yqb81W/ZGfrMKmB/Jbv3TMpbh0jbIeGszT4OtEXEzT09n17dHysOtkbJUZPHCjqT8yMzF9Tsq8nNwY+T14MaW/dzFLxvtqH5Zc0eltXCi2PFsQ7fxtZDr4YOYh85LGDj2R1Cajy+9V8YkL6qYZAmkXzcC6dfbBOlet70aVrRIwVZnJR8PRgQOMKKAERgdLmP020YY/ba1C4nOA1H26GGXkTCiYZIKqLJLWCHgXoLnd4nfxR8SwIs/ZUR/8ddGsP2+9suNx7a06NSzWFr1Ew+SUrokzz+2Vp4yh1odnX8c0vxzW6Wp4+sz2i/cd0b6k9L86zalea8mujwOdcVZoTsU5xSnZ/FJORJCFaN8Sud30LCr/OP7Isn/efL+4P07dtB91X0tK+Rp1wYQ9NRcVmKI9fcLJUBXpbs+e7eAo9BpfkwpKBmqGHh/bbk2Pvo2LddVpXnVjRZeJHiqg+/BdXxkR++Uq7TWoqTgb+YHIsUHPeWn8c/+D1BLAQIUABQAAAAIABwL1lL8yfIf/AQAAEYlAAAXAAAAAAAAAAAAAAAAAAAAAABnZW9nZWJyYV9kZWZhdWx0czJkLnhtbFBLAQIUABQAAAAIABwL1lKU9B6XXAMAAP8QAAAXAAAAAAAAAAAAAAAAADEFAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbFBLAQIUABQAAAAIABwL1lLWN725GQAAABcAAAAWAAAAAAAAAAAAAAAAAMIIAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzUEsBAhQAFAAAAAgAHAvWUuh3S6grDAAA7D8AAAwAAAAAAAAAAAAAAAAADwkAAGdlb2dlYnJhLnhtbFBLBQYAAAAABAAEAAgBAABkFQAAAAA=",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 1,'PC': 1,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>

                                </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>