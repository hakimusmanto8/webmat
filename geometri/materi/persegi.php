<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Persegi</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/persegi.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/persegi.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1660,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIANhmzlJAa9R8EwUAAPslAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9v4jgUwM87nyLyafdQSCABWjUddUZabaVOZ7StVns1wQRvjZ2NnQL99PNshyQU6EBgCqjDAec5/vt7z/azncuP0zFznkgqqeAh8houcgiPxIDyOESZGp710MerD5cxETHpp9gZinSMVYgCnbLIB1Kj4/s6DidJiCKGpaQRchKGlc4SIjEcMsoJcpyppBdc3OExkQmOyH00ImN8KyKsTFkjpZKLZnMymTTmtTZEGjehYNmcykEzjlUDQuRA07kMUf5wAeUu5J60Tb6W63rNf7/c2nrOKJcK8wgaAt0akCHOmJLwSBgZE64cNUtIiBJBuUIOw33CQvRNS87vw5SQP5CTZwJaLrr68NulHImJI/r/kQjiVJpB0Xk+IzR1Gnj9WTCROmmIul3kAFwd9EPUCgKAxpIRDpFrEzM8I6nzhKGEPAZnSkQmv4kdYibzgk1NX8SA2Dd+np5T0BLgdKQioA+34SFHJoQMoNUo7yM8gHpmRtOVEiMh0oF0piG6w3fImeXhsw1NEkPnnj7nlQbVWDVjebRp+2UzB7sZ4gFJCB9AogXOXi3OnZ7hrAPgrIPTxpxX+vMwd04E8wEgw1DflvJXXmXbqsXWa8HkAF0y4a/JYoHvDf+bxNDqKuX2L8p7prxoxX4tvuAVQH/g/63Y7pWsSWIZSv0P3o0YJ4xM9wjeekY5xFsjFNBb9byMKnTtlh3CnKHemtA1EItPjWj0yIkERw8spyhXP/xFB7CGmfoEeJJUQUlet2dLIP/zBaVR0BmFNK8rYpjxSPeqgPs5S5+q2mj77iH0UZZZewSsUcaupNezlCTWUsHlfi6Xpl3PsXvvpi0yxXTNN1zBBgyoQVvlUuceCUkeoKiv/CHFXOpd2KItrddcimevaS04Ba29N53NZy7+hNNCE1Wt1fON1q7dDTCDA6tui2m8CmJ3J+aozHdL29zJiDr1hn7L9VfTa3SP2IieoHuixPBPLpauwEk4Zkc2D67wpnGqiKSY/2hvwmZxZUR/m8uFPrpWH7u3cev9Y9A2Og2gtBf27bn25/nnrud14Bjg4E7K64gXdiKasY0oIVuX7e0hH63Tt55nJLg+A5/vJKxUkPRPYvrYxvndw6aNxoTbeVfCRYFrks0ggMzPWtI3DFPPyDMI4O2zDiDa5Iemp3TqXNsc1zbhdcsGbRv4NggKRPV2ika5CcxdFU/5xQLh19veHMN08q6V/gbeOs/GJK1MDndzuTCewE4PUF5WPUPaaDJYZyfrrUIyOgATGlNQ0hlob4zBDdDefl8Klim4pIO7L15e0lkzntCBGmn3Duoe0qk2F1umMxIpfRZcFTQcPQqumbnOWzjOWGU+rde81wVj3W2Cxjxm5Wi8tlKpAXtmbxK9PMpbpZgqQ2iIQdhptHptrxe03a7XPQ96nQ2Rer0SqX2xMdFl+wBr2N1CthrnoMDlcY7TqDwnBed5zXrjel0/aLfOW4F3fu7DA1Sy7+3gn0VEubU5xmM9YwFLSX/aiR0TUSbLc2grFYTAJE/AYdl85cLZlDKK09lutr4VYUWmpcPwYITK5wRHCHh9VwB7XDbtxkqVO3vbmSEFihy+9YBzBFMJ5Z9w9BinIuO5ZVeXob10PV8mjvFMoS8EI7DVnXfr01yu3BQvLfzrAOVr7SFHH3xTEz32xXRhrfrBVZcsR8CtESr3tytGwOa9XF7nzg5uCnXO57a6VFzjoFQV0Kx85tScf1N19R1QSwMEFAAAAAgA2GbOUjREtr6BAwAAUREAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzW7bOBCAz9unIHivJdmSEgVRCqN72AXaoote9spQY5u7MqmStGXl1foOfaYOf+LIbRLUQZKiRX3w8EczQ34zGok6f7Vbt2QL2ggla5pNUkpActUIuazpxi5entJXFy/Ol6CWcKkZWSi9Zramhbtyr4e9SZnnbox1XU15y4wRnJKuZdap1FQtFq2QQAnZGXEm1Tu2BtMxDh/4CtbsjeLMelsra7uzJOn7fnLtdaL0MkHDJtmZJlku7QQlJbh0aWoaG2do90C7n3m9aZpmyb9v3wQ/L4U0lkmOC8FtNbBgm9YabEILa5CW2KED3ICSgs/QR8suoa3p39LiXoG7JRK+0VvUj8o1nWVFSi9e/HHOldKNIWpXUyShhiCugugRLyILc9swtw1zfRjsw2DvBxNn0KxUT9Tlf+i4plZv0GtckO/4a3D6tWqVJrqmU/SAcctSlJcoqykGpO1WDC1OsjT8srxKs6zMpkG/ZQNosmVoNHplG6u4N+lHF6w10Zd3/lY1EGbyeL0UmBOOjLGA0UfnpgNofCvwxG1hKgw+q8b2MCM+2KEFYleC/y/BYDyLkZJr/CWaBlxyBh0QS5BbJKK0wWRKvZcBBV5+5XouC3eZ7w8ocPbKCRz2+rhULXZkHjTm4cL5NIhZEHkQxR4JfJRhncb917RjGvMXDXE3f57E7Pkmj9hOmNmf+6jNY3eUOunMp86xkcblIUr8xzC7uy4G+alCitnztEG9my+JbcBdf/50P25/Z3KmLRjB5Oj+fe0mviZf/gzkn5L73SDRvoQRv/e+f8AP6+qD+FWVBzjNUCJCL/c1qngsjLEUh8Ia6mqstXuTC+aectHLndXxNqiR5dFQVTusoNFK3nAdDd2gnUW0D7mTjg1HVsx8PIrwyBhl9CSPSIqqTPMyf7TYPDTFjyI713wl1tAAO0SLgX0utNMsPI7zE4/WiV+D7fsBK7LA6jDm+nwp60sGLr4KXKc/Ime//4F4HFktzPqQa/aMXMtQmgPXCns/Yb5KsPt9vnPtcV0tftfVY1h+3LDGv4PFrf5z3R8zDQn6mMWxzCv3Oymz4jTL8VDzSICe4rhx62HDDYYTxRDEFYpo8NjzB5mXQZwEcRpEdefZRKy7VnBh7w+t2egFnr5ve1mOU4dRzh8WZdS79XV5cvK9aX9j+FlemMdK977bJaOPB8n1l4qLL1BLAwQUAAAACADYZs5S1je9uRkAAAAXAAAAFgAAAGdlb2dlYnJhX2phdmFzY3JpcHQuanNLK81LLsnMz1NIT0/yz/PMyyzR0FSorgUAUEsDBBQAAAAIANhmzlKlDgyCwggAAEoiAAAMAAAAZ2VvZ2VicmEueG1s1VpZb+M4En7u+RWEntu2blkNuwe5dneAnpnGZHax2MU+0BJtsyNLgiQnTmN+/H5FUpKPHJ3YcyTdCiWyVKyqry4RmXy/WWXsVlS1LPKp5Qxti4k8KVKZL6bWupkPxtb3H7+bLESxELOKs3lRrXgztQKi7N7D0zD0fZrjZTm1kozXtUwsVma8oVemVjGfZzIXFpPp1Lrwr7wr3zkfXIaxO/ADOx6c+2E4uLo4GweXf4vPr+zYYmxTyw958RNfibrkibhOlmLFPxUJb9Suy6YpP4xGd3d3w1a+YVEtRhChHm3qdLRYzIYYLQYl83pqmZsP4Lvz9p2n3nNt2xn9+8dPep+BzOuG5wlEJgOs5cfv3k3uZJ4Wd+xOps0S5gpDaLwUcrGEScZBYLERUZWwSymSRt6KGu9uPSrtm1VpKTKe0/o7fceyTjGLpfJWpqKaWvZw7EWeH3txFMS2Fzq+xYpKirwxtI7Zc9Rym9xKcafZ0p3aMXBcizVFkc048WS/MYcFNi7mxOw9CyPMuMwJmI+ZMWYi5tFc4PjMY0TieMz3Mfo07YRYoWX8DgKbOQ5WmGsz12Wuw1wPj0HAApBF9K4L2jBW/GxcRA2JcHk053m41Jzn43LpDowCzQZyBF6o7gL1e0zvYJcA+/3G1BLm/Bjb0UQQOcyDJHiObAa+YA+JlTa+zei/w3zaxI2YO2aKq+Jvw0a3spazTEytOc9qctZ8XsH9uue6uc+EMqKZ6EFz3uMfKORXkAc2HEM7C1Zs+z1dIS6fFgiwLXQA6TY2gMKGbhDQhppqgAFpFojRo02GwaCUsG2CBUOgaaAgPUJJNWgaBR0G71gNW/28l+g33tIPdOQ5GMgpMHiM5MYN5KfBN4+hflTuZsNt9CyBjwG+BI86UhkY4xXKwATdrk21fnJTvd7v2e3oOO4LtjzOL7tN4SMAgs+m1tmnv1+d/3J2KIEbPKL0kbZ+0NLYS/1X18GW3ou03rf0a3YMd+LwNAr742/e3nERJ3/wnr4dR6cxMxWYfW+KaNfgcNeI/PAw4ekR2UGNp4E/fgb+yaitlhMjEauXRGtCuxEr9A02izwWqoSlyibqJeqFrp2Ry6KARZSu2gqKijdmIY2mjFIRHe+U0YCK7FYtDWkS9YqyG1NlUBdV12/rKu5VZaWqu1tZUQL9vgpCQGLlMIbazUJKmKYcQgq3K4guxEf9CxmKZuCykJLyI7URXVxRy86wS5GhwzMQKBvKvFw3O3ZLVtTmqNumADXPVHdm6NMiuTnvLG04CV6jh+rZoqHp2ybd4Ox0Ve8mGZ8JtJSLa3IDxm55RslO7TAv8oa1uTakuclIdXATsU4ymUqe/wu4t93ST+vVTFTwN9wWpKRiQq+zrtXz4u1WD72CokmKokqv72v4Cdv8R1R4O4zRBt7re98mT0w4ObEfD+PtH0T7vVlCjChu4vZaNA1UqhnfCDieMceiorDZevihPi+yfqosZN5c8LJZV6pvR8qvSNCzfJEJZR6FHNra5GZWbK51OQCwxOvX+5ICWEswW1wUWVExxJRL7SyYqRFFg0ZFQ6J1VDAKaPAbFEYLYtqtOzECGRRqBA2Nioo+BjRyWlXIp9U0XPhG1ipbgPeOoyjYqUde57L51D40MrnpNaUXNKjgrPxwl6chOZ7nZLTnUBPj6q17rYpUbLnmZLSzPrkRVS4yIlzXovYuNWkvcQJHlvm6WNd6RcurlvDCZ94sz/L0F7FAcH7mlBwbiLfPJBWJXOFFPW/sz8k3/gl19WwqFpVozaRF1OgY2VldVoKn9VII+L/BSHt/T6amJ6NWqQmajUyotL+SSB4DuMCKb1RDg4hBXlCRNqmTSpbk9myGFH4jesdOZU0sugmi3jG4d/lIFMJrTAzi7qu+GzhDuHIfc2yjAoQ6I9CY+wE+6R4ORp3PThSLB5F36O+mqJ3S3U/HkiA8DUs6IhDbzL45EcEPypLcBqHQNwXbcW0qgNmoKr5Q+Shy1ijLG3c9cCiKuhqiGFrZkAJUfRBnOOBYN8sCYsE7IThG8slMrPBJbvjm65Wo6PTDWIWrr31EyNrEydgUGIjKihkJtWfH3g5Y7g1ho0OlXEpfqsYQjGflktM5ATou9eP4se04IRoynWf5PZW2LjxN3fx5Pq9FwxCMHpUrMOvosfpjl7XaWggLgY0KY0CpohgFjM/qIls3OJlB8sj7kxmtjCmd6rsD9IGvdwrAYC43FNWaDuaUX5HouqylQDzTWWjbGH3NaJZwLZx3UOGgbtLgqW7+IdNUKDlNokPyU0i2OQe5TNDuuOtehBfeq3jtE83IoHqArwrxDqszhe4zWDJxS9/5B5BGaL+xKQ37gH4jfANXW9XbXu7xa6vpnhHgLodmwM2hGUx2rdVeSGbYagA3/NqHFVmDWgq9oYlNPXtQGnqTJsVqxfOU5aqj/6xM2jeT3MYhoaySTPz37D3j/zOWWzft4rnmZ7g8A9H5MRA5ZGCKOhpfCZKB6OEQOylEeqffAaFrsaD5PYxwhsAdg8YOPvOn8UEqVdxaBObPINR+JDwM0W4T+m3gHNi/tyH1Diq9wYQdZg+lHhj7kczDChxYywZsnMhkexgnI+B+yKlH04XosKu7EaKkvuDn/NeK5zWdne+2c0/FUHa/KPLHEGLc7Y3XA1XiLchLNAtNs8TgTS0qdf7U+oIhQDBiCKfW5XNhp0VoYdPMT2O/1zrHM1XzZU6i6tZ+f83XG5lJXt0/jtTT3r84yvufVPBl6u1p8mCGeTPR8bTNl39Rm29/+3V1N9o9OlBnB8BgOB7DgL4T46QN5ckVAzqoBS5+u+Pbw0W+JVycoee5bhiFboyDTmccjQGBKcEmQpw96Mzn3htE5ssbQkYHyKP4ABjXe7PA7Da3F8c0tyEMBVBoACY0vLCz1V8Ex8KFvx/Y+YFdqV/ez3r4Sjq6q33KlJd/qikH+IQjtbuW5LVZyX3AbuB7im8CdH790Yg6VTd/ePHx/1BLAQIUABQAAAAIANhmzlJAa9R8EwUAAPslAAAXAAAAAAAAAAAAAAAAAAAAAABnZW9nZWJyYV9kZWZhdWx0czJkLnhtbFBLAQIUABQAAAAIANhmzlI0RLa+gQMAAFERAAAXAAAAAAAAAAAAAAAAAEgFAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbFBLAQIUABQAAAAIANhmzlLWN725GQAAABcAAAAWAAAAAAAAAAAAAAAAAP4IAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzUEsBAhQAFAAAAAgA2GbOUqUODILCCAAASiIAAAwAAAAAAAAAAAAAAAAASwkAAGdlb2dlYnJhLnhtbFBLBQYAAAAABAAEAAgBAAA3EgAAAAA=",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 1,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
<br><br>
<iframe src="https://drive.google.com/file/d/1755rZGV1wwJk65VWg4p_3ZHWUdW9scll/preview" width="100%" height="480" allow="autoplay"></iframe>                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>