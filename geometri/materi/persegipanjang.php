<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Persegi Panjang</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/persegipanjang.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/persegipanjang.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1660,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAA6PzlJAa9R8EwUAAPslAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9v4jgUwM87nyLyafdQSCABWjUddUZabaVOZ7StVns1wQRvjZ2NnQL99PNshyQU6EBgCqjDAec5/vt7z/azncuP0zFznkgqqeAh8houcgiPxIDyOESZGp710MerD5cxETHpp9gZinSMVYgCnbLIB1Kj4/s6DidJiCKGpaQRchKGlc4SIjEcMsoJcpyppBdc3OExkQmOyH00ImN8KyKsTFkjpZKLZnMymTTmtTZEGjehYNmcykEzjlUDQuRA07kMUf5wAeUu5J60Tb6W63rNf7/c2nrOKJcK8wgaAt0akCHOmJLwSBgZE64cNUtIiBJBuUIOw33CQvRNS87vw5SQP5CTZwJaLrr68NulHImJI/r/kQjiVJpB0Xk+IzR1Gnj9WTCROmmIul3kAFwd9EPUCgKAxpIRDpFrEzM8I6nzhKGEPAZnSkQmv4kdYibzgk1NX8SA2Dd+np5T0BLgdKQioA+34SFHJoQMoNUo7yM8gHpmRtOVEiMh0oF0piG6w3fImeXhsw1NEkPnnj7nlQbVWDVjebRp+2UzB7sZ4gFJCB9AogXOXi3OnZ7hrAPgrIPTxpxX+vMwd04E8wEgw1DflvJXXmXbqsXWa8HkAF0y4a/JYoHvDf+bxNDqKuX2L8p7prxoxX4tvuAVQH/g/63Y7pWsSWIZSv0P3o0YJ4xM9wjeekY5xFsjFNBb9byMKnTtlh3CnKHemtA1EItPjWj0yIkERw8spyhXP/xFB7CGmfoEeJJUQUlet2dLIP/zBaVR0BmFNK8rYpjxSPeqgPs5S5+q2mj77iH0UZZZewSsUcaupNezlCTWUsHlfi6Xpl3PsXvvpi0yxXTNN1zBBgyoQVvlUuceCUkeoKiv/CHFXOpd2KItrddcimevaS04Ba29N53NZy7+hNNCE1Wt1fON1q7dDTCDA6tui2m8CmJ3J+aozHdL29zJiDr1hn7L9VfTa3SP2IieoHuixPBPLpauwEk4Zkc2D67wpnGqiKSY/2hvwmZxZUR/m8uFPrpWH7u3cev9Y9A2Og2gtBf27bn25/nnrud14Bjg4E7K64gXdiKasY0oIVuX7e0hH63Tt55nJLg+A5/vJKxUkPRPYvrYxvndw6aNxoTbeVfCRYFrks0ggMzPWtI3DFPPyDMI4O2zDiDa5Iemp3TqXNsc1zbhdcsGbRv4NggKRPV2ika5CcxdFU/5xQLh19veHMN08q6V/gbeOs/GJK1MDndzuTCewE4PUF5WPUPaaDJYZyfrrUIyOgATGlNQ0hlob4zBDdDefl8Klim4pIO7L15e0lkzntCBGmn3Duoe0qk2F1umMxIpfRZcFTQcPQqumbnOWzjOWGU+rde81wVj3W2Cxjxm5Wi8tlKpAXtmbxK9PMpbpZgqQ2iIQdhptHptrxe03a7XPQ96nQ2Rer0SqX2xMdFl+wBr2N1CthrnoMDlcY7TqDwnBed5zXrjel0/aLfOW4F3fu7DA1Sy7+3gn0VEubU5xmM9YwFLSX/aiR0TUSbLc2grFYTAJE/AYdl85cLZlDKK09lutr4VYUWmpcPwYITK5wRHCHh9VwB7XDbtxkqVO3vbmSEFihy+9YBzBFMJ5Z9w9BinIuO5ZVeXob10PV8mjvFMoS8EI7DVnXfr01yu3BQvLfzrAOVr7SFHH3xTEz32xXRhrfrBVZcsR8CtESr3tytGwOa9XF7nzg5uCnXO57a6VFzjoFQV0Kx85tScf1N19R1QSwMEFAAAAAgADo/OUjREtr6BAwAAUREAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzW7bOBCAz9unIHivJdmSEgVRCqN72AXaoote9spQY5u7MqmStGXl1foOfaYOf+LIbRLUQZKiRX3w8EczQ34zGok6f7Vbt2QL2ggla5pNUkpActUIuazpxi5entJXFy/Ol6CWcKkZWSi9Zramhbtyr4e9SZnnbox1XU15y4wRnJKuZdap1FQtFq2QQAnZGXEm1Tu2BtMxDh/4CtbsjeLMelsra7uzJOn7fnLtdaL0MkHDJtmZJlku7QQlJbh0aWoaG2do90C7n3m9aZpmyb9v3wQ/L4U0lkmOC8FtNbBgm9YabEILa5CW2KED3ICSgs/QR8suoa3p39LiXoG7JRK+0VvUj8o1nWVFSi9e/HHOldKNIWpXUyShhiCugugRLyILc9swtw1zfRjsw2DvBxNn0KxUT9Tlf+i4plZv0GtckO/4a3D6tWqVJrqmU/SAcctSlJcoqykGpO1WDC1OsjT8srxKs6zMpkG/ZQNosmVoNHplG6u4N+lHF6w10Zd3/lY1EGbyeL0UmBOOjLGA0UfnpgNofCvwxG1hKgw+q8b2MCM+2KEFYleC/y/BYDyLkZJr/CWaBlxyBh0QS5BbJKK0wWRKvZcBBV5+5XouC3eZ7w8ocPbKCRz2+rhULXZkHjTm4cL5NIhZEHkQxR4JfJRhncb917RjGvMXDXE3f57E7Pkmj9hOmNmf+6jNY3eUOunMp86xkcblIUr8xzC7uy4G+alCitnztEG9my+JbcBdf/50P25/Z3KmLRjB5Oj+fe0mviZf/gzkn5L73SDRvoQRv/e+f8AP6+qD+FWVBzjNUCJCL/c1qngsjLEUh8Ia6mqstXuTC+aectHLndXxNqiR5dFQVTusoNFK3nAdDd2gnUW0D7mTjg1HVsx8PIrwyBhl9CSPSIqqTPMyf7TYPDTFjyI713wl1tAAO0SLgX0utNMsPI7zE4/WiV+D7fsBK7LA6jDm+nwp60sGLr4KXKc/Ime//4F4HFktzPqQa/aMXMtQmgPXCns/Yb5KsPt9vnPtcV0tftfVY1h+3LDGv4PFrf5z3R8zDQn6mMWxzCv3Oymz4jTL8VDzSICe4rhx62HDDYYTxRDEFYpo8NjzB5mXQZwEcRpEdefZRKy7VnBh7w+t2egFnr5ve1mOU4dRzh8WZdS79XV5cvK9aX9j+FlemMdK977bJaOPB8n1l4qLL1BLAwQUAAAACAAOj85S1je9uRkAAAAXAAAAFgAAAGdlb2dlYnJhX2phdmFzY3JpcHQuanNLK81LLsnMz1NIT0/yz/PMyyzR0FSorgUAUEsDBBQAAAAIAA6PzlKdp5DXQwkAAOAoAAAMAAAAZ2VvZ2VicmEueG1s3VrrcuO2Ff69eQoMf68k3inuSJvxrW1mnGQnTjuddjodiIQkxBTJkJQt7eTh+x0AlETJl2itbuzsWgYIHB7gnO/cAHn07WqRsTtR1bLIx5bTty0m8qRIZT4bW8tm2hta3378ZjQTxUxMKs6mRbXgzdgKiHLzHp76oe/TGC/LsZVkvK5lYrEy4w29MraK6TSTubCYTMfWhX/lXfnOee8yjN2eH9hx79wPw97VxdkwuPxLfH5lxxZjq1p+yIsf+ELUJU/ETTIXC35dJLxRq86bpvwwGNzf3/fb/fWLajbAFurBqk4Hs9mkj9ZiEDKvx5bpfADfztv3nnrPtW1n8M/vr/U6PZnXDc8TbJkUsJQfv3k3upd5Wtyze5k2c6grDCHxXMjZHCoZBoHFBkRVQi+lSBp5J2q8u/OopG8WpaXIeE7z73SPZRvBLJbKO5mKamzZ/aEXeX7sxVEQ217o+BYrKinyxtA6Zs1By210J8W9Zks9tWLguBZriiKbcOLJfmMOC2x8mBOz9yyMMOIyJ2A+RoYYiZhHY4HjM48RieMx30fr07ATYoam8TsIbOY4mGGuzVyXuQ5zPTwGAQtAFtG7LmjDWPGz8SFq7Agfj8Y8Dx815vn4uNQDo0CzwT4CL1S9QP0e0jtYJcB6vzE1hTE/xnI0EEQO87ATPEc2A1+wx46VNL7N6MdhPi3iRswdMsVV8behoztZy0kmxtaUZzUZaz6tYH6b57pZZ0Ip0QxsQXPe4z8o5GeQBzYMQxsLZmz7PX1CfHyaIMB20AGku9gAChuyYYM2xFQNFEijQIwebVIMGiWEbRMsaAJNAwHpEUKqRtMo6NB4L5Wwlc87Rr7hjnygI8tBQ0aBxmO0b3Swf2p88xjqR2VuNsxGjxL4aGBLsKgXCgNlfIEwUMFm1aZaPrmont+uuVnRcdwjlnyZXW4WhY0ACD4ZW2fXf706/+nscAdu8IjQL9T1g5rGWupHfQ6W9I6Sel/TX7Ji2PHD0wjsD3/38o4LP/nKa/p2HJ1GzSEsZN+aIlo1OFw1Ijs8DHi6RXRQ7Wngj5+BfzRos+XI7IjVc6I1rt2IBeoGm0UeC1XAUmkT+RL5QufOyGVRwCIKV20GRcYbspBak0YpiQ47aTSgJLuTS0MaRL6i6MZUGtRJ1fXbvIq+yqyUdbuZFSnQ32ZBbJBYOYwhd7OQAqZJh9iFu0mILraP/BcyJM3AZSEF5UdyI6q4opYbxc5FhgrPQKB0KPNy2XT0liyozFHdpgA1z1R1ZujTIrk932jacBK8Rg21ZYuCZls26QKnU1W9G2V8IlBSzm7IDBi74xkFO7XCtMgb1sbakMZGA1XBjcQyyWQqef4P4N5WSz8sFxNRwd7QLUhIxYReZ5tSz4t3Sz3UCoomKYoqvVnXsBO2+peo8HZgR/2h7Q3DADWbH6OOXZuJ2O+7gRv4ceDGYWBTYE442Tcq5ygKhsNhMLSR3CIEgvUjU7FeV9zdiKaB8DXjKwETNYqbVeRgOw/f1edFth0qC5k3F7xslpWq8LGHikQ6y2eZUIpUGKMATm4nxepGJw6YAPH6eV2Sq+sdTGYXRVZUDN7nUuELZqpFeqFW0dDWNlRQH2jwGxS2niemm3knhsuDQrWgoVZR0bFBY6xFxf60mIYLX8laxRXw7piUMhCqppe5bK7bh0Ymt1tJ6QUNPzgri+3yNCQv5zka7JneyDhFa4iLIhU7RjwadOZHt6LKRUaEy1rU3qUm3e44gcnLfFksaz2j96um8MIn3szP8vQnMYMbf+IURhtsb59JKhK5wIt63Oifk238HeLq0VTMKtGqSW9Ro2P2zuqyEjyt50LAUwxG2k+2ZGp4NGiFGqEsyYRKEAuJMNODCSz4SpU+8C1EEOWTozqpZElmzyYI9rdia9iprInFZoCoOwr3Lh/xV1iNcVD0Putez+nDlI0LUgZbKQehGgo0pt/D4e9hZ9SR70S+eOB5h/Zu0t8pzf10LAnC07CkywSxy+x3ByLYQVmS2cAVtuXDrl+bXGEWqopfKNEUOWuU5o25HhgUeV2NrRha2ZAAlKfgZwwH8l/gO7gSWTbzAtuDlUIAtGSbmVjgEG/458uFqOi+xGiHq/sBeMrS+MvQpCRsmRUT2tyePrf6wPRezHXoZEsRNYCJ8aycc7pXQIWm/jl+bDtOiBsCHW35mlLhxklNnv1xOq1Fw+CSHmUn5Qnb2e83savNndAT2Chnbn0Z7/FJXWTLBjc5CCH59iZHi2JSrTqngF7nTpSLYDCVK/JtTQdlys8Id5vYpaA807FoVxXbzNHMYWC4H6H0QdWnQVV1/ibTVKjaxoQ7hECFp448ZL+loNVhmZsXYYtr5bXbcDMwmD6L7mQfXZPUXwW6EBI6d8yWnkcX9ApdYHUkujh50Ep07Hlj8CbFYsHzlOXqNPJdTqkUoClYdTHM7bG1OkMIhFacsbVWXQ3gsmkJzjRfw+3AbFR+2JjAmeL+jIkwcUfXSQeWEkLHEIYaWAo1raE8aRZb4HfTeE3wUdbUGXMbNWm/VDHqV8w29OhB5n9MmZ+U0F1FXsgqycS/z94z/p8DJZ4fo8TzlyjRcXV2Ua3JLl+ox30jhL8emiE6h2a4i4EOxE9hYNLfkRjciBmN76GA6yIyZaXvDgLTpxFALlTcWh1Pn8GgPQ8+DEL3FHEiM4YiocL2lYeiCpB+JKiwAl9NyIYiGc5trW4ygm0TGAD0YVV+K0RJdd2P+c8Vz2v6lqRbjn+pk0wOneTiGCe5+FM5SdDHlw6xYwdx6ISeHYseXWYCLiSsr+k3CoIOKLPj/Gb2mvymp0uzh5X7pnzpmr6U7CJ2rhFT+HQQmz+NmP5+08AxP4Sre4XwqvBCjX0iwMSvuWagbmfHllyUmQTN0ShcaBRUdumgII9AQb4qFHazTe+PVvljZetc611puaP3y2OSyOXrKleDSAf90Nfab5V/opo1W8+K/LF6iXGX9MG491AW+BWQEh3/L3VAOVEd0OLvFriPv6V4Tu168Vax4Hcaq3pZpnn4VPr0KfQAP3Us3L+j5MuVzCSv1o9H+KcTKWn6/yTgkQVOV5IHy5e3UqA+rXKy6TejcpMr9bnqdFH6qys9fTsq11bei/px999bhyB5OxBoo++9yYoeyXF7Da++6zV/Dvjxf1BLAQIUABQAAAAIAA6PzlJAa9R8EwUAAPslAAAXAAAAAAAAAAAAAAAAAAAAAABnZW9nZWJyYV9kZWZhdWx0czJkLnhtbFBLAQIUABQAAAAIAA6PzlI0RLa+gQMAAFERAAAXAAAAAAAAAAAAAAAAAEgFAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbFBLAQIUABQAAAAIAA6PzlLWN725GQAAABcAAAAWAAAAAAAAAAAAAAAAAP4IAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzUEsBAhQAFAAAAAgADo/OUp2nkNdDCQAA4CgAAAwAAAAAAAAAAAAAAAAASwkAAGdlb2dlYnJhLnhtbFBLBQYAAAAABAAEAAgBAAC4EgAAAAA=",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 1,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
<br><br>
<iframe src="https://drive.google.com/file/d/1WPHMeZZsCrsbj3gfcKtLA9nlu0tqQdFs/preview" width="100%" height="480" allow="autoplay"></iframe>
                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>