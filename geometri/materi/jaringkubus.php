<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Jaring-Jaring Kubus</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/jaringkubus.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/jaringkubus.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1660,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 | 1 501 5 19 , 67 | 2 15 45 18 , 7 37 | 514 3 9 , 13 44 , 47 | 16 51 | 551 550 11 ,  20 22 21 23 , 55 56 57 , 12 | 69 | 510 511 , 512 513 | 533 531 , 534 532 , 522 523 , 537 536 , 535 , 538 | 521 520 | 36 , 38 49 560 | 571 30 29 570 31 33 | 17 | 540 40 41 42 , 27 28 35 , 6 , 502",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIANdizlJDJ5qJEQUAAPklAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9z4jYUwM/dT+HRqT0EbLCBZOLsZHem08xksztNptOrMMKoEZJryQHy6fskGdsEnAWTXWBSDshP1t/fe5KeJF9+nE+Z80RSSQUPkddykUN4JEaUxyHK1PhsgD5efbiMiYjJMMXOWKRTrEIU6JRFPpBaPd/XcThJQhQxLCWNkJMwrHSWEInxmFFOkOPMJb3g4g5PiUxwRO6jCZniWxFhZcqaKJVctNuz2ay1rLUl0rgNBcv2XI7acaxaECIHms5liPKHCyh3Jfesa/J1XNdr//3l1tZzRrlUmEfQEOjWiIxxxpSER8LIlHDlqEVCQpQIyhVyGB4SFqJvWnJ+HaeE/IacPBPQctHVh18u5UTMHDH8h0QQp9IMis7zGaGt08Drz4KJ1ElD1O8jB+DqYBiiThAANJZMcIhcm5jhBUmdJwwl5DE4UyIy+U3sGDOZF2xq+iJGxL7x8/ScgpYApyMVAX24LQ85MiFkBK1GeR/hAdSzMJqulBgJkY6kMw/RHb5DziIPn21okhg69/Q5rzSoxqoFy6NN2y/bOdjtEI9IQvgIEq1w9hpx7g0MZx0AZx2cNua80h+HuXcimA8AGYb6rpS/8irbTiO2XgcmB+iSCf+fLFb43vA/SQytrlLung7lk2C8asN+I7rgE0B/4P8kyZoklqHU/+DbiGnCyPwNwVu/KId4a4QCeqeZj1GFrp2yQ0wZUG9D6BqIxacmNHrkRIKbB5ZTlKsf/qAjWMFMfQL8SKqgJK8/sCWQf/mK0ijojEKa1xUxznike1XA/ZylT1VtdH33EPooy2w8AmqUsS/pepaSxFoquNwv5dK0m7l17920RaaYrvmGK9h+ATVoq1zr3CMhyQMU9ZU/pJhLvQdbtaV6zaV48ZrWglPQ2nvT2XLm4k84LTRR1Vozz6h27W6BGRxYdTtM41UQ+zsxR2W+O9rmXkbUazb0O66/mV6rf8RG9ATdEyWGv3KxdAVOwjE7snlwgzeNU0Ukxfx7exO2iCsj+ttSLvTRt/rYv4077x6DrtFpAKW9sG/PtT/PP3c9rweHAAd3Ul5HvLIT0YxtRAnZumw/H/LROn31PCPB9Qn4cidhpYKkfxLTxy7O7xts2mhMuJ13JVwTuCbZAgLI/Kwlfb8w94y8gADePusAok1+aHpK5861zXFtE153bNC1gW+DoEDUbKdolJvA3FXxlF8sEH6z7c0xTCfvWuk/wVvn2ZSklcnhbikXxhPY6QHKy6pnSFtNBnV2Um8VktERmNCUgpLOQHtTDG6A9vaHUrBMwRUd3Hzx8orOmvGMjtREu3dQ95jOtbnYMp2JSOmz4Kqg4ehRcM3MZd7KccYm8+m85r2uGOt+EzTmMStH47WVSg3YE3uT6OVR3ibFVBlCQwzCXqsz6HqDoOv2vf55MOhtidQblEjti62JrtsHWMP+FrLTOAcFro9znEblOSk4zzXrjev1/aDbOe8E3vm5Dw9QyVtvB38vIsqtzTEe6xkLWEv6w07smIgyWZ5DW6kgBCZ5Ag7L9isXzuaUUZwu9rP1nQgrMi8dhgcjVD4mOELA9V0B7HHZtBsrVW7sbWfGFChy+NIDzhFMJZR/wtFjnIqM55ZdXYbepOv5MnGMZwpDIRiBre6yW5+WcuWeeG3hrwOUr7WHHH3wRU30OBTzlbXqO1ddshwBt0ao3N5uGAHb93J9nTs7uCk0OZ/b6VKxxkGpKqBd+cipvfyi6uo/UEsDBBQAAAAIANdizlJI+v/cfAMAAEcRAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWztmM1y0zAQgM/wFBrdie3EdutOXSYDB5ihnTK9cFVlJRE4kispcdxX4x14JlY/TR1oOk0nzaGQQ9aSvLvSt+uV5dP3q3mNlkxpLkWJk0GMERNUVlxMS7wwk3fH+P3Z29Mpk1N2rQiaSDUnpsSZvXOtB61Bnqa2jzRNiWlNtOYUo6YmxqqUWE4mNRcMI7TS/ETICzJnuiGUXdEZm5MvkhLjbM2MaU6iqG3bwZ3XgVTTCAzraKWraDo1A5AYwdSFLnG4OAG7G9rtyOkN4ziJvp1/8X7ecaENERQmAsuq2IQsaqPhktVszoRBpmsYLEAKTkfgoybXrC7xZ2FgrYzaKSK6UEvQD8olHiVZjM/evjmlUqpKI7kqMZCQnRe3XrSAF5D5saUfW/qx1ne2vrN1nZE1qGeyRfL6OzgusVEL8Bom5BruHhj+IGupkCrxEDxA3JIY5DXIYggBqZsZAYuDJPa/JC3iJMmTodevSccUWhIwGryShZHUmXS9E1Lr4Ms5P5cV8yNpuF9wyAlLRhsG0QfnumGscleeJywLUqFzWdW3BxlxZbqaITPj9IdgGuKZ9ZTsxSdeVcwmp9dhfMrEEohIpSGZYuelAwG339qWzcJV4todCBi9tQK6nT5MVfEVGnuNsb9xPPRi5EXqRbZGwm6En6e2/yVuiIL8BUPUjp9GIXv+yiOy4nr0cR21cWj2UiceudTZNdIwPUAJ/xBm+9SFIL9USCF7Xjao2/micM1g1b9+Po7bPZmUKMM0J6L3/H6wA3+Sz/+T344S7AvWI3jp2hsEobI+i2BROITDBCRAdHJdpbJ9gQzF2JdWX1lDtV2bnBC7zwUvwHk7xMBuZ4iy7masUlLcc+x13aMcBZTPeXZ2xZ9kI8c/85tEL4cHadgisiKP0zzdWyyeu0PsRHas6IzPWcXIJlrY6A6Fdpj4DTg9cmiteB1sLzuowRyqQZ/r4VLWlQiYfOG5Dl9Nzl4qruebVJMDUs19IfZUC2gdnOrTt7ftXAUz65Ve2Ot+Xc3+pbq6D5o3C1K5966w2K937T5Vn6T7LI95WtjfUZ5kx0kKB5k9IXqJI8aDBwzb6U8RnRe3IILBXc8caJx7ceTFsRfF1vMInzc1p9w8Hlq9UBM4cT/0ghyGNqOcPi/KoPfgK/Lg6KmJf2/4QC/JfaVH3++i3ieD6O77xNlvUEsDBBQAAAAIANdizlJFzN5dGgAAABgAAAAWAAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc0srzUsuyczPU0hPT/LP88zLLNHQVKiu5QIAUEsDBBQAAAAIANdizlKKIoVsMQ0AANBuAAAMAAAAZ2VvZ2VicmEueG1s7V1bc+K4En6e/RUqnhNi+YLNFuwWIXPJdbKTmalzztY+GFDAO8ambJPL1P7487UkgwlJIMGZMFtOQmTZstT9qbsldcum9fvNOGRXIkmDOGrXeN2oMRH140EQDdu1aXa569V+/+2X1lDEQ9FLfHYZJ2M/a9ccKjm7D7l6w7bpnD+ZtGv90E/ToF9jk9DP6JZ2Lb68DINI1FgwaNfedgxj3+h0dvn+Pt+1D7rWrse773Zdp9k96LiWYXW7NcZu0uDXKD7zxyKd+H1x0R+JsX8S9/1MtjrKssmve3vX19f1nL56nAz3QEK6d5MO9obDXh1pjYHJKG3X9MGvqHfh7mtL3mcaBt/7z+mJamc3iNLMj/ogmQCYBr/98qZ1HUSD+JpdB4NsBLgaDXA8EsFwBEg8x6mxPSo1AS4T0c+CK5Hi3kJWcp+NJzVZzI/o+ht1xMIZYzU2CK6CgUjaNaPOPbfpcM9wTdNrcqfRqLE4CUSU6cJcN7qXV9e6CsS1qpeOZJO20XTRX0Ea9ELRrl36YUpdEV0mAHeWT7PbUPR8NJslU+TnFPEdvmOhQPAdpU2OQ4UErhjGDn1cfBwHcBAxxZZrLIvjUNZqsH/+YaZhGmyHEq4SU501VNawVGKqxFaJo8rY6k5bFbVVGVuVsUHU+gzqEwsc5vxZBti4y18DH5su3OHPK/CHcpz9w5CAJ0oslYB6SmyVNFTiyoQDE8p5KmkiQQdvxMesn57EB7iftap6/wmNzpvk5vpNougGjM7btKB3md9r1zon79/uf+osU2A6DzC9IdYzjWjiMG8Ubck/+Vlq0noS10t6OG+xAPTjLTYWVLAchm2vIFqPN89NqMgPbtPFqXusjkqhojItpyOaRSV7nCqHlKNIFuPMgcVwGG/ChDXIIpiMO8zGGQ9nXGbROYfbzGJUhFtMWjtbGo8GrtBl/IflZZysIhhjsKhg0iRD6jjMQTGX7iWb1CAD48DoOLI0KMKHrJRjWfjIc5aND5lWBxU5qhrQ4VgNeeTI/2SvHLTiEKxMXsI5u4nm6ITjcmaBEuRdg6FeVA+KJTcw4vTHmbLfLjM9JmuV9RvFbnmyIZp1i7fS9rX28nGypfuEpSMqrRvNxBhThqqXHuylSZwGM9RHIsS8T/eHhDeIJtNsAdL+mCY/8jCLUdoP5ZxNlx/E/W/7s07QNQk/xcxqXi1mOfPJlJr1LMy13rRCvycw0RxekJAwduWHNFTIFi7jKGNaPmD5ZXVyXtcS034YDAI/+gqRyKdQZ9NxTyQQRhzGxKSshG5n+QRQDnez+R9AkUX6cZwMLm5TSBC7+Z9IcLPluXXHdqyma6BXcFRjt+oK5w2zbmMA4Y5ncaNJw1rfJ+G3jLqLCULDxSTBcpsGDXO3919T3LxpiasLkWXgPmX+jYD4auSGCdmfQuYw3Y/D+alJHERZ159k00RO/EFEQkx1omEoJJKykzEv7n/rxTcXagjC4El1fb6dEEWKgt6wG4dxwqCcJs2HUZlMMTpTKssQabNS0EaUwX+UMNR1qnR2ndNAhxIyRRlKZSlaTahOVqyCPsWmrsW/CVJpdlH3gkxJCaFJ9jQKspM8kwX9b3NO6QbV/6hZiuxinbrI5nW29u7I3lqyqCFYkEVuF2TRNXR/3COLJp9Ln8WxdCv+oE+1iNFcPhc3x1gspXVhSdyUclfSNrd5pQrbhlUuy5q2wLmkjeOBUBZTd3A/DkN/koqBOit5be0t3NX6JpJIhFR6morUOlBF52z0YXWDaBpPU3VFCYG8hBvO/WzUiQafxBAjyblP43wGou9WMhD9YIwb1XndJz7JyxeAoM4OxDAROXaKRNVjmnaWThLhD9KRENAQ3W9KP4rFJIs5Uy2sK0IhZ3DjACPdLsRi7N/ItQt0CoOYRKqV9pNgQprAepiMfBNzYR8EKVUxO0GlF7rBOnhAT+GMMTzLbDQbLrcxSkBNtNbumnVuG3bDbvCma5tew4FyfNfXeL3pNawmhy/FxKq40cT0Ox81eN20Go6L36bhcdfDyuBGah3us4HQ9zxjwZmTK9ALDypLSv1zGO5CnSQLJdVJ/jKxUNvadg4iNZmQBEKr5lPlIl165qNbSuK/adoURyyT6GvJX5JNUuAUFOqyQUYcHPnUp7t/y4QdT3vTFK6/aTaKQSKEHlwgJVEPxRi+Kt2GFIkZQB3pBSMSWdwjYu4iqOyduCIvkiQaxWZAuHBoAQdKNAzMDycjf6baoX9LQ+ZMq/Xc8HRm5HLTHcG0SOaURsPlRrZCkM7iSFGOA/TNrRTkQsdItU0ZDAJZhlspC9/nGBO7NHyp9nRHqbNLJkcjdT9mFpyYGpT9fyFqlAA2WrI9Cl4RpX48HvvRgEVyaXguJWu+7vCNdq0bJP1Q/Lm/ww4C5cr9s7PD9v/aYRdiSLWo7F8apWk2u1G1pVtY2SPdjXqEm0qhZfp6feJPb4Iw8JPb5fE776yy+qo77Yk7XQWfnc9JtplvavwXegSLRLp+oK6/RWK1a++Q2O3aeyROu/YBSYNY6ovOfpdKujp38IFu8PJr7yjXVLn97nuqhaMJynYPPlBtHE1R9u2791QrR5NiMERFlEHDMkO0cjRPmX1EK3wOIigj2+YgRRajxjgoocyBrA2EUOatzIAOmSEqTFAha5MZ0ECZd0SQqSnoyoymALStENPwdiQGSRzNBNGXsM/nstkIgxKiBhiDyIeqxwF58CEYDIRc26ulxjOl2yQXKw1XyuA0kctluw5XjPxxmg2azGjzuK6wXwY3+by0MMFd04gebKSyDUypwBMl4ImS5+nrXdVarVmSmPXZfLtlbC4Mkph8lsXnu+3kU5vKEhl9v2WMvhSfH1bxmfsHX0NBN+IzvB3GUYHT2Xj1DMvMYoTFgwyEYS230k4/DpnyjMBLrn0jZZnpO/OK+SL4qRjRKF5htEKOMLepMHoUIznjqzB6FCM5Da4wehQjuTaoMNJspmpBXcBIL5hK4G+Ju3KYed46CGvSkNbOhxG5qZUDbdmx/U2ICfk0P0afEz9KaQfcuiuVB5Bc6fWpkFwPSSzVKyRLQVLPWSskN9fulev0Csn1kISHrUKyFCThnqyQLAfJld6pCsk1x+4KyZKQRFShQrKcWVCFZElI6vV0heQTkIymY5HQg1YaoZ6EEDdPc7bqnl78r8R1XUDrcDioXYB208D2VeyBXQ/iPJSI/SUoKPdToVq5nQp+Eb+XxuE0w1Ne2MUVzZ/yUhENvckRjwJhyxI9/iD98bRHUcYBZxxhA0rwHTvOZvDJLTAdtR2sGNa+tzu57saH+nM5wI6tkEsRdty7HGF/OBx+Ju5uXNDR7t5SGBwdTRcOVRj8SIXBj1UY/ESFwU9VGPxMxb8/qsD3uYp4/6FD3Z90jPtCB7c/67j2Fx3S/qqj2eTnOjw6ppopni2zJ2fUAkW01dVPslIdXD86/izr1NH145OvVCXFtSl7evaRCMkj24dUbx7ZPiRm8sj2EbFk6ti6bN7UsfUT4svUsfVTmdGx9UMiy9Sx9UMiytSx9T+IXUvH1o9kRlNwRMRamoILQsHSFBzLTE4BMWFpCr4QOJam4ERmNAVnBLalKZCcWpqC0/PHI/URJCDXxX45IfqFraKP203uYGceRNWBAJfoRlw/2He4bPefQP4LR6lLjMYfbSefa2zoeSKjx1vG6EvxebKVfJbO5umWsanl1ql7XtN2mw2PYzuzS48uEN/1pmW5Hrcdx8ZmZ5NjplAWEGdbBsRr4fBxK3Fo1vGAlWM4ruth+zp3FQ5m3bWxSd6yHdvy8Bg97UArC4fzLcNBK8aPB+KPbQPiHp2YW8YX1IxPPwcQ+VD4gkhcbBsSehLw463l5y1D4tWA+LJlQLyabnz9KYB4CRzu20kjPQwvsJPmCZDOHqoysLcToNJSeMv2G0nHyyujROgUUFp4UmVbYCKH1OvClPtTik/obRtM0lH3IEyg/8fp3OtvXrsfIum8fF1J2p79ffdDJB262yFFubJJ5duybZBwdL+Mf/ZFQ1sPd99rBgkRJ/gJsaQ3cG0flgiz/IRYbqdc6insZpb+x8autxNIRPgqoSwJSwRIKyzLGng2i4JUWBax3MxfXGFZwBK7Gyosy5oQVViWh+VmAYAKywKW2JpUYVnWRL3CsjwsN4voVFgWsMS+wgrLstaQFZalYYldrRWWJWGpveUVlmX4Niosn4Flb5plhffBqSxfgeT8oYlNXZYfLy9TkdGWA9q2gPiP5W78PsN7n7Z45H2G+FIg9bZedeEi8xO8klY39Gdvh0rqV0LKF8aneMDmciZO9A7VU/XK1/w98ur7CGZE9/HGWyJZ1n5+0vnvM3rEfIUeoe/MoIgcNnhsW5fIOkrrk4vPsMMro5133pzYSfqjYCwGwo9e4h2KK4KcnN6TjEvqJYqUlBXivNOdz3maaSV257d4rXcweA3csFkJZNN3X9FGjBIj6M9Wg6chlwTp+DVwe6mXdpaDGx6kmr+SmvL517/99n9QSwECFAAUAAAACADXYs5SQyeaiREFAAD5JQAAFwAAAAAAAAAAAAAAAAAAAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWxQSwECFAAUAAAACADXYs5SSPr/3HwDAABHEQAAFwAAAAAAAAAAAAAAAABGBQAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWxQSwECFAAUAAAACADXYs5SRczeXRoAAAAYAAAAFgAAAAAAAAAAAAAAAAD3CAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc1BLAQIUABQAAAAIANdizlKKIoVsMQ0AANBuAAAMAAAAAAAAAAAAAAAAAEUJAABnZW9nZWJyYS54bWxQSwUGAAAAAAQABAAIAQAAoBYAAAAA",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 1,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>