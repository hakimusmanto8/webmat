<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Jajar Genjang</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/jajargenjang.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/jajargenjang.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1270,
"height":756,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 39 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAHBizlKDPI3r/gQAANElAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9z2jgUwM/bT+HRafcQsMEGkonTSTuzs5lJ084ms7NXYYTRRkheSw4mn75PkrFNgDQYmpA0HJCfrL+/9yQ9ST79mE+Zc0dSSQUPkddykUN4JEaUxyHK1PhogD6efTiNiYjJMMXOWKRTrEIU6JRlPpBaPd/XcThJQhQxLCWNkJMwrHSWEInxmFFOkOPkkp5wcYWnRCY4ItfRhEzxpYiwMmVNlEpO2u3ZbNZa1NoSadyGgmU7l6N2HKsWhMiBpnMZouLhBMpdyj3rmnwd1/Xa/365tPUcUS4V5hE0BLo1ImOcMSXhkTAyJVw5ap6QECWCcoUchoeEheiblpzfxykhfyCnyAS0XHT24bdTOREzRwz/IxHEqTSDoot8RmjrNPD6s2AiddIQ9fvIAbg6GIaoEwQAjSUTHCLXJmZ4TlLnDkMJRQzOlIhMfhM7xkwWBZuavogRsW/8Ij2noCXA6UhFQB8ecmRCyMg82R7CAyhnbvRcKy8SIh1JJw/RFb5CzrwI721okhg21/S+qDKox6o5K6JNy0/bBdanAR6RhPARJFqi7DWi3BsYyjoAyjp4zZCLKn8e5N475E2QYZhvS/krr7PtNGLrdWBigC6Z8H2iqNG94H+TGNpcZ9x9Z7xXxssW7DeiC94A9Af+XyVZk8QylPofvBoxTRjJ9wjeekQFxEsjlNA7zbyLOnTtjj0/cqi1IXKNw8JTExrdciLBvQO7KcvVD3/REaxepj4B/iNVUJLXH9gSyP98SWUUNEYhzeNqGGc80n0q0X7O0ru6Lrq++xLaqMrctzJ2Jb2ZpSSxlkou1wu5MuxmDt2vbdgiU0zXfMEVbLqAGbRUrnTtlpDkBor6ym9SzKXeeS1b0ma9pXj+mM6Cd50dns4W8xa/w2mpibrWmnlFG9ftFpjBC6tui0m8DmJ3B+agzHdL29zJiHrNhn7H9dfTa/UP2IjuoHuiwvBPIVaOwLtTtv08uMaTxqkikmL+o30Jm8e1Ef1tIZf66Ft97N7GrXeOQdfoNIDSHti359qf5x+7nteD7f/BmrsGvLQH0YRtRIXYumvPj/hAB81mmpHg+sx7sYewUsnRf2NTxx42azQm3M64Eq4FXJNsDgFkvteSvk/IPSPPIYC39zqAaJMfGp7S3Dm3Oc5twvOODbo28G0QlICa7RCNahOYtWo+8oOlwW+2rXlNE8mbVPoz+Ok8m5K0NjVcLeTSeAI7OUB5Wf3k6ElTwSY72WwVktERmNCUgpKOQHtTDA6A9vOHUrBMwZUc3HTx6krOmvGMjtREO3ZQ95jm2lzsi4lI6b3gqoTh6EFwzszd3dIpxjrr6Tzmti7Z6m6zM+YxqwbjuZUqBdhDepPo4fndOr3UEUJDDMFeqzPoeoOg6/a9/nEw6D2RqDdoTHTVPMAYdjeQrYY5KHB1mOM0qg5HwWteO++4Ldfr+0G3c9wJvONjHx6gkn3vA/8sI6o9zSGe5hkLWEn60w7qmIgyWR0+W6kkBCb5prwVnOWUUZzOd7P1rQgrklf+wo0Rat8OHCDgzV0B7HHVtAsr1a7obWfGFChy+LADDhBMJZR/wtFtnIqMF5ZdX4X20vVimTjE/cBQCEZgj7vo1qeFXLsaXln3NwGy8S86+uADmuh2KPKlteoH91uyGgGXRqhd2a4ZAU/v5eo6d/TiptDkYG7TTeJaT6ROul37eKm9+FLq7DtQSwMEFAAAAAgAcGLOUtoN/fRiAwAALBEAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzXLTMBCAz/AUGt2J7cROSacuk4EDzABTpheuqrxJBI7kSkoc99V4B56J1U9Tp7SdSaeUaSGHrH68K+nb9Ury0ZvNsiZr0EYoWdJskFICkqtKyHlJV3b26jV9c/zyaA5qDmeakZnSS2ZLWrgnt3pYG4zz3LWxpikpr5kxglPS1Mw6lZKq2awWEighGyMOpfrMlmAaxuGUL2DJPirOrLe1sLY5TJK2bQeXow6Unido2CQbUyXzuR2gpASnLk1JY+EQ7e5otyOvN0zTLPn66WMY55WQxjLJcSK4rApmbFVbg0WoYQnSEts1gAtQUvARjlGzM6hL+kFaXCtwN0XCV3qN+lG5pKOsSOnxyxdHXCldGaI2JUUSqgviIogW8SKy0LcOfevQ14bGNjS2vjFxBs1CtUSdfcOBS2r1CkeNE/IV/wx2v1W10kSXdFgUlKDjsuEBJWfeGqubBcPSIEvDL8snaZaNs2FQr1kHmqwZ2oyDspVV3Fv0rTNWmziUH/uTqiD05PF5KTAkHBhjAZ2PqzQNQOVLASfOAyOh80HVt4cBcWq7GohdCP5dgkF34gq2Sq7wXlQVuNgMOiDmINcIRGmDsZT6UToU+PiFq7kg3GS+3qHA3gsnsNnr41S12JBp0JiGB6fDIEZB5EEUWyRwLsM8jfsvacM0hi8a4q7/KInB81sYsY0wo3dbp01jtRc56chHzr6OxukhSvzfdfITdentdEksA67554+7YfvXkjNtwQgmey/vW9dxnfv4X+d+O0i0L6HH78TXd/hhUr0Xv8nEAxxmKBGhl9sMVTwUxpiHQ1YNSTUm2q3JGXNbXBxlEAPxRoiR3d4QVd0toNJKXnHsNV2hHEWU93lz9sWfFSPPv8BwurY95HF7KCbjNB/nD+aL+4b0XmSnmi/EEipgu2hxk3sstMMM14MLyMPO68TzYHvSYQYWmA36XB8vZH2KwMlPAtfhs4nZEy3Mcpdq9ohUxyERB6oTrD1BqhLsdp2fXbmfVYv/WXUflucrVvkTV1zql8t6n2kI0L2DLc1vPjYNDh4MyJ+4Stx4kXCN4bbQBXGBIhrc925BpuMgDoJ4HcTk1nuHWDa14MLe7Uqz0jO8WN90FI5du17N/5ZXrww/ynG4r3TnSS7pfRdILj9CHP8CUEsDBBQAAAAIAHBizlLWN725GQAAABcAAAAWAAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc0srzUsuyczPU0hPT/LP88zLLNHQVKiuBQBQSwMEFAAAAAgAcGLOUtYtoeS2CQAAgCoAAAwAAABnZW9nZWJyYS54bWztWutu47gV/r37FIR+x7aouwb2LJKZol1gdnew2RZFi6KgJdpmI0teSc5lsA/f75CUbdm52JO5BdgkDiXq8JDnfOdGyuMfbpcFu5Z1o6py4vCh6zBZZlWuyvnEWbezQeL88Pr78VxWczmtBZtV9VK0Eyckys043A2jIKA+sVpNnKwQTaMyh60K0dKQiVPNZoUqpcNUDvogCkN/lg/y3PMGwTQWg1Tm/iBMZDwVIvXzLHYYu23Uq7L6WSxlsxKZvMwWcineVZlo9ayLtl29Go1ubm6G3fqGVT0fYQnN6LbJR/P5dIjWYRCybCaOvXgFvr3RN74e57kuH/3zp3dmnoEqm1aUGZZMClir199/N75RZV7dsBuVtwuoy4sh8UKq+QIqicPIYSOiWkEvK5m16lo2GLtzq6VvlytHk4mSnn9nrlixEcxhubpWuawnjjt04yQOXB7GQeL6PEgxSVUrWbaWmNtJRx278bWSN4YvXekpAzeFQq9Vo6aFnDgzUTQERTmrodzNfdPeFXIqMG1br3G/XRE/078gUR9AH7iQ2+gCcgfumR+6Z7HrnoUhHtByduYOueewtqoKzdllfzDOQhcfxlN2xqIYPR7jIQvQk6AnZj71hTxgPiMS7rMgQBtQN4/oWYjxocs4RzfzXOZ5zOPM83EbhiyMWBjTQA+0UaqZufgQNZaDj099vo+P7vMDfDy6AqPQsMEiQj/SVyFRg3+Imf5gutNPWJBiIuoIY858rAH3scvAEYyxUC1E4DL64ywg9l7MvISBH+Qmzi5UczwqtmMPlg6UcBcUDjDoE+Gj0doDJehDAgRcyIYFuhBTN7TcCLqmR0CL+lzSCxotiesSJGigHKKBlHQLSXVjaDRsaPznitkJ6Z8iZLIjJOg4gUKr143PaN24IHDRBPaWzAuNNjUXVmN6E/oHQ4I5RSxK9MUzZYJOPkImaGIzq/HSUybtpsQMJ8z5PCPdyOklFAjEdOKcv/vrXy5+PT9B6mcqeyN3gCV0kyJm6T/9OZjSP0nqg4C5mZEi4nEzRj2n/NKzcw/u8il0jDx17JyUPu8JQ6aFu+r2y+AwHnVpc2xXxJoF0Vona+USFQSie8pin0U6iOg0hvyFQG5yWeyxOGQxhZAuoyEJJSyi1qY1SmpJL62FlPS63IbEFlEnEgkFHqYzk8lzXtClOlzrZEeJsJ/skJuCbXrCAokVZwy5VIetLk9hFd4mU3lYPhITAhuSpMciCpQPJC2UdFWjNrpdyALlnkVBq1GVq3XbU122pJpHX7YVqEWhSzVLn1fZ1cVG2ZaTFA0Kqi1bFDfbGsoUO70S67txIaYS9eX8kiyBsWtRkAvpGWZV2bLOCiLqG490OTeW66xQuRLlPwB9Vzn9vF5OZQ2Tw2VFQmomNJw9UPe5vqHJqqrOL+8amAq7/ZesMdofItBuf7wICryzj6K0/xCPmkyQlQfpMN39wRj7JEQ9uPtjZ5bXl7JtIX7DxK2EnVrVzWvysp2bH5uLqth2rSpVtm/Eql3XuuBHqK5JqPNyXkitSo0y6uHsalrdXpowjoUSr9/uVpS8zAqm8zdVUdUMLuiFIQhsi2BPraahpW2ooEDQ4D8oLFDEdPOcp/B7UOgWNNRqKtpFGJSNqFifEdNyEbeq0cEFvHtGpU2Eiut1qdp33U2rsqutpDTAGAA4a5vt87Qkz+c5Hu0Z31HGaFXQN0ZKacfYoseBR2d8HFu2+42P4uRD5mad4cDcjHf/aW3boPdJje2ZLA9tzYbgztKWVS5NyLQAZ1VRiFUj851AOh71Ro2vZF3KwlCXMIZ1tW4MuYFbM1o38r1oF+dl/qucI2m8F5S3WyzPkG6lzmWmlhho+q32BVnG3yGu6c3lvJadlsxiDDZ2laxZ1VLkzUJK+IJFyHjCLpkWplv+GAVpIXVFslRIagMYwFLc6qIX3oN8pXUybrJarcjm2RTVxZXcmnWuGmKx6SBqqKSBbMhPVQmIWoIHZyPrdlHB2jBGtNRDbl/IJTbzrNX2Xa6XsqaDEwu20AcFWPfarj61EQA4s2r6P2TAPfPYahSPH7B/JorVQtDhAu0DtGMHqct5hJ26sXpxR1FnJ4lq7r/MZo1sGZQTQjt3FFJ2nv60b0UNbA5stFphm1qrnGafNlWxbnGeAzTL7XmOkcOGNWwscVxDaRJVMaaicnymbknNhg6aVB9geRsz0sZ/bsxiVw9bF24XcBYckpAfU+WpVW4v/qbyXOqFWsuDNWr0OiOAcUmaHVebgTjeutOhbIv8yAL6JLTTfWh1afL1ofVxUETQbuiPhRaB+kRkMRIzxdh3vGhkdV7boHSucX0CRSav6QzmAMwYysek1NhctgH0SPgGHmV5Gnsvfl3aeloJuDhUgg2nDc0UDH0gh6kGfAjn/LDRsNYHZW8zpV2I6T2Iw1ulZtVyKcqclXpn914rdbujEO7EeaPqrJD/Pj9j4j9Wd+u2e3hh+FkuT4B0cQxIB+hw0i1VCdR+JD60AdDR7DOjw/2hH+/vIj4TVpdyTv17aOF4R3CLSw+p2eNINZZbh8XsCay6PeP9HvVAeDzWm3zjTfFjcG2VTqUz5WitYHfY13/H476ghVkeiFmswqsP1YItR6DsVFkQ8j+WVEjRJhjVxkHpdSXliormX8rfalE29BamX3N9rO9ND33v7Sm+9/ar+d7AMwChxvvM3pcM8YYiDQK8WEj81Euw1SGvH3Lu8ThOoyBM0CLKf0lP1Cj1cJuf5onzr+mJtuSk/HaULw6Qn/a1jfHBkMd7yAADzx8mqR/FXoojmhivgg62f9+ss76jt6p9vC8M3hrdHt6Lx/E2L2gtmItDsPuHHieVo4+i/KlQjLwhDkC8OIjcOOXc73YnzwZR/l4aBvqgeeKo5apQoDkZmbcGGZ0Be8ioE5BR3wQyu7lu4CU4KEojl0cJ9/wwPTw++bKq33jbnv4XRv9a2z39vzklg715TokfoWgGDtQACGpOjIS26H40h21hAiBDDh+JsQNMvDA19Qz3UJ0knp/EPIpCPwmxyQaOfjJMejDaPelebrJSnVzRF3fzqnyoSmTCI80y4d+XqX6HcRCd+C9dgHKqL0CLr3eIAF85eQpAM3kHEfh9Gvt8XjYMwZOOG0MEsI/cuR9gr/fS+8drYn2rCiXqu4fTyuO5nxR/jLwnCHqaWHsS3Ft2vdj6+3Hdk61/87o/yNRmp7ufqe9N1K51kBeITf4ikAkCN0k9N03wvjvyIjmgL1RQFkn6bxrp22VUDu+n8xcLT/YJtixfBqX7C11gd1+lO3hRGxZk5e3rEP363X5d8/X/AVBLAQIUABQAAAAIAHBizlKDPI3r/gQAANElAAAXAAAAAAAAAAAAAAAAAAAAAABnZW9nZWJyYV9kZWZhdWx0czJkLnhtbFBLAQIUABQAAAAIAHBizlLaDf30YgMAACwRAAAXAAAAAAAAAAAAAAAAADMFAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbFBLAQIUABQAAAAIAHBizlLWN725GQAAABcAAAAWAAAAAAAAAAAAAAAAAMoIAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzUEsBAhQAFAAAAAgAcGLOUtYtoeS2CQAAgCoAAAwAAAAAAAAAAAAAAAAAFwkAAGdlb2dlYnJhLnhtbFBLBQYAAAAABAAEAAgBAAD3EgAAAAA=",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>

                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>