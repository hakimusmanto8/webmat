<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Layang-Layang</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/layanglayang.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/layanglayang.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1658,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIADix0FLlk7XdFQUAAPslAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmt1u4jgUgK93niLy1e5FIQECtGo66oy02kqdzmhbrfbWBBO8NXY2dsrP08+xHZJQSAdCW0AzXOAcx7/fObaP7Vx+nE2Y80QSSQUPkNdwkUN4KIaURwFK1eisjz5efbiMiIjIIMHOSCQTrALk65R5PpAa3U5Xx+E4DlDIsJQ0RE7MsNJZAiRGI0Y5QY4zk/SCizs8ITLGIbkPx2SCb0WIlSlrrFR80WxOp9PGstaGSKImFCybMzlsRpFqQIgcaDqXAcoeLqDcldzTtsnXcl2v+e+XW1vPGeVSYR5CQ6BbQzLCKVMSHgkjE8KVo+YxCVAsKFfIYXhAWIC+acn5fZQQ8gdyskxAy0VXH367lGMxdcTgPxJCnEpSKDrLZ4SmTgOvPwsmEicJUK+HHICrg0GAWr4P0Fg8xgFybWKG5yRxnjCUkMXgVInQ5DexI8xkVrCp6YsYEvumk6XnFLQEOB2pCOjDbXjIkTEhQ2g1yvoID6CeudF0qcRQiGQonVmA7vAdcuZZuLChSWLo3NNFVqlfjlVzlkWbtl82M7DbIR6SmPAhJFrh7NXi3O0bzjoAzjo4bcxZpW+HuXsimA8AGYb6rpS/8jLbVi22XgsmB+iSCX9NFit8b/jfJIJWlym3T4fySTBeteFOLbrgE0B/4P8kyZoklqHU/+DbiEnMyOwVwVu/KIN4a4Qcequej1GGrp2yQ0wZUG9N6BqIxafGNHzkRIKbB5aTl6sf/qJDWMFMfQL8SKqgJK/XtyWQ//mK0ijojEKalxUxSnmoe5XD/ZwmT2VttDvuIfRRlFl7BFQoY1/S1SwlibSUc7lfyoVp13PrfnbTFqliuuYbrmD7BdSgrXKtc4+ExA9Q1Ff+kGAu9R5s1ZaqNZfg+Uta839p7Ri1tpy7+BNOcl2U9VbPN6pcvRtgCO+3gG9U3Q4TeRnE/m7MOxjwm03hexlRt97gb7mdzfQavSM2oifonigw/JOJhTNwEq7Zkc2DG/xpnCgiKeY/2p2weVQa0d+Wcq6PntXH/m3cef/ot41OfSjtmX17rv15nXPX87pwDHDwBe9lxCt7Ec3YRhSQrdP2/pCP1oGo5hkKrs/Al3sJK+UkOycxfeziSL3Cto1GhNt5V8JFgWuSzSGAzAst6RuGmWfkOQTwdqEDiDb5oekJnTnXNse1TXjdskHbBh0b+DmientFo9wY5q6Sr/xsgejU2+Acw3TyUyv9Hbx1nk5IUpoc7pZybjy+nR6gvLR8irTVZFBlJ9VWIRkdgglNKCjpDLQ3weAGaG9/IAVLFVzSwd0XLy7prBlP6VCNtXsHdY/oTJuLLdMZi4QuBFc5DUePgmtmrvNWDjQ2mU/rJe91xVj3m6Axj1gxGq+tVGjAntmbRM8P8zYppswQGmIQdhutftvr+2235/XO/X53S6Rev0BqX2xNdN0+wBr2t5CdxjkocH2c4yQsTkrBea5Yb1yv1/HbrfOW752fd+ABKnnt7eCfeUSxtTnGgz1jAWtJ3+zMjokwlcVJtJVyQmCSJ+CwbL9y4XRGGcXJfD9b34mwIrPCYXgwQulzgiMEXN0VwB4VTbuxUunO3nZmRIEih2894BzBVEL5Jxw+RolIeWbZ5WXoVbqeLRPHeKYwEIIR2Oouu/VpKZduitcW/ipA2Vp7yNEH39SEjwMxW1mrfnDZJYsRcGuE0v3thhGwfS/X17mzg5tCnfO5na4VKxyUsgKapc+cmstvqq6+A1BLAwQUAAAACAA4sdBS08fHHYEDAABREQAAFwAAAGdlb2dlYnJhX2RlZmF1bHRzM2QueG1s7ZjNbts4EIDP26cgeK8l2ZISBVEKo3vYBdqii172ylBjm7syqZK0ZeXV+g59pg5/4shtEtRBkqJFffDwRzNDfjMaiTp/tVu3ZAvaCCVrmk1SSkBy1Qi5rOnGLl6e0lcXL86XoJZwqRlZKL1mtqaFu3Kvh71JmZdujHVdTXnLjBGckq5l1qnUVC0WrZBACdkZcSbVO7YG0zEOH/gK1uyN4sx6Wytru7Mk6ft+cu11ovQyQcMm2ZkmWS7tBCUluHRpahobZ2j3QLufeb1pmmbJv2/fBD8vhTSWSY4LwW01sGCb1hpsQgtrkJbYoQPcgJKCz9BHyy6hrenf0uJegbslEr7RW9SPyjWdZUVKL178cc6V0o0haldTJKGGIK6C6BEvIgtz2zC3DXN9GOzDYO8HE2fQrFRP1OV/6LimVm/Qa1yQ7/hrcPq1apUmuqZT9IBxy1KUlyirKQak7VYMLU6yNPyyvEqzrMymQb9lA2iyZWg0emUbq7g36UcXrDXRl3f+VjUQZvJ4vRSYE46MsYDRR+emA2h8K/DEbWEqDD6rxvYwIz7YoQViV4L/L8FgPIuRkmv8JZoGXHIGHRBLkFskorTBZEq9lwEFXn7lei4Ld5nvDyhw9soJHPb6uFQtdmQeNObhwvk0iFkQeRDFHgl8lGGdxv3XtGMa8xcNcTd/nsTs+SaP2E6Y2Z/7qM1jd5Q66cynzrGRxuUhSvzHMLu7Lgb5qUKK2fO0Qb2bL4ltwF1//nQ/bn9ncqYtGMHk6P597Sa+Jl/+DOSfkvvdING+hBG/975/wA/r6oP4VZUHOM1QIkIv9zWqeCyMsRSHwhrqaqy1e5ML5p5y0cud1fE2qJHl0VBVO6yg0UrecB0N3aCdRbQPuZOODUdWzHw8ivDIGGX0JI9IiqpM8zJ/tNg8NMWPIjvXfCXW0AA7RIuBfS600yw8jvMTj9aJX4Pt+wErssDqMOb6fCnrSwYuvgpcpz8iZ7//gXgcWS3M+pBr9oxcy1CaA9cKez9hvkqw+32+c+1xXS1+19VjWH7csMa/g8Wt/nPdHzMNCfqYxbHMK/c7KbPiNMvxUPNIgJ7iuHHrYcMNhhPFEMQVimjw2PMHmZdBnARxGkR159lErLtWcGHvD63Z6AWevm97WY5Th1HOHxZl1Lv1dXly8r1pf2P4WV6Yx0r3vtslo48HyfWXiosvUEsDBBQAAAAIADix0FLWN725GQAAABcAAAAWAAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc0srzUsuyczPU0hPT/LP88zLLNHQVKiuBQBQSwMEFAAAAAgAOLHQUlpHoLpaCQAAmSsAAAwAAABnZW9nZWJyYS54bWzdWutu20YW/p0+xYC/I4n3SyClUOIkWyBtg7q7WOxisRhJI2lqilRJypaCPny/MzMkRUt2rNiN7dqmhxyeOXMu37mQ0vD77Spll6IoZZ6NLKdvW0xk03wms8XI2lTzXmx9//q74ULkCzEpOJvnxYpXIysgymYdrvqhH9IcX69H1jTlZSmnFlunvKIlIyufz1OZCYvJ2chK3kfB+G0U9+IktHu+cxb03oRn73tRZEeJ6yVnY+/MYmxbyldZ/hNfiXLNp+J8uhQr/jGf8krtuqyq9avB4Orqql/L18+LxQAilINtORssFpM+RotByawcWebkFfh2Vl95ap1r287g3z9+1Pv0ZFZWPJtCZDLARr7+7sXwSmaz/IpdyVm1hLnCILbYUsjFEiaJg8BiA6Jawy5rMa3kpSixdu9SaV+t1pYi4xndf6HPWNooZrGZvJQzUYwsux97kRN4ThKGrut7YWSxvJAiqwytY/Yc1NyGl1JcabZ0pnYMHNdiVZ6nE0482R/MYYGNgzkJe8nCCDMucwLmYybGTMQ8mgscn3mMSByP+T5Gn6adEHfoNv4Hgc0cB3eYazPXZa7DXA+XQcACkEW01gVtmCh+Ng6ihkQ4PJrzPBxqzvNxuHQGRoFmAzkCL1Rngfof0xrsEmC/P5i6hTk/wXY0EUQO8yAJriObgS/YQ2KljW8z+nOYT5u4EXNjprgq/jZsdClLOUnFyJrztCSwZvMC8Guuy2qXCmVEM9E6zXmJX1DIzyAPbISCBgvu2PZLOkIcPt0gh+15x+/6Bq6woRsEtKGmGmBAmoXH6NImw2BQStg2uQVDoGmgIF1CSTVoGuU6DN59Naz1807RDzHS6Ac6Qg4GAgUGj5HcOIH8NPjmMtSXCm42YKNnyfkYgCUg6p7KwBhfoQxM0OxaFZtbN9X32z2bHR3HgVHuuuX9cNlsCozAEXwyssYfP7x788v4UAI3uEHpe9r6qKWxl/pTx8GW3klaX7f01+wYduLwYRT24ztv77iAxDfe07cT1JOHMHMAd11HU0S7Boe7RoTDw4SnR2QHNT6M+5MvuH84qKvl0EjEyiXRmtCuxAp9g80ij4UqYamyiXqJeqFrZ+SyKGARpau6gqLixSyk0ZRRKqJxp4wGVGT3amlIk6hXlN2YKoO6qLp+XVdxriorVd1uZUUJ9NsqCAGJlcMYajcLKWGacggp3KYguhAf9S9kKJqBy0JKyjfURnRxeSkbwy5Fig7PuEDZUGbrTdWx23RFbY46rXJQ81R1Z4Z+lk8v3jSWNpwEL9FDtWzR0LRtk25wOl3Vi2HKJwIt5eKcYMDYJU8p2akd5nlWsTrXhjQ3HKgObig201TOJM/+Bb/X3dJPm9VEFMAbTnNSUjGh5axp9bx4v9VDr6BopnlezM53JXDCtv8RBVYHgddP9n8sttN3/BAsyiknSPtJlyiGmXfmHkJGMReX56KqoGHJ+FYAh8Y6i4KiaO/ih/JNnrZT61xm1Vu+rjaFauNRAQqSe5wtUqGspRyJLnd6Mcm357o6QADi9etuTfGsJZgs3uZpXjCEmEvdLZipETWERkVDojVUCCvQ4D8ojBbEtLnvJIhrUKgRNDQqKno20I7UqkI+rabhwreyVMkDvDu4USiglnmTyepjfVHJ6UWrKS3QPgZnBcsuT0Nyf57DwTV8DQ3ya7St8pnYQ+pw0Lk/vBBFJlINrAye3+SbUpNryZTYm1J84tVynM1+EQtE5SdOWbGCIJq0VXAmpnKFhXreWJoTCv4JxfTsTCwKURtEC6P9YKRk5boQfFYuhQDwjTc07FsyNT0c1OIP0WWkQuX7lUTW6MHZK75VnQxCBQlBhdiwnBZyTQBnE+TuC9FCeCZLYtFMEHXHtN7ZDeEHfJhww9lnfdZz+gBtG11sq0KBWiLQmPNeeFPY6UT2QFF3EGOHyDbV7CGB/XAsyYUPw5LeDYh9ZndOOcDBek2wQSi03cB+BJvUbzYq8t+obuQZq5TlDVwPAEVRV0IUQysrUgCvNDbVMoc8gCUkxkhgTMUKD+GGYbZZiYLedxhzcPV8j9DYmABxDLZISJZPSJxrFmwtgNvX8imaKZNRGU/XS06vBTCnfhw/sR0nxAO+zqN8R5WsCUpTJn+ez0tRMYRgzwEwdxj9vds/Nmmprn0wDPio6K2DFzowPinzdFPhVQySRta+itGqmFqpHjSwAE/saisPPeZcbimaNR2sKT8jwTXZSjlvrLPPvinaqlAtASm84KDSQO2j8aM6+YeczYRqTkyCQ9JTHtS5hhC7FrQ7sNgsBPp2Kk7bBDMwTlVAWK14NmOZ6jd/yCi7wmfKrbrd4fbI2o4RFTCKM7J26lT7b1PVBGPN13A7gI1KGQ0Cxop7ByHdeqRNJS7pjcEBUqi3gDY0ACk01FC5FRit5/dTe0nuo0yqs2gbSSQw9Qt6iRFDzx5Ug5us+Ulp3bXkW1lMU/Hf8UvG/3dgxfenWPH9oRUP4+wmIzquzjhqNBnnbnZsA8wBymA3DLdY+TpGEc+HKMXJIUr3PUSR8AUXmYx5oovOxYLmrzkJLwwI6sodHQfNb3dQabjVLph/Cej1I8FxJ3V7zAeCOZkSRqzXHMs7gMINaYfleD0tK2ITxY11UvJckzrg68NW7kKINTUDP2e/Fjwr6U15t4fbd9HtJWdyveQYzz+BikOPNqoJvGPBgUFVwaHY+5p6Ez9uvTkSySfWm9sz5OQwQ344JUN+eOwM2YN/CBCmAf4rM6TXt0NANUySKA5jL/ZEj95lYneI/S2TpvJQx2eL05Lm4kklzZ7p7I6b94kn0nsG29kpwXb26MFmkiLa/r882no+Xmr5fhJHrmt7sR3pRw2773q+H3uBjakwsb9x6Cl/dTy4PC30lk8r9I6YUz3PHdr+7x2H706Jw3ePHod4IqOMmTxm0aO0/S1DT7mo4zR5WujJJxV6f6eil+4WeXbNa/jOjfYa4y495zHuHetcfofMRIcv1hAdzE90k//TtI8v/3wpLvXWtQfB7WGMdnNw3wUkx5541CdHdwXKzY9q10E9+0Iu+kpxT0M132xlKnmxO4DVHt6PVprjhcbFdOT6iRfi6xqJ7YW18Z5iMNzuH/Fc/GO6cPPuov7Y8BlanHLH87J57/kbffpcTO73oxBfNQz9BB8Xe3Zya8P7rPIQimT7gZP6koL5HuvrPwFQSwECFAAUAAAACAA4sdBS5ZO13RUFAAD7JQAAFwAAAAAAAAAAAAAAAAAAAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWxQSwECFAAUAAAACAA4sdBS08fHHYEDAABREQAAFwAAAAAAAAAAAAAAAABKBQAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWxQSwECFAAUAAAACAA4sdBS1je9uRkAAAAXAAAAFgAAAAAAAAAAAAAAAAAACQAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc1BLAQIUABQAAAAIADix0FJaR6C6WgkAAJkrAAAMAAAAAAAAAAAAAAAAAE0JAABnZW9nZWJyYS54bWxQSwUGAAAAAAQABAAIAQAA0RIAAAAA",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
<br><br>
<iframe src="https://drive.google.com/file/d/1EHVNegOBK3k_tUf34OImGTRZGjX8mJ6m/preview" width="100%" height="480" allow="autoplay"></iframe>

                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>