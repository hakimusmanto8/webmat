<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Belah Ketupat</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/belahketupat.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/belahketupat.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1540,
"height":756,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 39 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAHJfzlKDPI3r/gQAANElAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9z2jgUwM/bT+HRafcQsMEGkonTSTuzs5lJ084ms7NXYYTRRkheSw4mn75PkrFNgDQYmpA0HJCfrL+/9yQ9ST79mE+Zc0dSSQUPkddykUN4JEaUxyHK1PhogD6efTiNiYjJMMXOWKRTrEIU6JRlPpBaPd/XcThJQhQxLCWNkJMwrHSWEInxmFFOkOPkkp5wcYWnRCY4ItfRhEzxpYiwMmVNlEpO2u3ZbNZa1NoSadyGgmU7l6N2HKsWhMiBpnMZouLhBMpdyj3rmnwd1/Xa/365tPUcUS4V5hE0BLo1ImOcMSXhkTAyJVw5ap6QECWCcoUchoeEheiblpzfxykhfyCnyAS0XHT24bdTOREzRwz/IxHEqTSDoot8RmjrNPD6s2AiddIQ9fvIAbg6GIaoEwQAjSUTHCLXJmZ4TlLnDkMJRQzOlIhMfhM7xkwWBZuavogRsW/8Ij2noCXA6UhFQB8ecmRCyMg82R7CAyhnbvRcKy8SIh1JJw/RFb5CzrwI721okhg21/S+qDKox6o5K6JNy0/bBdanAR6RhPARJFqi7DWi3BsYyjoAyjp4zZCLKn8e5N475E2QYZhvS/krr7PtNGLrdWBigC6Z8H2iqNG94H+TGNpcZ9x9Z7xXxssW7DeiC94A9Af+XyVZk8QylPofvBoxTRjJ9wjeekQFxEsjlNA7zbyLOnTtjj0/cqi1IXKNw8JTExrdciLBvQO7KcvVD3/REaxepj4B/iNVUJLXH9gSyP98SWUUNEYhzeNqGGc80n0q0X7O0ru6Lrq++xLaqMrctzJ2Jb2ZpSSxlkou1wu5MuxmDt2vbdgiU0zXfMEVbLqAGbRUrnTtlpDkBor6ym9SzKXeeS1b0ma9pXj+mM6Cd50dns4W8xa/w2mpibrWmnlFG9ftFpjBC6tui0m8DmJ3B+agzHdL29zJiHrNhn7H9dfTa/UP2IjuoHuiwvBPIVaOwLtTtv08uMaTxqkikmL+o30Jm8e1Ef1tIZf66Ft97N7GrXeOQdfoNIDSHti359qf5x+7nteD7f/BmrsGvLQH0YRtRIXYumvPj/hAB81mmpHg+sx7sYewUsnRf2NTxx42azQm3M64Eq4FXJNsDgFkvteSvk/IPSPPIYC39zqAaJMfGp7S3Dm3Oc5twvOODbo28G0QlICa7RCNahOYtWo+8oOlwW+2rXlNE8mbVPoz+Ok8m5K0NjVcLeTSeAI7OUB5Wf3k6ElTwSY72WwVktERmNCUgpKOQHtTDA6A9vOHUrBMwZUc3HTx6krOmvGMjtREO3ZQ95jm2lzsi4lI6b3gqoTh6EFwzszd3dIpxjrr6Tzmti7Z6m6zM+YxqwbjuZUqBdhDepPo4fndOr3UEUJDDMFeqzPoeoOg6/a9/nEw6D2RqDdoTHTVPMAYdjeQrYY5KHB1mOM0qg5HwWteO++4Ldfr+0G3c9wJvONjHx6gkn3vA/8sI6o9zSGe5hkLWEn60w7qmIgyWR0+W6kkBCb5prwVnOWUUZzOd7P1rQgrklf+wo0Rat8OHCDgzV0B7HHVtAsr1a7obWfGFChy+LADDhBMJZR/wtFtnIqMF5ZdX4X20vVimTjE/cBQCEZgj7vo1qeFXLsaXln3NwGy8S86+uADmuh2KPKlteoH91uyGgGXRqhd2a4ZAU/v5eo6d/TiptDkYG7TTeJaT6ROul37eKm9+FLq7DtQSwMEFAAAAAgAcl/OUtoN/fRiAwAALBEAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzXLTMBCAz/AUGt2J7cROSacuk4EDzABTpheuqrxJBI7kSkoc99V4B56J1U9Tp7SdSaeUaSGHrH68K+nb9Ury0ZvNsiZr0EYoWdJskFICkqtKyHlJV3b26jV9c/zyaA5qDmeakZnSS2ZLWrgnt3pYG4zz3LWxpikpr5kxglPS1Mw6lZKq2awWEighGyMOpfrMlmAaxuGUL2DJPirOrLe1sLY5TJK2bQeXow6Unido2CQbUyXzuR2gpASnLk1JY+EQ7e5otyOvN0zTLPn66WMY55WQxjLJcSK4rApmbFVbg0WoYQnSEts1gAtQUvARjlGzM6hL+kFaXCtwN0XCV3qN+lG5pKOsSOnxyxdHXCldGaI2JUUSqgviIogW8SKy0LcOfevQ14bGNjS2vjFxBs1CtUSdfcOBS2r1CkeNE/IV/wx2v1W10kSXdFgUlKDjsuEBJWfeGqubBcPSIEvDL8snaZaNs2FQr1kHmqwZ2oyDspVV3Fv0rTNWmziUH/uTqiD05PF5KTAkHBhjAZ2PqzQNQOVLASfOAyOh80HVt4cBcWq7GohdCP5dgkF34gq2Sq7wXlQVuNgMOiDmINcIRGmDsZT6UToU+PiFq7kg3GS+3qHA3gsnsNnr41S12JBp0JiGB6fDIEZB5EEUWyRwLsM8jfsvacM0hi8a4q7/KInB81sYsY0wo3dbp01jtRc56chHzr6OxukhSvzfdfITdentdEksA67554+7YfvXkjNtwQgmey/vW9dxnfv4X+d+O0i0L6HH78TXd/hhUr0Xv8nEAxxmKBGhl9sMVTwUxpiHQ1YNSTUm2q3JGXNbXBxlEAPxRoiR3d4QVd0toNJKXnHsNV2hHEWU93lz9sWfFSPPv8BwurY95HF7KCbjNB/nD+aL+4b0XmSnmi/EEipgu2hxk3sstMMM14MLyMPO68TzYHvSYQYWmA36XB8vZH2KwMlPAtfhs4nZEy3Mcpdq9ohUxyERB6oTrD1BqhLsdp2fXbmfVYv/WXUflucrVvkTV1zql8t6n2kI0L2DLc1vPjYNDh4MyJ+4Stx4kXCN4bbQBXGBIhrc925BpuMgDoJ4HcTk1nuHWDa14MLe7Uqz0jO8WN90FI5du17N/5ZXrww/ynG4r3TnSS7pfRdILj9CHP8CUEsDBBQAAAAIAHJfzlLWN725GQAAABcAAAAWAAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc0srzUsuyczPU0hPT/LP88zLLNHQVKiuBQBQSwMEFAAAAAgAcl/OUjv/1S0eCgAAFSoAAAwAAABnZW9nZWJyYS54bWzlWuty27gV/r37FBj+6K9IIglewFTOju102p3J7mbibafTTqcDkZDEmiK1JOVLZh+q3UfoA+wz9TsASF0sOZbtZJNpYhokeAjgnO87F5Aef3OzKNiVqpu8Kk8cb+g6TJVpleXl7MRZtdOBcL559fV4pqqZmtSSTat6IdsTJyTJ/jlcDaMgoD65XJ44aSGbJk8dtixkS4+cONV0WuSlclieQT6IwpBPs0GW+f4gmMRykKiMD0Kh4omUCc/S2GHspslfltX3cqGapUzVRTpXC/mmSmWrZ5237fLlaHR9fT3s1jes6tkIS2hGN002ms0mQ7QOg5Jlc+LYk5cYd+vpa66f813XG/31uzdmnkFeNq0sUyyZDLDKX3391fg6L7Pqml3nWTuHucIAGs9VPpvDJHEYOWxEUkvYZanSNr9SDZ7duNTat4ulo8VkSfe/Mmes6BVzWJZf5ZmqTxx3GMYh94Oo/+2wqs5V2VpZz8456kYbX+Xq2gxLZ8benu+wtqqKiaQx2c/MY6GLg3kJe8GiGD0+80IWoEegJ2ac+kIvYJyRiMdZEKANqNuL6F6I50OXeR66me8y32e+x3yOyzBkYcTCmB70IRslejAXB0ljOTg49XGOQ/fxAIdPZxgoNMNgESGP9FlI0hg/xEw/M93JBQsSTEQdYewxjjXgOnYZRsTAWKhWInAZ/XgsoOH9mPmCYTzoTSO7MM1V3uSTQp04U1k0xNFyWoN1/XXT3hZK2852rLHyXuA/JPL3EA9d8MFwBHdc9wUdEY6AbhBOG6AE25AAARe6YYEu1NQNLTeCrekW0KI+l+yCRmviugQJGhiHZKAlXUJT3RgZDRsaEOdpanZK8mOUFBtKQs4jUGj1uuGM1o0TAhdNYC+JXmg01VywxvQK+gUigU4Ri4Q+eaJOsMkjdIIl+lnbenXvpOb+es5+RiGw+IfOeBRFD07pC4oCcnLinL754x/O3p0eofITLd0twQs37By6L/SPPu5MyZ+k9GNmjLY88lPP7vnwleewcSAePGfgJsizjybzWk13D5ljmpT035k0RteewGdaBAjdfhrwx6MuWY7tilgzJ1lriVYtUDYgnyQs5izSYUsnTmRMpA6TPWOfxSGLKWh1ORRpT7CIWptIKY2KrUQaUprtsilSaUSdSF0U6pjOhSaz+kGXXHGu0yul3u30imwYrBMiFkhDeYwhe+tA2WVGrMLvc6OP5SMVIpQiLfssotB8IE2ijquavLftXBWo8SwK2ox5uVy1W6ZLF1To6NO2grQsdH1m5bMqvTzrjW1HUrJBFbUeFiXNunAyJc5WXfXVuJAThaJydkFMYOxKFuS3eoZpVbasY0FEfeORruHGapUWeZbL8i+AvquXvl8tJqoG5XBakZJ6EHqcdcWeDth9redyI5JWVZ1d3DZgCrv5m6rxcMDFMNn8h7Bya27x0BuGIGcqidd+MhSu5wnB4ySKXBI7cIcHZjZ1daHaFho3TN4oUNNaa1aTY21cfNucVcW6a1nlZXsul+2q1oU9UkJNipyWs0Jp62lgUfeml5Pq5sKkC+hLY/14u6QMaVYwmZ1XRVUzeJ0fQhUMplskFWq1DC2tl4KnQQa/IWGxoUH7+14CV4eEbiFDrZai3YIB1qiK9Rk17SjyJm90PMHYWzzSrKAielXm7Zvuos3Ty7Wm9IDBvDfi9qBW5vGDau5r2u0QbmxdoaPfosqUoa5lXVoVhVw2Ktsg9Hi09dT4UtWlKox0CUKsqlVjxDeUWTXqrWznp2X2Ts3gvG8lxc8WyzOi3RLhjyrNF3jQ9FsAJJHjz1DX9GZqVqvOTGYxBh67StYsayWzZq4UHMSCZNxjU0wr0y1/jGqkUDozLHIElwE4sJA3uuCBSyFuaJuMm7TOl8R7NkGUv1RrZmd5Q0P0HSQNkzTQDXGiKgFRS/BgY7pq5xUIh2dkSz0UCwq1wFaKtZri5Wqhatq1WrCl3qVh3asOIKsVAc2qyb8QivrkYx5ZmxS3D/gAk8VyLmlrhyyp/3lBAm+PsEkzzJe3FIs2opke/YfptFEtg3WoNLrFcuKNu9/t0qgB6TCMtivIqc0KBZicNFWxarGbBpzlejdt9LDBDnsKbJYxU4SiBFPFCE7T/IbsbORgyvw9qNfzSLP/1PBi0w5rN27n8BbsUcmXqQTQNrcnf8qzTOmFWuqBjhq+jgVgl6LZcdY/iJcLtzqcraEfWUTvYKvJ3MP06380tLpzg502xm0iPuRJ5Ec8cL0wwVsLrlPJtnGxEm1c4f733w80r0clGpk3hj6EJEdEtva1xH6wgT9ARqauaKt5gJNg4BNZeYd3xwEOJld4t5O3ms/Cwl+nlIIsKF283+GEr839RFroxNgb7FSz4pEGjVFJY1JqbDLsTfpAtx7AoYkNfL9fH7DDHt/AyV0j2GDc6Jn4EG+rgihIhJ9EQni0Wsw8CIdJzNHpRrHnJpxKgPc9stpYa2Bstje9d0L82uJptVjIMmOlLt7faouvi0bpnjjneZ0W6u+nL5j8hzXsqu1unpnx7CgfQPDsKQh6vilodPtIDBNtRwzwkRGMhr7veXEgQp7EcRga5gwivIb1Ea1iLwriQMP6zPhdqBn17yCIVwnSs1htoTe9H73GjtbhM30KfgfS7APD12bFok2MV9qR4KGAl/AoEL4wsTqBi8QRnIRHCRJCjAgGE0f+ELEycWPuu0gZcK57oiFQOZD99gRDGLMgInxbUuVGux+UN3dqvUulllSo/1D+WMuyoXfu20XeYUTfVXiXrHYAPTOAIlEy6RO+u8C+PsYtX//WbjnwbICz+ftD2A+Q+AEikh9gj4UIhQ3Nw5hHqAUQJEMh4gSIPbN/6S3ZfjDIyYCFtvwWFr/+cj8YO8XPL3uLH2uDhxQ/mzhu74+6rfzHrTnWuJpCmO8Pt/cVJIdc8KH1yCMD5F3sZscFyNlnFCAHyTB28X4jFm4UJQLvK0zyA21c4fI4DGKRJCFtIeAkAx8hMvGRlxIhghi86nzx8w+Rb+jL5V6f1PBtATq/H1DzEdSiNb+L5rY/fb5wxqhAUDj6MccRIEg+F5jqp9IMoN/vnjj5YlnkkDkaodcGIV2BbCGUH4FQ/jkhdFRFMuARchgXEfzNFX7Q16S/EUK9c+7ANDcwaVC2YDo/psI4f0pcJEoDJ2oAFDVPRUoEAvVgwLnrRyLBN2yTp1yEQDf0AxG5PKQvssApEUN8sUENz11UG6618U4tYRd99F6ruJ1V5aFaXVcT52j4vsT0E7AnOflPOoHkxEji/ZgMcPU7iY8Cv/8QRmYBHQoY83koeBjo+ysQj/5+AlCHGPWZXnt0Hxzo7c3hfHJ/Vicbfxy1jlNCrm7yIpf17Z3k+f+wLbofo8kXg9Bnksg/OULpl4PQrhPRW0Panu73oo+WyT+9E9ms8aUAtd+TBgdc6Yva4SBprz+T6c/j9m8oX/0PUEsBAhQAFAAAAAgAcl/OUoM8jev+BAAA0SUAABcAAAAAAAAAAAAAAAAAAAAAAGdlb2dlYnJhX2RlZmF1bHRzMmQueG1sUEsBAhQAFAAAAAgAcl/OUtoN/fRiAwAALBEAABcAAAAAAAAAAAAAAAAAMwUAAGdlb2dlYnJhX2RlZmF1bHRzM2QueG1sUEsBAhQAFAAAAAgAcl/OUtY3vbkZAAAAFwAAABYAAAAAAAAAAAAAAAAAyggAAGdlb2dlYnJhX2phdmFzY3JpcHQuanNQSwECFAAUAAAACAByX85SO//VLR4KAAAVKgAADAAAAAAAAAAAAAAAAAAXCQAAZ2VvZ2VicmEueG1sUEsFBgAAAAAEAAQACAEAAF8TAAAAAA==",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>