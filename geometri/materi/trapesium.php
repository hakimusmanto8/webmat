<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Trapesium Siku-Siku</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/trapesium.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/trapesium.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body text-center">
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1658,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAHU50lLlk7XdFQUAAPslAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmt1u4jgUgK93niLy1e5FIQECtGo66oy02kqdzmhbrfbWBBO8NXY2dsrP08+xHZJQSAdCW0AzXOAcx7/fObaP7Vx+nE2Y80QSSQUPkNdwkUN4KIaURwFK1eisjz5efbiMiIjIIMHOSCQTrALk65R5PpAa3U5Xx+E4DlDIsJQ0RE7MsNJZAiRGI0Y5QY4zk/SCizs8ITLGIbkPx2SCb0WIlSlrrFR80WxOp9PGstaGSKImFCybMzlsRpFqQIgcaDqXAcoeLqDcldzTtsnXcl2v+e+XW1vPGeVSYR5CQ6BbQzLCKVMSHgkjE8KVo+YxCVAsKFfIYXhAWIC+acn5fZQQ8gdyskxAy0VXH367lGMxdcTgPxJCnEpSKDrLZ4SmTgOvPwsmEicJUK+HHICrg0GAWr4P0Fg8xgFybWKG5yRxnjCUkMXgVInQ5DexI8xkVrCp6YsYEvumk6XnFLQEOB2pCOjDbXjIkTEhQ2g1yvoID6CeudF0qcRQiGQonVmA7vAdcuZZuLChSWLo3NNFVqlfjlVzlkWbtl82M7DbIR6SmPAhJFrh7NXi3O0bzjoAzjo4bcxZpW+HuXsimA8AGYb6rpS/8jLbVi22XgsmB+iSCX9NFit8b/jfJIJWlym3T4fySTBeteFOLbrgE0B/4P8kyZoklqHU/+DbiEnMyOwVwVu/KIN4a4Qcequej1GGrp2yQ0wZUG9N6BqIxafGNHzkRIKbB5aTl6sf/qJDWMFMfQL8SKqgJK/XtyWQ//mK0ijojEKalxUxSnmoe5XD/ZwmT2VttDvuIfRRlFl7BFQoY1/S1SwlibSUc7lfyoVp13PrfnbTFqliuuYbrmD7BdSgrXKtc4+ExA9Q1Ff+kGAu9R5s1ZaqNZfg+Uta839p7Ri1tpy7+BNOcl2U9VbPN6pcvRtgCO+3gG9U3Q4TeRnE/m7MOxjwm03hexlRt97gb7mdzfQavSM2oifonigw/JOJhTNwEq7Zkc2DG/xpnCgiKeY/2p2weVQa0d+Wcq6PntXH/m3cef/ot41OfSjtmX17rv15nXPX87pwDHDwBe9lxCt7Ec3YRhSQrdP2/pCP1oGo5hkKrs/Al3sJK+UkOycxfeziSL3Cto1GhNt5V8JFgWuSzSGAzAst6RuGmWfkOQTwdqEDiDb5oekJnTnXNse1TXjdskHbBh0b+DmientFo9wY5q6Sr/xsgejU2+Acw3TyUyv9Hbx1nk5IUpoc7pZybjy+nR6gvLR8irTVZFBlJ9VWIRkdgglNKCjpDLQ3weAGaG9/IAVLFVzSwd0XLy7prBlP6VCNtXsHdY/oTJuLLdMZi4QuBFc5DUePgmtmrvNWDjQ2mU/rJe91xVj3m6Axj1gxGq+tVGjAntmbRM8P8zYppswQGmIQdhutftvr+2235/XO/X53S6Rev0BqX2xNdN0+wBr2t5CdxjkocH2c4yQsTkrBea5Yb1yv1/HbrfOW752fd+ABKnnt7eCfeUSxtTnGgz1jAWtJ3+zMjokwlcVJtJVyQmCSJ+CwbL9y4XRGGcXJfD9b34mwIrPCYXgwQulzgiMEXN0VwB4VTbuxUunO3nZmRIEih2894BzBVEL5Jxw+RolIeWbZ5WXoVbqeLRPHeKYwEIIR2Oouu/VpKZduitcW/ipA2Vp7yNEH39SEjwMxW1mrfnDZJYsRcGuE0v3thhGwfS/X17mzg5tCnfO5na4VKxyUsgKapc+cmstvqq6+A1BLAwQUAAAACAB1OdJS08fHHYEDAABREQAAFwAAAGdlb2dlYnJhX2RlZmF1bHRzM2QueG1s7ZjNbts4EIDP26cgeK8l2ZISBVEKo3vYBdqii172ylBjm7syqZK0ZeXV+g59pg5/4shtEtRBkqJFffDwRzNDfjMaiTp/tVu3ZAvaCCVrmk1SSkBy1Qi5rOnGLl6e0lcXL86XoJZwqRlZKL1mtqaFu3Kvh71JmZdujHVdTXnLjBGckq5l1qnUVC0WrZBACdkZcSbVO7YG0zEOH/gK1uyN4sx6Wytru7Mk6ft+cu11ovQyQcMm2ZkmWS7tBCUluHRpahobZ2j3QLufeb1pmmbJv2/fBD8vhTSWSY4LwW01sGCb1hpsQgtrkJbYoQPcgJKCz9BHyy6hrenf0uJegbslEr7RW9SPyjWdZUVKL178cc6V0o0haldTJKGGIK6C6BEvIgtz2zC3DXN9GOzDYO8HE2fQrFRP1OV/6LimVm/Qa1yQ7/hrcPq1apUmuqZT9IBxy1KUlyirKQak7VYMLU6yNPyyvEqzrMymQb9lA2iyZWg0emUbq7g36UcXrDXRl3f+VjUQZvJ4vRSYE46MsYDRR+emA2h8K/DEbWEqDD6rxvYwIz7YoQViV4L/L8FgPIuRkmv8JZoGXHIGHRBLkFskorTBZEq9lwEFXn7lei4Ld5nvDyhw9soJHPb6uFQtdmQeNObhwvk0iFkQeRDFHgl8lGGdxv3XtGMa8xcNcTd/nsTs+SaP2E6Y2Z/7qM1jd5Q66cynzrGRxuUhSvzHMLu7Lgb5qUKK2fO0Qb2bL4ltwF1//nQ/bn9ncqYtGMHk6P597Sa+Jl/+DOSfkvvdING+hBG/975/wA/r6oP4VZUHOM1QIkIv9zWqeCyMsRSHwhrqaqy1e5ML5p5y0cud1fE2qJHl0VBVO6yg0UrecB0N3aCdRbQPuZOODUdWzHw8ivDIGGX0JI9IiqpM8zJ/tNg8NMWPIjvXfCXW0AA7RIuBfS600yw8jvMTj9aJX4Pt+wErssDqMOb6fCnrSwYuvgpcpz8iZ7//gXgcWS3M+pBr9oxcy1CaA9cKez9hvkqw+32+c+1xXS1+19VjWH7csMa/g8Wt/nPdHzMNCfqYxbHMK/c7KbPiNMvxUPNIgJ7iuHHrYcMNhhPFEMQVimjw2PMHmZdBnARxGkR159lErLtWcGHvD63Z6AWevm97WY5Th1HOHxZl1Lv1dXly8r1pf2P4WV6Yx0r3vtslo48HyfWXiosvUEsDBBQAAAAIAHU50lLWN725GQAAABcAAAAWAAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc0srzUsuyczPU0hPT/LP88zLLNHQVKiuBQBQSwMEFAAAAAgAdTnSUjOk85ZGCQAAVikAAAwAAABnZW9nZWJyYS54bWzdWm1z28YR/uz8iht8NkW8v3hIZyjJaTPjJJ7I7XTa6XSOxJG8CgRYAJRIT358n707AIQoyaKkRFZsUQccFnt7z+6zuwA1+n67ytiVKCtZ5GPLObEtJvJZkcp8MbY29XwQW9+//260EMVCTEvO5kW54vXYCkiyvQ9nJ6Ef0hxfr8fWLONVJWcWW2e8plvGVjGfZzIXFpPp2Ep+iILJWRQP4iS0B75zHgxOw/MfBlFkR4nrJecT79xibFvJd3nxM1+Jas1n4mK2FCv+sZjxWq26rOv1u+Hw+vr6pLHvpCgXQ5hQDbdVOlwspicYLYZN5tXYMgfvoLd397Wn7nNt2xn+46ePep2BzKua5zOYTABs5Pvv3oyuZZ4W1+xapvUScIVBbLGlkIslIImDwGJDkloDl7WY1fJKVLh371Ttvl6tLSXGc7r+Rh+xrN2YxVJ5JVNRji37JPYiJ/CcJAxd1/fCyGJFKUVeG1nHrDlstI2upLjWaulIrRg4rsXqosimnHSy35jDAhsf5iTsLQsjzLjMCZiPmRgzEfNoLnB85jEScTzm+xh9mnZCXKHL+B0ENnMcXGGuzVyXuQ5zPZwGAQsgFtG9LmTDROmz8SFpWISPR3Oeh4+a83x8XDqCokCrgR2BF6qjQP2O6R6sEmC935i6hDk/wXI0EUQO82AJziObQS/Uw2K1G99m9OMwnxZxI+bGTGlV+m1gdCUrOc3E2JrzrKJgzeclwq89r+pdJhSIZqJzmvMW/yEhv0A8sEEFHSy4Yttv6RPi49MFctied/y+b+AKG3uDgTa2qQYASLPwGJ3aBAwGtQnbJrdgCLQMNkin2KQatIxyHQbvqTts9ucdsz9wpN0f5ChyMFBQYPAY2Y0D2E+Db05DfarCzUbY6FlyPgbEEiLqiZsBGI/YDCBoV63Lzb2L6uvdmu2KjuMAlIcu+bS4bBdFjMARfDq2Jh//8uH018mhBW5wx6afiPWtSGMt9aM+B0t6R+36JtKPWTHs8fB5NuzHD17ecRESf/Cavp2gnjwHzD7AuxlNEa0aHK4aURweJjw9Ijuo8Xncn3zF/aNhUy1HxiJWLUnWULsWK/QNNos8FqqEpcom6iXqha6dkcuigEWUrpoKiooXs5BGU0apiMa9MhpQkd2rpSFNol5RdmOqDOqi6vpNXcWxqqxUdfuVFSXQ76ogDCRVDmOo3SykhGnKIaxw24LownzUv5ChaAYuCykp31Eb0cUVlWyBXYoMHZ5xgcJQ5utN3cNttqI2Rx3WBaR5prozI58Ws8vTFmmjSfAKPVSnFg1N1zbpBqfXVb0ZZXwq0FIuLigMGLviGSU7tcK8yGvW5NqQ5kZD1cGNxGaWyVTy/O/we9Mt/bxZTUWJeMNhQZtUSuh21rZ6Xrzf6qFXUDKzoijTi12FOGHbf4oSdweBd5Ls/7PYTl/xQ6ioZpxC2k/6QjFg3plroIxSLq4uRF1jhxXjW4E4NOgsSmLR3smP1WmRdVPrQub1GV/Xm1K18agAJdk9yReZUGgpR6LLnV1Oi+2Frg4wgHR93q2Jz9qC6eKsyIqSgWIudbdQpkbUEBqVDJnWSoFWkMFvSJhdkNL2upOA15BQI2RoVFL0bKAdqbcK+/Q2jRa+lZVKHtDdixsVBdQyb3JZf2xOajm77HZKN2gfQ7MKy75OI/J0naPhjfgamchvom1VpGIvUkfD3vXRpShzkenAyuH5TbGptLi2TJm9qcQnXi8nefqrWICVnzhlxRqGaNFug6mYyRVu1PMGaU5R8DdsTM+mYlGKBhBtjPaDsZJV61LwtFoKgcA33tBh34mp6dGwMX+ELiMTKt+vJLLGAM5e8a3qZEAVJARFsVE1K+WaApxNkbsvRRfCqaxIRTtB0j1ovfM76If4MHTD0Rd9NHBOELQdu9hWUYFaIsiY40F4F+10Insm1h1w7DCyTTV7zsB+PpXkwudRSe8GxL6yB6ccxMF6TWEDKnTdwD6DTeo3C5XFf6luFDmrFfImXA8CilhXwRQjK2vaAF5pbOplAXsQlrAYIwVjJlZ4CDcK881KlPS+w8DB1fM9qLExBPFNSYGNrJiSNTcA7ADA5S5dBogiIBAARQMA49l6yenFAFos9c/xE9txQjzi60zKd1TLWlqaQvnLfF6JmoGEAwdKdxiNUeryT21iaqofoIEexd+Gvg6935lWRbap8TIGaSPvXsbo3ZhqqR41cIPq/rAUvbWYyy3xWcsBT/kFKa7NV8p9E51/9tHo6kK9RFDhFQcVB2ogjSfVwV9lmgrVnpgUh7SnfKizDcXsWtDqwLG9EfG3U0ztUszQuFWFwmrF85TlquP8Maf8Crcpx+qGh9tjazsBLwCKM7Z26lC7cFM3AhOt12g7CByVNNogmCjtvSDpVyQNlbhqA2o/WKi7wG5oQKzQ0ITKvYHReX4/uVfkPu08ypEdl8hg6hh6ca1nD+pBh+b9dJnepItJjd8GXagzUzXsgWzB4oottPhxZMGdWMlxcecLsoUy9gux5fQYtpwesuUwRvbJQqKfS7laifRmp/TQKHpozLhIeHAlhnuIpnx805Lfg31973xSKPY9cybLWSb+dfqW8X8feOXsGK+cHeuVHvQU+vR4QOMjsceLCMDmmVbhUchrFfchb7QfifyFWND8DexPNSMUyj3c5/fjXhltDbLzryDfPGjfDn3/ye2ZSgeABITNLbclJ/j3jtzECnznI2tKiFHcYpNREmrzC5LW4fPRpRBr6rB/yT+XPK/o66e+qx9LjekhNc6Pocb5S1Nj4OnOz8D5KG4M7BOUUBdfduELGidO8ATv6brlqjcaYYB3GUmE964JDPwjCaR80fPO4jgCLb4lAg1ug5P6kNvx/7PQ7PxWmn04hmYfXoZmxxIJ/o3xZaXv+1EYO3b8wiTC9+1EIoV0D/vlcSRafksk0lUISQ8QDmKCNk4i/MkBWGO7ySviS7ZbFPldfQPjLjmOce+2JPg/2Exy0//QASTx3EWScAL38f7va9TSSzfug7bnAe2xEXLvM17bbj8kUO5+Pr0Z0wTd72XzcaHNN1uZSV7uDsLr9bVe92M+e0WI31mr8fdU/VqtK7XjqFQUeokfxYlre01D9vrcJF6RmzQxBrocvKZqcL8L0lfkgjuI8ip7WtTM7tsC9Q2z+SPE9/8HUEsBAhQAFAAAAAgAdTnSUuWTtd0VBQAA+yUAABcAAAAAAAAAAAAAAAAAAAAAAGdlb2dlYnJhX2RlZmF1bHRzMmQueG1sUEsBAhQAFAAAAAgAdTnSUtPHxx2BAwAAUREAABcAAAAAAAAAAAAAAAAASgUAAGdlb2dlYnJhX2RlZmF1bHRzM2QueG1sUEsBAhQAFAAAAAgAdTnSUtY3vbkZAAAAFwAAABYAAAAAAAAAAAAAAAAAAAkAAGdlb2dlYnJhX2phdmFzY3JpcHQuanNQSwECFAAUAAAACAB1OdJSM6TzlkYJAABWKQAADAAAAAAAAAAAAAAAAABNCQAAZ2VvZ2VicmEueG1sUEsFBgAAAAAEAAQACAEAAL0SAAAAAA==",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
<br/>
<iframe src="https://drive.google.com/file/d/1DE4rVf1F04yJcne10R6WyBj1KKv8V6_O/preview" width="100%" height="480" allow="autoplay"></iframe>
                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>