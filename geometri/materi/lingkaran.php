<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Lingkaran</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/lingkaran.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/lingkaran.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1660,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAIFlzlJAa9R8EwUAAPslAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9v4jgUwM87nyLyafdQSCABWjUddUZabaVOZ7StVns1wQRvjZ2NnQL99PNshyQU6EBgCqjDAec5/vt7z/azncuP0zFznkgqqeAh8houcgiPxIDyOESZGp710MerD5cxETHpp9gZinSMVYgCnbLIB1Kj4/s6DidJiCKGpaQRchKGlc4SIjEcMsoJcpyppBdc3OExkQmOyH00ImN8KyKsTFkjpZKLZnMymTTmtTZEGjehYNmcykEzjlUDQuRA07kMUf5wAeUu5J60Tb6W63rNf7/c2nrOKJcK8wgaAt0akCHOmJLwSBgZE64cNUtIiBJBuUIOw33CQvRNS87vw5SQP5CTZwJaLrr68NulHImJI/r/kQjiVJpB0Xk+IzR1Gnj9WTCROmmIul3kAFwd9EPUCgKAxpIRDpFrEzM8I6nzhKGEPAZnSkQmv4kdYibzgk1NX8SA2Dd+np5T0BLgdKQioA+34SFHJoQMoNUo7yM8gHpmRtOVEiMh0oF0piG6w3fImeXhsw1NEkPnnj7nlQbVWDVjebRp+2UzB7sZ4gFJCB9AogXOXi3OnZ7hrAPgrIPTxpxX+vMwd04E8wEgw1DflvJXXmXbqsXWa8HkAF0y4a/JYoHvDf+bxNDqKuX2L8p7prxoxX4tvuAVQH/g/63Y7pWsSWIZSv0P3o0YJ4xM9wjeekY5xFsjFNBb9byMKnTtlh3CnKHemtA1EItPjWj0yIkERw8spyhXP/xFB7CGmfoEeJJUQUlet2dLIP/zBaVR0BmFNK8rYpjxSPeqgPs5S5+q2mj77iH0UZZZewSsUcaupNezlCTWUsHlfi6Xpl3PsXvvpi0yxXTNN1zBBgyoQVvlUuceCUkeoKiv/CHFXOpd2KItrddcimevaS04Ba29N53NZy7+hNNCE1Wt1fON1q7dDTCDA6tui2m8CmJ3J+aozHdL29zJiDr1hn7L9VfTa3SP2IieoHuixPBPLpauwEk4Zkc2D67wpnGqiKSY/2hvwmZxZUR/m8uFPrpWH7u3cev9Y9A2Og2gtBf27bn25/nnrud14Bjg4E7K64gXdiKasY0oIVuX7e0hH63Tt55nJLg+A5/vJKxUkPRPYvrYxvndw6aNxoTbeVfCRYFrks0ggMzPWtI3DFPPyDMI4O2zDiDa5Iemp3TqXNsc1zbhdcsGbRv4NggKRPV2ika5CcxdFU/5xQLh19veHMN08q6V/gbeOs/GJK1MDndzuTCewE4PUF5WPUPaaDJYZyfrrUIyOgATGlNQ0hlob4zBDdDefl8Klim4pIO7L15e0lkzntCBGmn3Duoe0qk2F1umMxIpfRZcFTQcPQqumbnOWzjOWGU+rde81wVj3W2Cxjxm5Wi8tlKpAXtmbxK9PMpbpZgqQ2iIQdhptHptrxe03a7XPQ96nQ2Rer0SqX2xMdFl+wBr2N1CthrnoMDlcY7TqDwnBed5zXrjel0/aLfOW4F3fu7DA1Sy7+3gn0VEubU5xmM9YwFLSX/aiR0TUSbLc2grFYTAJE/AYdl85cLZlDKK09lutr4VYUWmpcPwYITK5wRHCHh9VwB7XDbtxkqVO3vbmSEFihy+9YBzBFMJ5Z9w9BinIuO5ZVeXob10PV8mjvFMoS8EI7DVnXfr01yu3BQvLfzrAOVr7SFHH3xTEz32xXRhrfrBVZcsR8CtESr3tytGwOa9XF7nzg5uCnXO57a6VFzjoFQV0Kx85tScf1N19R1QSwMEFAAAAAgAgWXOUjREtr6BAwAAUREAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzW7bOBCAz9unIHivJdmSEgVRCqN72AXaoote9spQY5u7MqmStGXl1foOfaYOf+LIbRLUQZKiRX3w8EczQ34zGok6f7Vbt2QL2ggla5pNUkpActUIuazpxi5entJXFy/Ol6CWcKkZWSi9Zramhbtyr4e9SZnnbox1XU15y4wRnJKuZdap1FQtFq2QQAnZGXEm1Tu2BtMxDh/4CtbsjeLMelsra7uzJOn7fnLtdaL0MkHDJtmZJlku7QQlJbh0aWoaG2do90C7n3m9aZpmyb9v3wQ/L4U0lkmOC8FtNbBgm9YabEILa5CW2KED3ICSgs/QR8suoa3p39LiXoG7JRK+0VvUj8o1nWVFSi9e/HHOldKNIWpXUyShhiCugugRLyILc9swtw1zfRjsw2DvBxNn0KxUT9Tlf+i4plZv0GtckO/4a3D6tWqVJrqmU/SAcctSlJcoqykGpO1WDC1OsjT8srxKs6zMpkG/ZQNosmVoNHplG6u4N+lHF6w10Zd3/lY1EGbyeL0UmBOOjLGA0UfnpgNofCvwxG1hKgw+q8b2MCM+2KEFYleC/y/BYDyLkZJr/CWaBlxyBh0QS5BbJKK0wWRKvZcBBV5+5XouC3eZ7w8ocPbKCRz2+rhULXZkHjTm4cL5NIhZEHkQxR4JfJRhncb917RjGvMXDXE3f57E7Pkmj9hOmNmf+6jNY3eUOunMp86xkcblIUr8xzC7uy4G+alCitnztEG9my+JbcBdf/50P25/Z3KmLRjB5Oj+fe0mviZf/gzkn5L73SDRvoQRv/e+f8AP6+qD+FWVBzjNUCJCL/c1qngsjLEUh8Ia6mqstXuTC+aectHLndXxNqiR5dFQVTusoNFK3nAdDd2gnUW0D7mTjg1HVsx8PIrwyBhl9CSPSIqqTPMyf7TYPDTFjyI713wl1tAAO0SLgX0utNMsPI7zE4/WiV+D7fsBK7LA6jDm+nwp60sGLr4KXKc/Ime//4F4HFktzPqQa/aMXMtQmgPXCns/Yb5KsPt9vnPtcV0tftfVY1h+3LDGv4PFrf5z3R8zDQn6mMWxzCv3Oymz4jTL8VDzSICe4rhx62HDDYYTxRDEFYpo8NjzB5mXQZwEcRpEdefZRKy7VnBh7w+t2egFnr5ve1mOU4dRzh8WZdS79XV5cvK9aX9j+FlemMdK977bJaOPB8n1l4qLL1BLAwQUAAAACACBZc5S1je9uRkAAAAXAAAAFgAAAGdlb2dlYnJhX2phdmFzY3JpcHQuanNLK81LLsnMz1NIT0/yz/PMyyzR0FSorgUAUEsDBBQAAAAIAIFlzlL7j3hMoAcAAFIWAAAMAAAAZ2VvZ2VicmEueG1svVhbc9s2Fn5OfwWGz5HFO8WMlI6cpLs7k3Y69e7Ozr5BJCShpkiVF1ny9Mf3OwcgKVlJk9Se2qYBggcH5/KdCzn//rgrxEHVja7KhePduI5QZVblutwsnK5dT2bO92+/m29UtVGrWop1Ve9ku3Aiohz24e4mDkNak/v9wskK2TQ6c8S+kC1tWTjVel3oUjlC5wvHdcMgCjx3kqZuOgmX8Q+T1PuQTgLP+yFJ3y8/vL+9dYQ4NvpNWf0kd6rZy0zdZVu1kx+rTLZ86rZt92+m04eHh5tevpuq3kwhQjM9Nvl0s1ndYHQElCybhWMnb8D3YvdDwPt81/Wm//vxozlnosumlWUGkckAnX773av5gy7z6kE86LzdwlxxDI23Sm+2MMksihwxJao97LJXWasPqsHes1vWvt3tHSaTJT1/ZWaiGBRzRK4POlc1LHUzC5IgTIM0iVI3iL3QEVWtVdlaWs+eOe25zQ9aPRi2NOMTI893RFtVxUoST/G78ETk4hJeKl6LOMGKL7xIhFiZYSURAa1FXigCQSReIMIQY0jLXown9Bj/o8gVnocnwneF7wvfE36A2ygSEcgS2uuDNk6Zn4uLqCERroDWggAXrwUhLp9mYBQZNpAjCmKeRfx/RntwSoTzfhf8CGthiuNoIUo8EUAS3CeuAF+wh8SsTegK+vNESIf4ifBngrkyfxc2OuhGrwq1cNayaAis5boG/Ib7pj0Vio1oF0anea/xCwr9CPLIBTAMWPDEdV/TFeMK6QE57Mw7cOm5b+AKF7pBQBdq8gAD0io8RrcuGQYDK4FQMkNkaKAg3UJJHgwNuw5D8FwNe/2Cb9FvdqYf6Ag5GAgUGAJBcmMC+WkI7W1sbhluLmBjVsn5GIAlIOqZysAYf0EZmGA4ta27bzt0ONLz/G8483nAHA4FSOAJuVo4y4//+HD7y/JaAj/6jNbPNPYnTY2z+I+vqyODb9LauOJ5J8YXgfgyCoezrz7e8xEof/OZoZsmL2PmGYz3FE0JnRpdn5oQDq8znhmRHnh8GfenX3D/fNqXy7mVSDRborWx3aodGgdXJIGIOWNx3UTBRMEwxTPxRRKJhPJVX0JR8mYiptHWUaqis4s6GlGVPSumMS2iYFF6E1wHTVX1w76wYs6llcruZWlFDQzHMggBiZUnBIq3iClj2noIKfyhIvoQHwUwFqiakS9iysqfKY5o46pGD4bdqgItnnUB21CX+669sFu2oz6Hp20Fallwe2bp8yq7vx0sbTkp2aCJGtmioxn7JtPhXLRVr+aFXCn0lJs7goEQB1lQsuMT1lXZij7XxrQ2n3ILN1ddVuhcy/K/8HvfLv3U7VaqBt4wrUhJZkLbxdDrBel5r4dmgWmyqqrzu1MDnIjj/1WN3XGKPvBk5qFLSMwkgZgC4TTOebs63Km2hQ6NkEcFpFn9NzXFydnNv5rbqhiX9pUu23dy33Y1d+rI8TVJtiw3hWJ7sKvQyGb3q+p4Z/I/PEm8/n3aU8QaCVabd1VR1QJB5FMDC2Y8okrQyDQk2kAFLUCD/6CwWhDT4bmXInJBwSNoaGQqav+Nq4yqkM+oabnIo27M2wFMdo4M9jN1xV2p24/9Tauz+1FT2mC8CM4MvEueluT5POfTJwiaW2z3eNpVuTrD4nx68Xx+r+pSFQY6JTzfVV1jyI1kLHbXqJ9lu12W+S9qg7j7WVLeayGIIR0VzFWmd9ho1q2lJaHgP1DMrOZqU6veIEYY4wcrpWj2tZJ5s1UK0LbeMMAeyXh5Pu3Fn6OPKBRn9J1GXpjA2Tt55F4FwYCQ5yCaN1mt9wRwsUJ2vlcjhHPdEIthgagvTBu8/0yAURwNs0czm3g3AO1ZpB05FKjpAY2dT/C69umwM6nqhaLuKsaukW3r1UsC++VYkgtfhiW9/qtzZl+dcoCD/Z5gg1AY6/15BNvkbg+qq1+pMlSlaNnyFq5XgKKoayCKpdUtKUBF5B4hVuLzRdduKwgGfEJ0jITKQu3wwm05l91O1fRtw9pF8rs8YqSzkWI1IGFFtSKxnlhytAQej3mT2r8hs/KdLPZbSd8A0EzxjxemrufFeJs3GVWeqGoN4WlL4o9DBuoLGWwAQg5UOIvjFPaWq6YquhbfVZAeyvG7ihHW1j1+aSB6aipP1NuBw1ofKXANIeylH5HLhsTEflqaRHOu7VgA2i3Qg88VVAWoF7Qu48k/dZ4rFtTmMuQ3dlafVpCuFJ2O2bARQDtxSI65ZGrdduVAjuLBGUt23xecJdSBXtOvfJagecahNFjwDj77Sg/1te+JlnD5tZ6YXOtpM2RDPpq4NwnacHgJ6TAGgB7HACGlqQ24wKhZvUryo+WyareTZS5Kbrvf6TorFBvMtHzSJQMK6VEYGOt0bf8gM8wsiys3IBDPoij7ghv6JvHTfviz2AmsBb7WFYztp2VWdkddaFmfnj74JKYBg89AWlT4kKlbiqNkZmuR3qjyAI0rVHdxhOFAdsJAhYvu6KvqERam+MOAp480YJn3Aze1Poql2bg0FEufSjJAgNff8Qf1ahkwo2VIpiGwLCPiBbAYYX4rjTb8arVwqN/Wa/jpCS7g1TGNcnNtP8C+/QNQSwECFAAUAAAACACBZc5SQGvUfBMFAAD7JQAAFwAAAAAAAAAAAAAAAAAAAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWxQSwECFAAUAAAACACBZc5SNES2voEDAABREQAAFwAAAAAAAAAAAAAAAABIBQAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWxQSwECFAAUAAAACACBZc5S1je9uRkAAAAXAAAAFgAAAAAAAAAAAAAAAAD+CAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc1BLAQIUABQAAAAIAIFlzlL7j3hMoAcAAFIWAAAMAAAAAAAAAAAAAAAAAEsJAABnZW9nZWJyYS54bWxQSwUGAAAAAAQABAAIAQAAFREAAAAA",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
<br><br>
<iframe src="https://drive.google.com/file/d/1m6c4tsSJlaskBhJNxTN1tRziLv8GAe4X/preview" width="100%" height="480" allow="autoplay"></iframe>
                                    </div>                                
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>