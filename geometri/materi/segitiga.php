<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Segitiga</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/segitiga.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/segitiga.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1658,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAHyQzlJpgIJREwUAAPclAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9v4jgUwM87nyLyafdQSCABWjUddUZabaVOZ7StVns1wQRvjZ2NnQL99PNshySUpgOhHUAdDjjP8d/fe7af7Zx/nE+Z80BSSQUPkddykUN4JEaUxyHK1PhkgD5efDiPiYjJMMXOWKRTrEIU6JRFPpBaPd/XcThJQhQxLCWNkJMwrHSWEInxmFFOkOPMJT3j4gZPiUxwRG6jCZniaxFhZcqaKJWctduz2ay1rLUl0rgNBcv2XI7acaxaECIHms5liPKHMyh3Jfesa/J1XNdr//vl2tZzQrlUmEfQEOjWiIxxxpSER8LIlHDlqEVCQpQIyhVyGB4SFqJvWnJ+H6eE/IGcPBPQctHFh9/O5UTMHDH8j0QQp9IMis7zGaGt08Drz4KJ1ElD1O8jB+DqYBiiThAANJZMcIhcm5jhBUmdBwwl5DE4UyIy+U3sGDOZF2xq+iJGxL7x8/ScgpYApyMVAX24LQ85MiFkBK1GeR/hAdSzMJqulBgJkY6kMw/RDb5BziIPH21okhg6t/QxrzSoxqoFy6NN28/bOdjNEI9IQvgIEq1w9hpx7g0MZx0AZx28Nea3hJxX+XaQe78g10GGgb4t5a+8yrbTiK3XgakBumTCX1PFCt8r/jeJodVVyt3joXwUjFdt2G9EFzwC6A/8HyVZk8QylPofPBsxTRiZvyJ46xXlEK+NUEDvNPMwqtC1S7aPKQPqbQhdA7H41IRG95xIcPLAcopy9cNfdAQrmKlPgBdJFZTk9Qe2BPI/X1EaBZ1RSPOyIsYZj3SvCrifs/Shqo2u7+5DH2WZjUdAjTJ2JV3PUpJYSwWX26VcmnYzp+69m7bIFNM1X3EFmy+gBm2Va527JyS5g6K+8rsUc6l3YKu2VK+5FC9e0lpwDFp7bzpbzlz8AaeFJqpaa+YZ1a7dLTCDPatui2m8CmJ3J+agzHdL29zJiHrNhn7H9Z+n1+ofsBE9QPdEieGfXCxdgaNwzA5sHnzGm8apIpJi/qO9CVvElRH9bSkX+uhbfezexq13j0HX6DSA0p7Yt+fan+efup7Xg0OAvTspLyNe2YloxjaihGxdtp8P+WCdvnqekeD6/Hu5k7BSQdI/iuljG+f3FTZtNCbczrsSLglck2wBAWR+1JK+XZh7Rl5AAG8fdQDRJj80PaVz59LmuLQJLzs26NrAt0FQIGq2UzTKTWDuqnjKTxYIv9n25hCmk3et9J/grfNsStLK5HCzlAvjCez0AOVl1TOkjSaDOjuptwrJ6AhMaEpBSSegvSkGN0B7+0MpWKbggg7uvXh5QWfNeEZHaqLdO6h7TOfaXGyZzkSk9FFwVdBw9Ci4ZOYqb+U44znz6bzkva4Y624TNOYxK0fjpZVKDdgTe5Po6VHec4qpMoSGGIS9VmfQ9QZB1+17/dNg0NsQqTcokdoXGxNdtw+wht0tZKtxDgpcH+c4jcpzUnCea9Yb1+v7Qbdz2gm801MfHqCS194O/llElFubQzzWMxawlvTNTuyYiDJZnkNbqSAEJnkEDsvmKxfO5pRRnC52s/WtCCsyLx2GOyNUPiU4QMD1XQHscdm0KytV7uttZ8YUKHL4zgPOEUwllH/C0X2cioznll1dhl6l6/kycYhnCkMhGIGt7rJbn5Zy5Z54beGvA5SvtfscffA9TXQ/FPOVteoHV12yHAHXRqjc3j4zAjbv5fo6d7J3U2hyPrfVpWKNg1JVQLvyiVN7+T3VxXdQSwMEFAAAAAgAfJDOUnSB4oV8AwAATxEAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzW7bOBCAz9unIHivJdmSEgVRCqN72AXaIote9spQY5u7MqmStGXl1foOfaYOf+LIbRLUQRKgRX3w8EczQ34zGok6f7Nbt2QL2ggla5pNUkpActUIuazpxi5en9I3F6/Ol6CWcKUZWSi9Zramhbtyr4e9SZnnbox1XU15y4wRnJKuZdap1FQtFq2QQAnZGXEm1Qe2BtMxDh/5CtbsneLMelsra7uzJOn7fnLjdaL0MkHDJtmZJlku7QQlJbh0aWoaG2do90C7n3m9aZpmyb/v3wU/r4U0lkmOC8FtNbBgm9YabEILa5CW2KED3ICSgs/QR8uuoK3p39LiXoG7JRK+0VvUj8o1nWVFSi9e/XHOldKNIWpXUyShhiCug+gRLyILc9swtw1zfRjsw2DvBxNn0KxUT9TVf+i4plZv0GtckO/4a3D6rWqVJrqmU/SAcctSlFcoqykGpO1WDC1OsjT8srxKs6zMpkG/ZQNosmVoNHplG6u4N+lHF6w10Zd3/l41EGbyeL0UmBOOjLGA0UfnpgNofCvwxG1hKgw+q8b2MCM+2qEFYleC/y/BYDyLkZJr/CWaBlxyBh0QS5BbJKK0wWRKvZcBBV5+7XouC3eZ7w8ocPbaCRz2+rhULXZkHjTm4cL5NIhZEHkQxR4JfJJhncb917RjGvMXDXE3f57E7Pkuj9hOmNmf+6jNY3eUOunMp86xkcblIUr8xzC7uy4G+blCitnzvEG9ny+JbcBdf/n8MG5/Z3KmLRjB5Oj+fesmviVf/gzkn5P7/SDRvoQRv0vfP+CHdfVR/KrKA5xmKBGhl/saVTwVxliKQ2ENdTXW2r3JBXNPuejl3up4F9TI8mioqh1W0Gglb7mOhm7RziLax9xJx4YjK2Y+HkV4ZIwyepJHJEVVpnmZP1lsHpviR5Gda74Sa2iAHaLFwL4U2mkWHsf5iUfrxK/B9nLAiiywOoy5vlzK+pKBi68C1+kvk7OXWpj1IdXsBamWoTAHqhX2fkKqEux+nx9ce1xVi99V9RiWnzas8W9gcav/3PTHTEOCPmVpLPPK/U7KrDjNcjzSPBGg5zhs3HnUcIPhPDEEcY0iGjz29EHmZRAnQZwGUd17MhHrrhVc2IdDazZ6gWfvu16V49RhlPPHRRn17nxZnpz8aNrfGn6R1+Wx0oNvdsno00Fy853i4itQSwMEFAAAAAgAfJDOUtY3vbkZAAAAFwAAABYAAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzSyvNSy7JzM9TSE9P8s/zzMss0dBUqK4FAFBLAwQUAAAACAB8kM5SjabRPDgIAADSHAAADAAAAGdlb2dlYnJhLnhtbN1ZbW/bRhL+nP6KBT9HEpdcvgVSCtu93hVIe0HdOxzucDisyJW0Z4oUSMqWgv74PrO7Ii0pju04KJo6ppfcHc7rszOzzPTb3bpkt6ppdV3NPD72PaaqvC50tZx5224xSr1v334zXap6qeaNZIu6Wctu5kVE2b+Hp3EsBM3JzWbm5aVsW517bFPKjl6ZefViUepKeUwXMy9NrmIRfP/9KEt4OBJXV8no8rsoGaUBv7rioR9/dwlWbNfqN1X9k1yrdiNzdZ2v1Fq+q3PZGamrrtu8mUzu7u7GB/3GdbOcQIV2smuLyXI5H2P0GIys2pnnbt6A79Hbd6F5L/B9PvnXj++snJGu2k5WOVQmB2z1229eTe90VdR37E4X3QruiqPUYyullyu4JI0ij02IagO/bFTe6VvV4t17j8b6br3xDJmsaP2VvWNlb5jHCn2rC9XMPH+chgmPQp7FcRCIME48VjdaVZ2j5U7m5MBteqvVnWVLd0ZixAOPdXVdziXxZL8yziIfF+MZe83iBDMB4xETmEkxk7CQ5iIuWMiIhIdMCIyCpnmMFVrG3yjyGedYYYHPgoAFnAUhHqOIRSBL6N0AtHFm+Pm4iBoa4QppLgxxmblQ4AroDowiywZ6RGFs7iLzN6V3ICWCvF+ZWcKcyCCOJqKEsxCa4DnxGfiCPTQ21gif0S9ngoQECQtSZrga/j58dKtbPS/VzFvIsiWwVosG8Ouf225fKuNENzEEjb/GP1DoDyCPfODXggUrvv+arhiXoAUK2L3oiOPYIBQ+bIOCPsw0AxxIs4gYPfrkGAzGCN+nsGCILA0MpEcYaQZLY0KHIXyphQf7wufYhz3S2wc6Qg4GAgWGkJHeuIH+NAj3GNtHAzcfsLGzFHwMwBIQ9UJj4IzPMAYu6KV2zfaTQu36ILOXyDmHU54q8mW47IUCIwiEnM+8i3d//cvlzxfnGgTRA0a/0Ncf9TRkmV9znYkMn2X1qac/R2J8tA+/jMEifbJ4HgASv7NM4WeoJ1/CzSmcd4qmhKRG51ITwuF5wrMjsoMZv0z4s0fCP50cquXUacTaFdG6rd2pNfoGnyUhi03CMmUT9RL1wtbOJGBJxBJKV4cKioqXsphGV0apiKZHZTSiInuvlsY0iXpF2Y2ZMmiLaiAOdRX3prJS1T2urCiBYqiCUJBYccZQu1lMCdOVQ2gR9AUxgPqofzFD0YwCFlNSfqA2oourW907dqVKdHguBMaHutpsuyO/5Wtqc8xtV4NalqY7c/RFnd9c9p52nJRs0UMNbNHQDG2TbXCOuqpX01LOFVrK5TXBgLFbWVKyMxIWddWxQ66NaW46MR3cVG3zUhdaVv9E3A/d0k/b9Vw1wBtuazLSMKHXWd/qhen9Vg+9gqHJ67oprvctcMJ2/1YN3s4SMQ4y4B79CHJpgqZwb1dSjg6ZilgoRJamAbZMm0sCeBKNsyRLkaxEHKSCwL//+JKIrGB1e626Dta3TO4UMOo8t2xoh917+KG9rMthalPrqruSm27bmBYf1aEhmy6qZamMJ02Q0QHnN/N6d20rBzBAvH7Zb2ivWw3my6u6rBuG7RdQ5wtmZkR9odHQkGo9FbYcaPAXFL5dJ6b9Os9gNijMCBoaDRWdG2yQranQz5rpuMidbk1iAe8jTBmEUDu9rXT37vDQ6fxmsJResPEHZwPZY56O5OU8p5MT7E3drjggcV0X6h6Kp5Oj9emNaipVWtBViPy23raW3Gpm1N626r3sVhdV8bNaYse+l5QxOyhiSQcDC5XrNV60887TklDwDxhmZwu1bNTBIVYZGwenJWs3jZJFu1IKm8JFw26JgcxMTycH9afoQEplasFaI6OMEOy13JkuB9sIycJsv2mbN3pDAGdz5PUbNUC40C2x6CeIGi5pYRuSQ10hGB0FolUrZK0lso7cdqsaCMO7ssMKTl9TVao1TlGsM5iutmvV0IHVhVeaAxr03zorMpcTEFlWz/+PNNRXHfvG4FksD5iO4CVgOgIq3MZgstysJJ3skArMDxeZz3mMM5qFu9xTMup95zLdjz08DvkJYAKh8SLYGydmYD9v63Lb4bSM2FXDadlq69KZ6QVBLwSlGuiHXLTQO3KqpYO/9AfgrAeNAfWFBcF9a4fN2a2wCXAGpR1KFd441t38TReFMno6nAF7JlaHkANKiqTjrn8RHw/2JkkNcZ64sJ0F0OS1PhgXJnzPDVaCXgjSaDiN1RMjc0hIJ+Yh1OcG4ubcQLd9WorNKBlHPBZJEqKj43GaIQsjVFRDUEBEGPhhFqSUmz/0jjVuoKRt9XFp2M6ebcnBl3m9XsuqYJVpr94bXw6VXfoz70o3ean+c/Gayf86z227w+Kl5ee4PBKby8+KDQ9siTHjHyE6fCzSEN8p4oijOnM/BGp+l+hcqyXNn8QHhznJXSSOYrP4dGxax+3g/cUj0bnni6fmuedFZ/AwMgAciowGB47CeMwDP0PbBK+HkQid8z6afiD8gezDanxI1B348iTtfVVSnH+oqExSf4laclZYb5TaUPvz9+qXRlYtfdM8rqhPTU1Xf4rUJD6amtIT8IPPF09N5X5ZVw+Bn8mAPHy6BzooQgSo70Qg/0fP4cybP5a3rLA+NNzIfTnkHtteTN3SV8EzFOALnk2C9KnyrJ0IzUHn6VAw9f6pID7NE/nn5YlPWvBMJG93utSy2Z/t1T9fCjn1PiH4a/D/KBxnRz+RTRQjWxZDHwlDJFEchX6QZj2Av76AzL+KcDwUDdRuREMEOJHEgQiwOwIR++6c/4eMBnL2cOAzX3fcfwC+/Q1QSwECFAAUAAAACAB8kM5SaYCCURMFAAD3JQAAFwAAAAAAAAAAAAAAAAAAAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWxQSwECFAAUAAAACAB8kM5SdIHihXwDAABPEQAAFwAAAAAAAAAAAAAAAABIBQAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWxQSwECFAAUAAAACAB8kM5S1je9uRkAAAAXAAAAFgAAAAAAAAAAAAAAAAD5CAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc1BLAQIUABQAAAAIAHyQzlKNptE8OAgAANIcAAAMAAAAAAAAAAAAAAAAAEYJAABnZW9nZWJyYS54bWxQSwUGAAAAAAQABAAIAQAAqBEAAAAA",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
<br><br>
<iframe src="https://drive.google.com/file/d/1f-Xmg121p5GMFch5KXPtehoVv6gIlm94/preview" width="100%" height="480" allow="autoplay"></iframe>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>