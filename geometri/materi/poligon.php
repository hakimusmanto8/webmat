<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Web Pembelajaran Matematika</title>

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link
      href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
      rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin-2.min.css" rel="stylesheet">
  <meta name=viewport content="width=device-width,initial-scale=1">
  <meta charset="utf-8"/>
  <script src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<link rel="shortcut icon" href="../../img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../img/favicon.ico" type="image/x-icon"></head>
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        
        <?php
        include "../../geosidebar.html";
        ?>

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

            <?php
            include "../../topbar.html";
            ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Poligon</h1>
                    </div>
                    <div class="row">

                        <!-- Area Chart -->
                        <div class="col-xl-12 col-lg-7">
                            <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Materi</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="btn btn-success dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Kegiatan & Latihan
                                            <i class="fas fa-arrow-down fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in"
                                            aria-labelledby="dropdownMenuLink">
                                            <div class="dropdown-header">Pilih salah satu:</div>
                                            <a class="dropdown-item" href="../kegiatan/poligon.php">Kegiatan</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="../latihan/poligon.php">Latihan</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                    <div class="card-body">
                                    
                                    <div id="ggbApplet"></div>

<script>
var parameters = {
"id": "ggbApplet",
"width":1662,
"height":855,
"showMenuBar":true,
"showAlgebraInput":true,
"showToolBar":true,
"customToolBar":"0 73 62 | 1 501 67 , 5 19 , 72 75 76 | 2 15 45 , 18 65 , 7 37 | 4 3 8 9 , 13 44 , 58 , 47 | 16 51 64 , 70 | 10 34 53 11 , 24  20 22 , 21 23 | 55 56 57 , 12 | 36 46 , 38 49  50 , 71  14  68 | 30 29 54 32 31 33 | 25 17 26 60 52 61 | 40 41 42 , 27 28 35 , 6",
"showToolBarHelp":true,
"showResetIcon":false,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ },
"showFullscreenButton":true,
"scale":1,
"disableAutoScale":false,
"allowUpscale":false,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"language":"en",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQAAAAIAE+k1FLSrCIpEwUAAPslAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztmk9v4jgUwM87nyLyafdQSIAArZqOOiOttlKnM9pWq72aYIK3xs7GToF++nm2QxIK6UBgCqjDAec5/vt7z/azncuPswlznkgiqeAB8houcggPxZDyKECpGp310cerD5cREREZJNgZiWSCVYB8nTLPB1Kj2+nqOBzHAQoZlpKGyIkZVjpLgMRoxCgnyHFmkl5wcYcnRMY4JPfhmEzwrQixMmWNlYovms3pdNpY1NoQSdSEgmVzJofNKFINCJEDTecyQNnDBZS7lHvaNvlarus1//1ya+s5o1wqzENoCHRrSEY4ZUrCI2FkQrhy1DwmAYoF5Qo5DA8IC9A3LTm/jxJC/kBOlglouejqw2+Xciymjhj8R0KIU0kKRWf5jNDUaeD1Z8FE4iQB6vWQA3B1MAhQy/cBGovHOECuTczwnCTOE4YSshicKhGa/CZ2hJnMCjY1fRFDYt90svScgpYApyMVAX24DQ85MiZkCK1GWR/hAdQzN5oulRgKkQylMwvQHb5DzjwLn21okhg69/Q5q9Qvx6o5y6JN2y+bGdjNEA9JTPgQEi1x9mpx7vYNZx0AZx2cNuas0p+HuXsimA8AGYb6tpS/8jLbVi22XgsmB+iSCX9NFkt8b/jfJIJWlym3f1HeM+VlK+7U4gteAfQH/t+K7V7JmiSWodT/4N2ISczIbI/grWeUQbw1Qg69Vc/LKEPXbtkhzBnqrQldA7H41JiGj5xIcPTAcvJy9cNfdAhrmKlPgCdJFZTk9fq2BPI/X1IaBZ1RSPO6IkYpD3Wvcrif0+SprI12xz2EPooya4+ACmXsSrqapSSRlnIu9wu5MO16jt17N22RKqZrvuEKNmBADdoqVzr3SEj8AEV95Q8J5lLvwpZtqVpzCZ6/pjX/FLT23nS2mLn4E05yTZS1Vs83qly7G2AGB1bdFtN4GcTuTsxRme+WtrmTEXXrDf2W21lPr9E7YiN6gu6JAsM/mVi4AifhmB3ZPLjGm8aJIpJi/qO9CZtHpRH9bSHn+uhZfezexq33j37b6NSH0l7Yt+fan9c5dz2vC8cAB3dSXke8tBPRjG1EAdm6bG8P+WidvmqeoeD6DHyxk7BSTrJzEtPHNs7vHjZtNCLczrsSLgpck2wOAWR+1pK+YZh5Rp5DAG+fdQDRJj80PaEz59rmuLYJr1s2aNugYwM/R1Rvp2iUG8PcVfKUXywQnXrbm2OYTt610t/AW+fphCSlyeFuIefG49vpAcpLy2dIG00GVXZSbRWS0SGY0ISCks5AexMMboD29gdSsFTBJR3cffHiks6a8ZQO1Vi7d1D3iM60udgynbFI6LPgKqfh6FFwzcx13tJxxjrzab3mvS4Z624TNOYRK0bjtZUKDdgze5Po5VHeOsWUGUJDDMJuo9Vve32/7fa83rnf726I1OsXSO2LjYmu2gdYw+4WstU4BwWujnOchMU5KTjPFeuN6/U6frt13vK98/MOPEAl+94O/plHFFubYzzWMxawkvSnndgxEaayOIe2Uk4ITPIEHJbNVy6cziijOJnvZutbEVZkVjgMD0YofU5whICruwLYo6JpN1Yq3dnbzowoUOTwrQecI5hKKP+Ew8coESnPLLu8DO2l69kycYxnCgMhGIGt7qJbnxZy6aZ4ZeGvApSttYccffBNTfg4ELOlteoHV12yGAG3Rijd364ZAZv3cnWdOzu4KdQ5n9vqUrHCQSkroFn6zKm5+Kbq6jtQSwMEFAAAAAgAT6TUUoFwd9F8AwAATxEAABcAAABnZW9nZWJyYV9kZWZhdWx0czNkLnhtbO2YzW7bOBCAz9unIHivJdmSEgVRCqN72AXaIote9spQY5u7MqmStGXl1foOfaYOf+LIbRLUQRKgRX3w8EczQ34zGok6f7Nbt2QL2ggla5pNUkpActUIuazpxi5en9I3F6/Ol6CWcKUZWSi9Zramhbtyr4e9SZmXbox1XU15y4wRnJKuZdap1FQtFq2QQAnZGXEm1Qe2BtMxDh/5CtbsneLMelsra7uzJOn7fnLjdaL0MkHDJtmZJlku7QQlJbh0aWoaG2do90C7n3m9aZpmyb/v3wU/r4U0lkmOC8FtNbBgm9YabEILa5CW2KED3ICSgs/QR8uuoK3p39LiXoG7JRK+0VvUj8o1nWVFSi9e/XHOldKNIWpXUyShhiCug+gRLyILc9swtw1zfRjsw2DvBxNn0KxUT9TVf+i4plZv0GtckO/4a3D6rWqVJrqmU/SAcctSlFcoqykGpO1WDC1OsjT8srxKs6zMpkG/ZQNosmVoNHplG6u4N+lHF6w10Zd3/l41EGbyeL0UmBOOjLGA0UfnpgNofCvwxG1hKgw+q8b2MCM+2qEFYleC/y/BYDyLkZJr/CWaBlxyBh0QS5BbJKK0wWRKvZcBBV5+7XouC3eZ7w8ocPbaCRz2+rhULXZkHjTm4cL5NIhZEHkQxR4JfJJhncb917RjGvMXDXE3f57E7Pkuj9hOmNmf+6jNY3eUOunMp86xkcblIUr8xzC7uy4G+blCitnzvEG9ny+JbcBdf/n8MG5/Z3KmLRjB5Oj+fesmviVf/gzkn5P7/SDRvoQRv0vfP+CHdfVR/KrKA5xmKBGhl/saVTwVxliKQ2ENdTXW2r3JBXNPuejl3up4F9TI8mioqh1W0Gglb7mOhm7RziLax9xJx4YjK2Y+HkV4ZIwyepJHJEVVpnmZP1lsHpviR5Gda74Sa2iAHaLFwL4U2mkWHsf5iUfrxK/B9nLAiiywOoy5vlzK+pKBi68C1+kvk7OXWpj1IdXsBamWoTAHqhX2fkKqEux+nx9ce1xVi99V9RiWnzas8W9gcav/3PTHTEOCPmVpLPPK/U7KrDjNcjzSPBGg5zhs3HnUcIPhPDEEcY0iGjz29EHmZRAnQZwGUd17MhHrrhVc2IdDazZ6gWfvu16V49RhlPPHRRn17nxZnpz8aNrfGn6R1+Wx0oNvdsno00Fy853i4itQSwMEFAAAAAgAT6TUUkXM3l0aAAAAGAAAABYAAABnZW9nZWJyYV9qYXZhc2NyaXB0LmpzSyvNSy7JzM9TSE9P8s/zzMss0dBUqK7lAgBQSwMEFAAAAAgAT6TUUmiDekCnCgAAezAAAAwAAABnZW9nZWJyYS54bWzdWllz27oVfs79FRg+9CmmABAgyFTKHTtxljvZJsntdPpGS7DEmCJVkbLlzP3x/Q5AarUTJ447VZMoILGf850VRP/35bRgl3Ze51U5CETIA2bLYTXKy/EgWDTnR0nw+9Pf+mNbje3ZPGPn1XyaNYNAU8/VOLyFsYqpLpvNBsGwyOo6HwZsVmQNDRkE1fl5kZc2YPloEHCuIh0JfpSmPD1Sx/GLo1ScpkeREC9M+vz49PnJScDYss6flNW7bGrrWTa0n4YTO83eVMOscatOmmb2pNe7uroKu/2F1Xzcwxbq3rIe9cbjsxBlwEBkWQ+C9uEJ5t0afRW5cZJz0fvn2zd+naO8rJusHGLLxIBF/vS3R/2rvBxVV+wqHzUTsCuOZcAmNh9PwJJE64D1qNcMfJnZYZNf2hpjN14d9c10FrhuWUntj/wTK1aEBWyUX+YjOwenwiQyWmkhY3DMpDwOWDXPbdm0fUW7Zq+brX+Z2ys/LT25FTE8YE1VFWcZzcn+YoJpjh8TKXvMYoMayYRmCjUJagyLqE4LxSJGXUTElEKpqFrEaKFm/K81Z0KghUnOpGRSMBnhVWum0c3QWIm+cerm4/hRb+wIv4jqogg/Vxcp/CQ9YSLtp8E+dBS7J+3+T2gMVtFY7y/mmlCnUixHFdoIFmEneDecYV5Mjx07ahRn9E8wRYtIw2TC3Kxufg4eXeZ1flbYQXCeFTUJa3k+h/it3uvmurCOiW3FGjTxGH/RI/+K7ppDFbywoIXzx/SL8VPUQIBtoKO2sQEUHLRhgxxkugIMpFogRq+cGIPCEQFV8oX2fUAgvYJIV/g+DjoU0X0p7OiLfoS+ZIM+9CPJQUFCgSJitG88YP9UqPY19q9O3DjExtcS+CggS5CoexIDZvwEMWDBatVmvvjmor59veZqRSEkhO2uS95PLleLQkYARHY2CI7fvDw9+Xi8vwOpbyH6nry+kdNYy/1zv70lox+iepfTP7NivKWHv4Zgldx5eSGhJ//lNRVPza9hM5mWXWkytKreX9WQHO4bPF/COrjy18Cffgf+fq/zlv12R6yeUN9WtRs7RdzAmYlY7AyWc5vwl/AX3ncayYxmhsxV50Hh8RIWU9m6UXKiyZYb1eRkN3xpTJXwV2TdmHOD3qlK1flVPDvPSl5327PCBaq1F8QGaSrBGHw3i8lgtu4Qu5Arhyixffi/mMFpasliMsq3+EZEcVWdrxg7sQUivBYCx8O8nC2aLb4NpxTmuMem2uk9qoYXJys+ty02qxFBrbshnFkHTT682YqpHvWL7MwioBx/IiFg7DIryNS5Fc6rsmGdpY2prt9z8VvfLoZFPsqz8h9AvYuV3i2mZ3YOacNjRSS6SWg4WwV6UboR6Bmd+j7DqpqPPl3XkBK2/JedY3SsTZgqBX1IhUEYA/W69i1RFIVCRFLxRKYijdFSDzMS7ygNjVBxQnGeNLFCGHl9S5P0C9vLT7ZpQH3NsqWFhLacG89JvzZeXtcnVbGumlV52TzLZs1i7gJ8KO2caDoux4V1nHQQI/4dXpxVy0/eb0ACaK7P1zPakd/B2fhZVVRzBuWTFPdiMlfCu1Dp+tDWVr2gcOiD/9GD+3aadNUuiMPo4Ur0odL1oqzBg+xJxf48me0s2TKvfVIBhm7KlJMQCqYXZd686V6afHixppQGePwxsxPY7TnbLvefs9/bkb1+VriMpZPEaTWyXoqdAPZ7W+39CzsvbeGFrgTyi2pR++5+Z27bi9p+yJrJcTn6aMfQ1w8Z2csGG/Fd1wSO7DCfYqCvbzmdkRT8CcJ87ciO57ZjiN+Mx8G1Erdnc5uN6om1UIoWDa8Sm90cMd32+4g/Cus8wTSHPTkC2NNs6WIcqBFMhae+Hs7zGQk4O4NVv7BrER7lNU2xqqDeYEkN2mAcqhJgNATErCrycVUiIV00kwoChqFZgwakXn1b2ClSKNY4kS4XUzunbLVFN3PZGba/6BBpiSNkWXX2BWZo5XP8kDVn0bwj84ISC0i0hmxkxWySUVYH/+j+CJVyIWLkZ17Ys2syRSvOtXbu7bZwYCcQJXR0POxYiF2y7KyuikWDVBnQletU2W+2tWYuECSeC+zout3geb4kpvqOYFj+FXK2Ehon1MdeCDapXStnM4ESIAMlDSX/7jjbPrzKRyPrvEcrZ5A9h1UHOUTJ0up4Wg3E0cG1M1JrnHstbnsIOru2AuPY4fcdsJi9pMxrDzMDu4xFqWht2Qq0O0LU2aUdKoH5Pp142Kez1aKaMDqSIUdcCJSOdEinDV9XbHREk4n2y7ZG19fuKeDdOHfyf8Q5ESZgV8u4X8C3YTWdZuWIlS4E/VAV12Re1vFPxknyWCaIjSyTZEc8jxZN1w6jdE1WAH3OfZ8xigiHUSjUIMhR6EHwBUU8CC5QmEFQoEgGwRRFCmNFw2kqKjHPMyox0XMqMdMplZjqBZWY6yWVmOwVlZjtNZWY7g9PX0vVDXLh6euQ9zu/VeWB9S0azyoc3eUNmRmTfM+MdtHgzQL2DXMqk9acImzgqWzjs7sKnLN9uy4yWyzzIs/m19sNt6tSbcf0tibmO8r0s8T+oDJtk3GzkeEhErDNP4pMDTCLQmRQWw1OkySUC9HoN3zADwkENKQgTX5dUqxCQT4c+t5+L6ydUQz6vvw8z8qajpV/Fprx/yI0a9HbMv8RT7SSiYwSmQjlkgPCRSDHEFybiItYIeFY4QLsFHKGROo0Em20foAQTQ4GoggpmsY5k8H5dZRoOlEhzoUiiQUO7pUxUYxsEF6cEAKgShnu1AqNsctPDxKh/JAQ4jLFwSaQwGeUNEU8QIGBCKVUIjFIyaMkEanxcYKMQplyICoTxUXKzcFC9OVgIJIhjjdEImHhOL4GKToeIYhg6OI4lSJN8LmwtX8EkSKXLyMj41Tga5A+XDW6OBSMbg0SAFLaoZKSrTOxgSHkyuwd4RwMKsWhoLIbIEStbduNEDQUiiDCp3rokeYaCVoqRHdWd4AQTQ8For0AAcGadz87IUKnRDjw5UkCJTJpkqaHi5BPjw8DoZsChN34gJySjw9wsq+AY0JuKeqyzcMDyN9DOQCAdsMDHx3sBge6zU5liGskuA6i4KLQJg8nC9o+jnt2n+O4GAkH4KEC6FDxYOjA7cdbvqZzQfjQBccjAJ0R7nrUg55ePj8QdsVwzanW2ki4BJm0HpuHCi4c9+IMgtrYKJH6nP7h+HV6IPyCeEncAYOSywi3DGFviV8qxJ04A+5pnkYc4vfA3HpxINwSodGJApskuBarFN+MiV1xqODi8Hk6VTCJKvHHEQ/HrpcHwi5knkIhDI5iww20D0as5Rfi6Q1+mYe2Xq8OhWEaqbdKcRdB4WYqwlP/EVGFYJ+SMjWUVOCM64HZ9fpQ2AXFA0tgo+ALyU16deQhwkzcApTCGAFrj3PAh+XXH4fCL4gXoraNoG4VTVD2hFAMeSsSJP1LvOMSVxhwUx2fGjs22GUDgUbDIPjbvxdV8/cP79+8fvn+HTs5/Xj8+c+Px+98tZtqm+E0ds1PN9Hdw+uWQ/vxdXdl5idZfuO3LXdDqsalh/P1ZTow7y15DyiuvzpFF5T8pZYOvRsvF7jwnHuQVHvbas1hfGBc381wF7Ham/pP/wNQSwECFAAUAAAACABPpNRS0qwiKRMFAAD7JQAAFwAAAAAAAAAAAAAAAAAAAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWxQSwECFAAUAAAACABPpNRSgXB30XwDAABPEQAAFwAAAAAAAAAAAAAAAABIBQAAZ2VvZ2VicmFfZGVmYXVsdHMzZC54bWxQSwECFAAUAAAACABPpNRSRczeXRoAAAAYAAAAFgAAAAAAAAAAAAAAAAD5CAAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc1BLAQIUABQAAAAIAE+k1FJog3pApwoAAHswAAAMAAAAAAAAAAAAAAAAAEcJAABnZW9nZWJyYS54bWxQSwUGAAAAAAQABAAIAQAAGBQAAAAA",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
window.onload = function() {applet.inject('ggbApplet')};
applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');
</script>
<br><br>
<iframe src="https://drive.google.com/file/d/1Wrt-hYu0FAadVfAGS-ccGP1jLUCVGcb4/preview" width="100%" height="480" allow="autoplay"></iframe>
                            </div>
                        </div>

                        <!-- Pie Chart -->
                        
                    </div>

                    <!-- Content Row -->
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <?php
            include "../../footer.html";
            ?>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <?php
    include "../../script.html";
    ?>

</body>

</html>